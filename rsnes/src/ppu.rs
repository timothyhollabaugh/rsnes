use crate::snes::{AddressBus, DataBus, ReadWrite};

#[derive(Copy, Clone, Debug, Default)]
pub struct PpuWriteOnlyRegisters {
    ///2100h - INIDISP - Display Control 1                                  8xh
    display_control_1: u8,

    ///2101h - OBSEL   - Object Size and Object Base                        (?)
    object_size_base: u8,

    ///2102h - OAMADDL - OAM Address (lower 8bit)                           (?)
    ///2103h - OAMADDH - OAM Address (upper 1bit) and Priority Rotation     (?)
    oam_address: u16,

    ///2104h - OAMDATA - OAM Data Write (write-twice)                       (?)
    oam_data: u8,

    ///2105h - BGMODE  - BG Mode and BG Character Size                      (xFh)
    background_mode: u8,

    ///2106h - MOSAIC  - Mosaic Size and Mosaic Enable                      (?)
    mosaic: u8,

    ///2107h - BG1SC   - BG1 Screen Base and Screen Size                    (?)
    bg1_base_size: u8,

    ///2108h - BG2SC   - BG2 Screen Base and Screen Size                    (?)
    bg2_base_size: u8,

    ///2109h - BG3SC   - BG3 Screen Base and Screen Size                    (?)
    bg3_base_size: u8,

    ///210Ah - BG4SC   - BG4 Screen Base and Screen Size                    (?)
    bg4_base_size: u8,

    ///210Bh - BG12NBA - BG Character Data Area Designation                 (?)
    bg12_character_data_area: u8,

    ///210Ch - BG34NBA - BG Character Data Area Designation                 (?)
    bg34_character_data_area: u8,

    ///210Dh - BG1HOFS - BG1 Horizontal Scroll (X) (write-twice) / M7HOFS   (?,?)
    bg1_horizontal_scroll: u8,

    ///210Eh - BG1VOFS - BG1 Vertical Scroll (Y)   (write-twice) / M7VOFS   (?,?)
    bg1_vertical_scroll: u8,

    ///210Fh - BG2HOFS - BG2 Horizontal Scroll (X) (write-twice)            (?,?)
    bg2_horizontal_scroll: u8,

    ///2110h - BG2VOFS - BG2 Vertical Scroll (Y)   (write-twice)            (?,?)
    bg2_vertical_scroll: u8,

    ///2111h - BG3HOFS - BG3 Horizontal Scroll (X) (write-twice)            (?,?)
    bg3_horizontal_scroll: u8,

    ///2112h - BG3VOFS - BG3 Vertical Scroll (Y)   (write-twice)            (?,?)
    bg3_vertical_scroll: u8,

    ///2113h - BG4HOFS - BG4 Horizontal Scroll (X) (write-twice)            (?,?)
    bg4_horizontal_scroll: u8,

    ///2114h - BG4VOFS - BG4 Vertical Scroll (Y)   (write-twice)            (?,?)
    bg4_vertical_scroll: u8,

    ///2115h - VMAIN   - VRAM Address Increment Mode                        (?Fh)
    vram_address_increment_mode: u8,

    ///2116h - VMADDL  - VRAM Address (lower 8bit)                          (?)
    ///2117h - VMADDH  - VRAM Address (upper 8bit)                          (?)
    vram_address: u16,

    ///2118h - VMDATAL - VRAM Data Write (lower 8bit)                       (?)
    ///2119h - VMDATAH - VRAM Data Write (upper 8bit)                       (?)
    vram_data: u16,

    ///211Ah - M7SEL   - Rotation/Scaling Mode Settings                     (?)
    mode7_settings: u8,

    ///211Bh - M7A     - Rotation/Scaling Parameter A & Maths 16bit operand(FFh)(w2)
    mode7_matrix_a_maths_16bit: u8,

    ///211Ch - M7B     - Rotation/Scaling Parameter B & Maths 8bit operand (FFh)(w2)
    mode7_matrix_b_maths_8bit: u8,

    ///211Dh - M7C     - Rotation/Scaling Parameter C         (write-twice) (?)
    mode7_matrix_c: u8,

    ///211Eh - M7D     - Rotation/Scaling Parameter D         (write-twice) (?)
    mode7_matrix_d: u8,

    ///211Fh - M7X     - Rotation/Scaling Center Coordinate X (write-twice) (?)
    mode7_matrix_x: u8,

    ///2120h - M7Y     - Rotation/Scaling Center Coordinate Y (write-twice) (?)
    mode7_matrix_y: u8,

    ///2121h - CGADD   - Palette CGRAM Address                              (?)
    palette_cgram_address: u8,

    ///2122h - CGDATA  - Palette CGRAM Data Write             (write-twice) (?)
    palette_cgram_data: u8,

    ///2123h - W12SEL  - Window BG1/BG2 Mask Settings                       (?)
    bg12_window_mask_settings: u8,

    ///2124h - W34SEL  - Window BG3/BG4 Mask Settings                       (?)
    bg34_window_mask_settings: u8,

    ///2125h - WOBJSEL - Window OBJ/MATH Mask Settings                      (?)
    window_obj_math_settings: u8,

    ///2126h - WH0     - Window 1 Left Position (X1)                        (?)
    window1_left: u8,

    ///2127h - WH1     - Window 1 Right Position (X2)                       (?)
    window1_right: u8,

    ///2128h - WH2     - Window 2 Left Position (X1)                        (?)
    window2_left: u8,

    ///2129h - WH3     - Window 2 Right Position (X2)                       (?)
    window2_right: u8,

    ///212Ah - WBGLOG  - Window 1/2 Mask Logic (BG1-BG4)                    (?)
    window_mask_logic_bg: u8,

    ///212Bh - WOBJLOG - Window 1/2 Mask Logic (OBJ/MATH)                   (?)
    window_mask_logic_obj_math: u8,

    ///212Ch - TM      - Main Screen Designation                            (?)
    main_screen_designation: u8,

    ///212Dh - TS      - Sub Screen Designation                             (?)
    sub_screen_designation: u8,

    ///212Eh - TMW     - Window Area Main Screen Disable                    (?)
    window_area_main_screen_disable: u8,

    ///212Fh - TSW     - Window Area Sub Screen Disable                     (?)
    window_area_sub_screen_disable: u8,

    ///2130h - CGWSEL  - Color Math Control Register A                      (?)
    color_math_control_a: u8,

    ///2131h - CGADSUB - Color Math Control Register B                      (?)
    color_math_control_b: u8,

    ///2132h - COLDATA - Color Math Sub Screen Backdrop Color               (?)
    color_math_sub_screen_backdrop_color: u8,

    ///2133h - SETINI  - Display Control 2                                  00h?
    display_control_2: u8,
}

#[derive(Copy, Clone, Debug)]
pub enum PpuError {}

pub struct PpuOutputs {
    pub nmi: bool,
}

#[derive(Copy, Clone, Debug)]
pub struct Ppu {
    pub h_pixel: u16,
    pub v_pixel: u16,
    pub field: bool,
    pub control_registers: PpuWriteOnlyRegisters,
}

impl Ppu {
    pub fn new() -> Ppu {
        Ppu {
            h_pixel: 0,
            v_pixel: 0,
            field: false,
            control_registers: PpuWriteOnlyRegisters::default(),
        }
    }

    pub fn dot_clock(&mut self) -> PpuOutputs {
        let mut out = PpuOutputs { nmi: false };

        self.h_pixel += 1;

        if self.h_pixel > 339 {
            self.h_pixel = 0;
            self.v_pixel += 1;
        }

        if self.v_pixel > 261 {
            self.v_pixel = 0;
            self.field = !self.field;
        }

        if self.h_pixel == 0 && self.v_pixel == 225 {
            out.nmi = true;
        }

        out
    }

    pub fn clock(
        &mut self,
        address_bus: AddressBus,
        data_bus: DataBus,
        read_write: ReadWrite,
    ) -> Result<DataBus, PpuError> {
        if let Some(address_bus) = address_bus.into_valid() {
            match read_write {
                ReadWrite::Write => match address_bus.addr() {
                    0x2100 => self.control_registers.display_control_1 = data_bus.unwrap(),
                    0x2101 => self.control_registers.object_size_base = data_bus.unwrap(),
                    0x2102 => {
                        self.control_registers.oam_address &= !0x00ff;
                        self.control_registers.oam_address |= data_bus.unwrap() as u16;
                    }
                    0x2103 => {
                        self.control_registers.oam_address &= !0xff00;
                        self.control_registers.oam_address |= (data_bus.unwrap() as u16) << 8;
                    }
                    0x2104 => self.control_registers.oam_data = data_bus.unwrap(),
                    0x2105 => self.control_registers.background_mode = data_bus.unwrap(),
                    0x2106 => self.control_registers.mosaic = data_bus.unwrap(),
                    0x2107 => self.control_registers.bg1_base_size = data_bus.unwrap(),
                    0x2108 => self.control_registers.bg2_base_size = data_bus.unwrap(),
                    0x2109 => self.control_registers.bg3_base_size = data_bus.unwrap(),
                    0x210A => self.control_registers.bg4_base_size = data_bus.unwrap(),
                    0x210B => self.control_registers.bg12_character_data_area = data_bus.unwrap(),
                    0x210C => self.control_registers.bg34_character_data_area = data_bus.unwrap(),
                    0x210D => self.control_registers.bg1_horizontal_scroll = data_bus.unwrap(),
                    0x210E => self.control_registers.bg1_vertical_scroll = data_bus.unwrap(),
                    0x210F => self.control_registers.bg2_horizontal_scroll = data_bus.unwrap(),
                    0x2110 => self.control_registers.bg2_vertical_scroll = data_bus.unwrap(),
                    0x2111 => self.control_registers.bg3_horizontal_scroll = data_bus.unwrap(),
                    0x2112 => self.control_registers.bg3_vertical_scroll = data_bus.unwrap(),
                    0x2113 => self.control_registers.bg4_horizontal_scroll = data_bus.unwrap(),
                    0x2114 => self.control_registers.bg4_vertical_scroll = data_bus.unwrap(),
                    0x2115 => {
                        self.control_registers.vram_address_increment_mode = data_bus.unwrap()
                    }
                    0x2116 => {
                        self.control_registers.vram_address &= !0x00ff;
                        self.control_registers.vram_address = data_bus.unwrap() as u16;
                    }
                    0x2117 => {
                        self.control_registers.vram_address &= !0xff00;
                        self.control_registers.vram_address = (data_bus.unwrap() as u16) << 8;
                    }
                    0x2118 => {
                        self.control_registers.vram_data &= !0x00ff;
                        self.control_registers.vram_data = data_bus.unwrap() as u16;
                    }
                    0x2119 => {
                        self.control_registers.vram_data &= !0xff00;
                        self.control_registers.vram_data = (data_bus.unwrap() as u16) << 8;
                    }
                    0x211A => self.control_registers.mode7_settings = data_bus.unwrap(),
                    0x211B => self.control_registers.mode7_matrix_a_maths_16bit = data_bus.unwrap(),
                    0x211C => self.control_registers.mode7_matrix_b_maths_8bit = data_bus.unwrap(),
                    0x211D => self.control_registers.mode7_matrix_c = data_bus.unwrap(),
                    0x211E => self.control_registers.mode7_matrix_d = data_bus.unwrap(),
                    0x211F => self.control_registers.mode7_matrix_x = data_bus.unwrap(),
                    0x2120 => self.control_registers.mode7_matrix_y = data_bus.unwrap(),
                    0x2121 => self.control_registers.palette_cgram_address = data_bus.unwrap(),
                    0x2122 => self.control_registers.palette_cgram_data = data_bus.unwrap(),
                    0x2123 => self.control_registers.bg12_window_mask_settings = data_bus.unwrap(),
                    0x2124 => self.control_registers.bg34_window_mask_settings = data_bus.unwrap(),
                    0x2125 => self.control_registers.window_obj_math_settings = data_bus.unwrap(),
                    0x2126 => self.control_registers.window1_left = data_bus.unwrap(),
                    0x2127 => self.control_registers.window1_right = data_bus.unwrap(),
                    0x2128 => self.control_registers.window2_left = data_bus.unwrap(),
                    0x2129 => self.control_registers.window2_right = data_bus.unwrap(),
                    0x212A => self.control_registers.window_mask_logic_bg = data_bus.unwrap(),
                    0x212B => self.control_registers.window_mask_logic_obj_math = data_bus.unwrap(),
                    0x212C => self.control_registers.main_screen_designation = data_bus.unwrap(),
                    0x212D => self.control_registers.sub_screen_designation = data_bus.unwrap(),
                    0x212E => {
                        self.control_registers.window_area_main_screen_disable = data_bus.unwrap()
                    }
                    0x212F => {
                        self.control_registers.window_area_sub_screen_disable = data_bus.unwrap()
                    }
                    0x2130 => self.control_registers.color_math_control_a = data_bus.unwrap(),
                    0x2131 => self.control_registers.color_math_control_b = data_bus.unwrap(),
                    0x2132 => {
                        self.control_registers.color_math_sub_screen_backdrop_color =
                            data_bus.unwrap()
                    }
                    0x2133 => self.control_registers.display_control_2 = data_bus.unwrap(),
                    _ => {}
                },
                ReadWrite::Read => {}
            }
        }
        Ok(DataBus::invalid())
    }
}
