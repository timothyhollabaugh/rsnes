use crate::{
    apu::{Apu, ApuError},
    cpu::{Cpu65816, CpuError, CpuInputs},
    ppu::{Ppu, PpuError},
    ram::{Ram, RamError},
    rom::{Rom, RomError},
};

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum ReadWrite {
    Read,
    Write,
}

#[derive(Copy, Clone, Debug)]
pub struct AddressBus(Option<[u8; 3]>);

impl AddressBus {
    pub fn from_bank_addr(bank: u8, addr: u16) -> AddressBus {
        AddressBus(Some([
            (addr & 0x00ff) as u8,
            ((addr & 0xff00) >> 8) as u8,
            bank,
        ]))
    }

    pub fn from_index(index: usize) -> AddressBus {
        AddressBus(Some(index.to_le_bytes()[0..3].try_into().unwrap()))
    }

    pub fn invalid() -> AddressBus {
        AddressBus(None)
    }

    pub fn unwrap(self) -> [u8; 3] {
        self.0.expect("Attempted to access invalid address bus")
    }

    pub fn into_valid(self) -> Option<ValidAddressBus> {
        self.0.map(ValidAddressBus)
    }
}

#[derive(Copy, Clone, Debug)]
pub struct ValidAddressBus([u8; 3]);

impl ValidAddressBus {
    pub fn bank(self) -> u8 {
        let [_byte, _page, bank] = self.0;
        bank
    }

    pub fn addr(self) -> u16 {
        let [byte, page, _bank] = self.0;
        u16::from_le_bytes([byte, page])
    }

    pub fn page(self) -> u8 {
        let [_byte, page, _bank] = self.0;
        page
    }

    pub fn region(self) -> u8 {
        self.bank() / 0x40
    }

    pub fn bank_addr(self) -> (u8, u16) {
        let [byte, page, bank] = self.0;
        (bank, u16::from_le_bytes([byte, page]))
    }

    pub fn bank_page_byte(self) -> (u8, u8, u8) {
        let [byte, page, bank] = self.0;
        (bank, page, byte)
    }

    pub fn index(self) -> usize {
        usize::from_le_bytes([self.0[0], self.0[1], self.0[2], 0, 0, 0, 0, 0])
    }
}

#[derive(Copy, Clone, Debug)]
pub struct DataBusInvalid;

#[derive(Copy, Clone, Debug)]
pub struct DataBusClash;

#[derive(Copy, Clone, Debug)]
pub struct DataBus(Option<u8>);

impl DataBus {
    pub fn from_data(data: u8) -> DataBus {
        DataBus(Some(data))
    }

    pub fn maybe_data(data: Option<u8>) -> DataBus {
        DataBus(data)
    }

    pub fn invalid() -> DataBus {
        DataBus(None)
    }

    pub fn unwrap(&self) -> u8 {
        self.0.expect("Attempted to access invalid data bus")
    }

    pub fn merge(self, other: DataBus) -> Result<DataBus, DataBusClash> {
        match (self.0, other.0) {
            (None, None) => Ok(DataBus::invalid()),
            (None, Some(d)) => Ok(DataBus::from_data(d)),
            (Some(d), None) => Ok(DataBus::from_data(d)),
            (Some(_), Some(_)) => Err(DataBusClash),
        }
    }

    pub fn valid(&self) -> Result<u8, DataBusInvalid> {
        self.0.ok_or(DataBusInvalid)
    }
}

#[derive(Copy, Clone, Debug)]
pub enum SnesError {
    Cpu(CpuError),
    Ram(RamError),
    Rom(RomError),
    Ppu(PpuError),
    Apu(ApuError),
    DataBusClash,
}

impl From<CpuError> for SnesError {
    fn from(e: CpuError) -> Self {
        SnesError::Cpu(e)
    }
}

impl From<RamError> for SnesError {
    fn from(e: RamError) -> Self {
        SnesError::Ram(e)
    }
}

impl From<RomError> for SnesError {
    fn from(e: RomError) -> Self {
        SnesError::Rom(e)
    }
}

impl From<PpuError> for SnesError {
    fn from(e: PpuError) -> Self {
        SnesError::Ppu(e)
    }
}

impl From<ApuError> for SnesError {
    fn from(e: ApuError) -> Self {
        SnesError::Apu(e)
    }
}

impl From<DataBusClash> for SnesError {
    fn from(_: DataBusClash) -> Self {
        SnesError::DataBusClash
    }
}

pub enum DebugRead {
    Ram(u8),
    Rom(u8),
    Ppu(u8),
    Apu(u8),
}

#[derive(Debug, Clone)]
pub struct Snes {
    pub clock: u64,
    pub cpu_clock_count: u8,
    pub dot_clock_count: u8,
    pub instruction: u32,
    pub cpu: Cpu65816,
    pub ram: Ram,
    pub rom: Rom,
    pub ppu: Ppu,
    pub apu: Apu,
    pub last_address_bus: AddressBus,
    pub last_data_bus: DataBus,
    pub nmi: bool,
}

impl Snes {
    pub fn new(rom: Rom) -> Snes {
        Snes {
            clock: 0,
            cpu_clock_count: 0,
            dot_clock_count: 0,
            instruction: 0,
            cpu: Cpu65816::new(rom.exception_vectors()),
            ram: Ram::new(),
            rom,
            ppu: Ppu::new(),
            apu: Apu::new(),
            last_address_bus: AddressBus::invalid(),
            last_data_bus: DataBus::invalid(),
            nmi: false,
        }
    }

    pub fn debug_address(&mut self, address: usize) -> Result<Vec<DebugRead>, SnesError> {
        let mut out = Vec::new();

        let ram_data_bus = self.ram.clock(
            AddressBus::from_index(address),
            DataBus::invalid(),
            ReadWrite::Read,
        )?;

        if let Ok(data) = ram_data_bus.valid() {
            out.push(DebugRead::Ram(data));
        }

        let rom_data_bus = self.rom.clock(
            AddressBus::from_index(address),
            DataBus::invalid(),
            ReadWrite::Read,
        )?;

        if let Ok(data) = rom_data_bus.valid() {
            out.push(DebugRead::Rom(data));
        }

        let ppu_data_bus = self.ppu.clock(
            AddressBus::from_index(address),
            DataBus::invalid(),
            ReadWrite::Read,
        )?;

        if let Ok(data) = ppu_data_bus.valid() {
            out.push(DebugRead::Ppu(data));
        }

        let apu_data_bus = self.apu.clock(
            AddressBus::from_index(address),
            DataBus::invalid(),
            ReadWrite::Read,
        )?;

        if let Ok(data) = apu_data_bus.valid() {
            out.push(DebugRead::Apu(data));
        }

        Ok(out)
    }

    pub fn step(&mut self) -> Result<u64, SnesError> {
        self.clock()?;

        let mut cycles = 0;

        while self.cpu.cycle != 0 {
            self.clock()?;
            cycles += 1;
        }

        Ok(cycles)
    }

    pub fn clock(&mut self) -> Result<(), SnesError> {
        self.cpu_clock_count += 1;

        if self.cpu_clock_count >= 6 {
            self.cpu_clock_count = 0;

            let cpu_outputs = self.cpu.clock_rising()?;

            self.last_address_bus = cpu_outputs.address_bus;

            let ram_data_bus = self.ram.clock(
                cpu_outputs.address_bus,
                cpu_outputs.data_bus,
                cpu_outputs.read_write,
            )?;

            let rom_data_bus = self.rom.clock(
                cpu_outputs.address_bus,
                cpu_outputs.data_bus,
                cpu_outputs.read_write,
            )?;

            let ppu_data_bus = self.ppu.clock(
                cpu_outputs.address_bus,
                cpu_outputs.data_bus,
                cpu_outputs.read_write,
            )?;

            let apu_data_bus = self.apu.clock(
                cpu_outputs.address_bus,
                cpu_outputs.data_bus,
                cpu_outputs.read_write,
            )?;

            let data_bus = cpu_outputs
                .data_bus
                .merge(ram_data_bus)?
                .merge(rom_data_bus)?
                .merge(ppu_data_bus)?
                .merge(apu_data_bus)?;

            self.last_data_bus = data_bus;

            let cpu_inputs = CpuInputs {
                data_bus,
                abortb: true,
                interrupt_request: true,
                nonmaskable_interrupt: true,
                reset: true,
                ready: None,
                bus_enable: true,
            };

            self.cpu.clock_falling(cpu_inputs)?;

            if self.cpu.cycle == 0 {
                self.instruction += 1;
            }
        }

        self.dot_clock_count += 1;

        if self.dot_clock_count >= 4 {
            self.dot_clock_count = 0;
            let ppu_output = self.ppu.dot_clock();
            self.nmi = ppu_output.nmi;
        }

        self.clock += 1;

        Ok(())
    }
}
