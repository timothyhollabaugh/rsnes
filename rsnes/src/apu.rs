use crate::snes::{AddressBus, DataBus, ReadWrite};

#[derive(Copy, Clone, Debug)]
pub enum ApuError {}

#[derive(Copy, Clone, Debug)]
pub struct Apu {
    in_registers: [u8; 4],
    out_registers: [u8; 4],
    intialized: bool,
}

impl Apu {
    pub fn new() -> Apu {
        Apu {
            in_registers: [0x00; 4],
            out_registers: [0xAA, 0xBB, 0x00, 0x00],
            intialized: false,
        }
    }

    pub fn clock(
        &mut self,
        address_bus: AddressBus,
        data_bus: DataBus,
        read_write: ReadWrite,
    ) -> Result<DataBus, ApuError> {
        if let Some(address_bus) = address_bus.into_valid() {
            match (address_bus.addr(), read_write) {
                (0x2140, ReadWrite::Write) => {
                    self.in_registers[0] = data_bus.unwrap();

                    // Emulate the boot rom just enough to get the game playing
                    if self.in_registers[0] == 0xcc {
                        self.intialized = true;
                    }

                    if self.intialized {
                        self.out_registers[0] = self.in_registers[0];
                    }
                    Ok(DataBus::invalid())
                }
                (0x2141, ReadWrite::Write) => {
                    self.in_registers[1] = data_bus.unwrap();
                    Ok(DataBus::invalid())
                }
                (0x2142, ReadWrite::Write) => {
                    self.in_registers[2] = data_bus.unwrap();
                    Ok(DataBus::invalid())
                }
                (0x2143, ReadWrite::Write) => {
                    self.in_registers[3] = data_bus.unwrap();
                    Ok(DataBus::invalid())
                }
                (0x2140, ReadWrite::Read) => Ok(DataBus::from_data(self.out_registers[0])),
                (0x2141, ReadWrite::Read) => Ok(DataBus::from_data(self.out_registers[1])),
                (0x2142, ReadWrite::Read) => Ok(DataBus::from_data(self.out_registers[2])),
                (0x2143, ReadWrite::Read) => Ok(DataBus::from_data(self.out_registers[3])),
                _ => Ok(DataBus::invalid()),
            }
        } else {
            Ok(DataBus::invalid())
        }
    }
}
