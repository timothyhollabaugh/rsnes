use std::sync::Arc;

use crate::snes::{AddressBus, DataBus, ReadWrite};

#[derive(Copy, Clone, Debug)]
pub enum RomParseError {
    InvalidHeader(RomHeaderParseError),
}

impl From<RomHeaderParseError> for RomParseError {
    fn from(e: RomHeaderParseError) -> Self {
        RomParseError::InvalidHeader(e)
    }
}

#[derive(Copy, Clone, Debug)]
pub enum RomError {
    UnhandledMapMode,
}

#[derive(Clone, Debug)]
pub struct Rom {
    rom: Arc<[u8]>,
    header: RomHeader,
    exception_vectors: ExceptionVectors,
}

impl Rom {
    pub fn new(rom: Vec<u8>) -> Result<Rom, RomParseError> {
        let mut header = RomHeader::parse(rom[0x007fb0..0x007fe0].try_into().unwrap());
        let mut exception_vectors =
            ExceptionVectors::parse(rom[0x007fe0..0x008000].try_into().unwrap());
        if header.is_err() && rom.len() >= 0x00ffe0 {
            header = RomHeader::parse(rom[0x00ffb0..0x00ffe0].try_into().unwrap());
            exception_vectors =
                ExceptionVectors::parse(rom[0x00ffe0..0x010000].try_into().unwrap());
        }
        if header.is_err() && rom.len() >= 0x40ffe0 {
            header = RomHeader::parse(rom[0x40ffb0..0x40ffe0].try_into().unwrap());
            exception_vectors =
                ExceptionVectors::parse(rom[0x40ffe0..0x410000].try_into().unwrap());
        }

        Ok(Rom {
            rom: rom.into_boxed_slice().into(),
            header: header?,
            exception_vectors,
        })
    }

    pub fn clock(
        &mut self,
        address_bus: AddressBus,
        _data_bus: DataBus,
        _read_write: ReadWrite,
    ) -> Result<DataBus, RomError> {
        if let Some(bus) = address_bus.into_valid() {
            if ((bus.region() == 0 || bus.region() == 2) && bus.addr() > 0x7fff)
                || (bus.region() == 1 || bus.region() == 3)
            {
                match self.header.map_mode {
                    MapMode::LoRom => {
                        let rom_addr = (bus.index() & 0xff0000) / 2 + (bus.index() & 0x007fff);
                        Ok(DataBus::maybe_data(self.rom.get(rom_addr).cloned()))
                    }
                    MapMode::HiRom => Err(RomError::UnhandledMapMode),
                    MapMode::LoRomSdd1 => Err(RomError::UnhandledMapMode),
                    MapMode::LoRomSa1 => Err(RomError::UnhandledMapMode),
                    MapMode::ExHiRom => Err(RomError::UnhandledMapMode),
                    MapMode::ExHiRomSpc7110 => Err(RomError::UnhandledMapMode),
                }
            } else {
                Ok(DataBus::invalid())
            }
        } else {
            Ok(DataBus::invalid())
        }
    }

    pub fn header(&self) -> RomHeader {
        self.header
    }

    pub fn exception_vectors(&self) -> ExceptionVectors {
        self.exception_vectors
    }
}

#[derive(Copy, Clone, Debug)]
pub enum RomSpeed {
    Fast,
    Slow,
}

impl RomSpeed {
    pub fn parse(byte: u8) -> Result<RomSpeed, ()> {
        match byte & 0xf0 {
            0x30 => Ok(RomSpeed::Fast),
            0x20 => Ok(RomSpeed::Slow),
            _ => Err(()),
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum MapMode {
    LoRom,
    HiRom,
    LoRomSdd1,
    LoRomSa1,
    ExHiRom,
    ExHiRomSpc7110,
}

impl MapMode {
    pub fn parse(byte: u8) -> Result<MapMode, ()> {
        match byte & 0x0f {
            0x0 => Ok(MapMode::LoRom),
            0x1 => Ok(MapMode::HiRom),
            0x2 => Ok(MapMode::LoRomSdd1),
            0x3 => Ok(MapMode::LoRomSa1),
            0x5 => Ok(MapMode::ExHiRom),
            0xa => Ok(MapMode::ExHiRomSpc7110),
            _ => Err(()),
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum Coprocessor {
    Dsp,
    Gsu,
    Obc1,
    Sa1,
    Sdd1,
    Srtc,
    Other,
    Custom,
    //Spc7110,
    //St01x,
    //St018,
    //Cx4,
}

#[derive(Copy, Clone, Debug)]
pub struct Chipset {
    pub ram: bool,
    pub coprocessor: Option<Coprocessor>,
    pub battery: bool,
    pub rtc_4513: bool,
    pub overclocked_gsu1: bool,
}

impl Chipset {
    pub fn parse(byte: u8) -> Result<Chipset, ()> {
        match byte {
            0x00 => Ok(Chipset {
                ram: false,
                coprocessor: None,
                battery: false,
                rtc_4513: false,
                overclocked_gsu1: false,
            }),
            0x01 => Ok(Chipset {
                ram: true,
                coprocessor: None,
                battery: false,
                rtc_4513: false,
                overclocked_gsu1: false,
            }),
            0x02 => Ok(Chipset {
                ram: true,
                coprocessor: None,
                battery: true,
                rtc_4513: false,
                overclocked_gsu1: false,
            }),
            _ => {
                let coprocessor = match byte & 0xf0 {
                    0x00 => Coprocessor::Dsp,
                    0x10 => Coprocessor::Gsu,
                    0x20 => Coprocessor::Obc1,
                    0x30 => Coprocessor::Sa1,
                    0x40 => Coprocessor::Sdd1,
                    0x50 => Coprocessor::Srtc,
                    0xE0 => Coprocessor::Other,
                    0xF0 => Coprocessor::Custom,
                    _ => return Err(()),
                };

                match byte & 0x0f {
                    0x03 => Ok(Chipset {
                        ram: false,
                        coprocessor: Some(coprocessor),
                        battery: false,
                        rtc_4513: false,
                        overclocked_gsu1: false,
                    }),
                    0x04 => Ok(Chipset {
                        ram: true,
                        coprocessor: Some(coprocessor),
                        battery: false,
                        rtc_4513: false,
                        overclocked_gsu1: false,
                    }),
                    0x05 => Ok(Chipset {
                        ram: true,
                        coprocessor: Some(coprocessor),
                        battery: true,
                        rtc_4513: false,
                        overclocked_gsu1: false,
                    }),
                    0x06 => Ok(Chipset {
                        ram: false,
                        coprocessor: Some(coprocessor),
                        battery: true,
                        rtc_4513: false,
                        overclocked_gsu1: false,
                    }),
                    0x09 => Ok(Chipset {
                        ram: true,
                        coprocessor: Some(coprocessor),
                        battery: true,
                        rtc_4513: true,
                        overclocked_gsu1: false,
                    }),
                    0x0a => Ok(Chipset {
                        ram: true,
                        coprocessor: Some(coprocessor),
                        battery: true,
                        rtc_4513: false,
                        overclocked_gsu1: true,
                    }),
                    _ => Err(()),
                }
            }
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct ExtendedRomHeader {
    pub maker_code: [u8; 2],
    pub game_code: [u8; 4],
    pub expansion_flash_size_k: u32,
    pub expansion_ram_size_k: u32,
    pub special_version: u8,
    pub chipset_subtype: u8,
}

#[derive(Copy, Clone, Debug)]
pub struct RomHeader {
    pub title: [u8; 21],
    pub speed: RomSpeed,
    pub map_mode: MapMode,
    pub chipset: Chipset,
    pub rom_size_k: u16,
    pub ram_size_k: u8,
    pub country: u8,
    pub developer_id: u8,
    pub rom_version: u8,
    pub checksum: u16,
    pub extended: Option<ExtendedRomHeader>,
}

#[derive(Copy, Clone, Debug)]
#[allow(clippy::enum_variant_names)]
pub enum RomHeaderParseError {
    InvalidSpeed,
    InvalidMapMode,
    InvalidChipset,
    InvalidRomSize,
    InvalidRamSize,
    InvalidChecksum,
}

impl RomHeader {
    pub fn parse(bytes: [u8; 48]) -> Result<RomHeader, RomHeaderParseError> {
        let title: [u8; 21] = bytes[0x10..0x25].try_into().unwrap();
        let speed = RomSpeed::parse(bytes[0x25]).map_err(|_| RomHeaderParseError::InvalidSpeed)?;
        let map_mode =
            MapMode::parse(bytes[0x25]).map_err(|_| RomHeaderParseError::InvalidMapMode)?;
        let chipset =
            Chipset::parse(bytes[0x26]).map_err(|_| RomHeaderParseError::InvalidChipset)?;
        let rom_size_k = u16::checked_shl(0x01, bytes[0x27] as u32)
            .ok_or(RomHeaderParseError::InvalidRomSize)?;
        let ram_size_k =
            u8::checked_shl(0x01, bytes[0x28] as u32).ok_or(RomHeaderParseError::InvalidRomSize)?;
        let country = bytes[0x29];
        let developer_id = bytes[0x2a];
        let rom_version = bytes[0x2b];

        let checksum_complement = u16::from_be_bytes(bytes[0x2c..0x2e].try_into().unwrap());
        let checksum = u16::from_be_bytes(bytes[0x2e..0x30].try_into().unwrap());
        let checksum_valid = checksum == !checksum_complement;
        if !checksum_valid {
            return Err(RomHeaderParseError::InvalidChecksum);
        }

        let extended = if developer_id == 0x33 {
            Some(ExtendedRomHeader {
                maker_code: bytes[0x00..0x02].try_into().unwrap(),
                game_code: bytes[0x02..0x06].try_into().unwrap(),
                expansion_flash_size_k: u32::checked_shl(0x01, bytes[0x0c] as u32).unwrap(),
                expansion_ram_size_k: u32::checked_shl(0x01, bytes[0x0d] as u32).unwrap(),
                special_version: bytes[0x0e],
                chipset_subtype: bytes[0x0f],
            })
        } else {
            None
        };

        Ok(RomHeader {
            title,
            speed,
            map_mode,
            chipset,
            rom_size_k,
            ram_size_k,
            country,
            developer_id,
            rom_version,
            checksum,
            extended,
        })
    }
}

#[derive(Copy, Clone, Debug)]
pub struct ExceptionVectors {
    pub cop_native: u16,
    pub brk_native: u16,
    pub abort_native: u16,
    pub nmi_native: u16,
    pub irq_native: u16,
    pub cop_emu: u16,
    pub abort_emu: u16,
    pub nmi_emu: u16,
    pub reset_emu: u16,
    pub irq_break_emu: u16,
}

impl ExceptionVectors {
    pub fn parse(bytes: [u8; 32]) -> ExceptionVectors {
        ExceptionVectors {
            cop_native: u16::from_le_bytes(bytes[0x04..0x06].try_into().unwrap()),
            brk_native: u16::from_le_bytes(bytes[0x06..0x08].try_into().unwrap()),
            abort_native: u16::from_le_bytes(bytes[0x08..0x0a].try_into().unwrap()),
            nmi_native: u16::from_le_bytes(bytes[0x0a..0x0c].try_into().unwrap()),
            irq_native: u16::from_le_bytes(bytes[0x0e..0x10].try_into().unwrap()),
            cop_emu: u16::from_le_bytes(bytes[0x14..0x16].try_into().unwrap()),
            abort_emu: u16::from_le_bytes(bytes[0x18..0x1a].try_into().unwrap()),
            nmi_emu: u16::from_le_bytes(bytes[0x1a..0x1c].try_into().unwrap()),
            reset_emu: u16::from_le_bytes(bytes[0x1c..0x1e].try_into().unwrap()),
            irq_break_emu: u16::from_le_bytes(bytes[0x1e..0x20].try_into().unwrap()),
        }
    }
}
