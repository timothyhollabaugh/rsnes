use std::io::Write;

use rsnes::{opcodes, Rom};
use rsnes::{Snes, SnesError};

fn main() {
    let rom_dump = std::fs::read(
        std::env::args()
            .nth(1)
            .expect("Argument 1 should be ROM filename"),
    )
    .expect("Could not read ROM");

    let dump_header = rom_dump[0x08] == 0xaa && rom_dump[0x09] == 0xbb;

    let rom = if dump_header {
        rom_dump[0x200..].to_owned()
    } else {
        rom_dump
    };

    let rom = Rom::new(rom).unwrap();

    println!("Loaded ROM");
    println!("Header: {:#02x?}", rom.header());
    println!("Exception Vectors: {:#02x?}", rom.exception_vectors());

    let mut snes = Snes::new(rom);

    let mut last_line = String::new();

    loop {
        println!("{}", description(&snes));

        print!("-> ");

        std::io::stdout().flush().unwrap();

        let mut line = String::new();
        std::io::stdin().read_line(&mut line).unwrap();

        let mut args = line.split_whitespace().peekable();

        if args.peek().is_none() {
            args = last_line.split_whitespace().peekable();
        } else {
            last_line = line.clone();
        }

        let mut clocks = 0;
        let cmd = args.next();
        println!("{:?}", cmd);
        let start = std::time::Instant::now();
        let result = match cmd {
            Some("c") | Some("clock") => clock(args, &mut snes, &mut clocks),
            Some("s") | Some("step") => step(args, &mut snes, &mut clocks),
            Some("b") | Some("break") => breakpoint(args, &mut snes, &mut clocks),
            Some("r") | Some("run") => run(args, &mut snes, &mut clocks),
            Some("read") => read(args, &mut snes, &mut clocks),
            Some("ppu") => ppu(args, &mut snes, &mut clocks),
            Some("apu") => apu(args, &mut snes, &mut clocks),
            Some(unknown_command) => {
                Err(DebuggerError::UnknownCommand(unknown_command.to_string()))
            }
            None => Err(DebuggerError::NoLastCommand),
        };
        let elapsed = start.elapsed();

        println!(
            "Ran {} cycles in {} microseconds for {} MHz",
            clocks,
            elapsed.as_micros(),
            clocks as f64 / elapsed.as_micros() as f64
        );

        match result {
            Ok(out) => println!("{}", out),
            Err(DebuggerError::Snes(e)) => println!("Error running SNES: {:02x?}", e),
            Err(DebuggerError::NoLastCommand) => println!("No last command to run"),
            Err(DebuggerError::UnknownCommand(cmd)) => println!("Unknown command: {}", cmd),
            Err(DebuggerError::MissingArgument) => println!("Missing argument for command"),
            Err(DebuggerError::InvalidDecimal(e)) => {
                println!("Error parsing decimal argument: {:?}", e)
            }
            Err(DebuggerError::InvalidHex(e)) => println!("Error parsing hex argument: {:?}", e),
        }
    }
}

pub enum DebuggerError {
    Snes(SnesError),
    NoLastCommand,
    UnknownCommand(String),
    MissingArgument,
    InvalidHex(std::num::ParseIntError),
    InvalidDecimal(std::num::ParseIntError),
}

impl From<SnesError> for DebuggerError {
    fn from(e: SnesError) -> Self {
        DebuggerError::Snes(e)
    }
}

fn description(snes: &Snes) -> String {
    let mut desc = String::new();

    desc += &format!(
        "Clock: {}, Instruction: {} Cycle: {}, Opcode: {:02x?} ({}), Operand: {:02x?}",
        snes.clock,
        snes.instruction,
        snes.cpu.cycle,
        snes.cpu.opcode,
        opcodes::opcode_name(snes.cpu.opcode),
        snes.cpu.operand,
    );

    desc += &format!("\nRegisters: | X {:04x?} | Y {:04x?} | SP {:04x?} | A {:04x?} | PC {:04x?} | D {:04x?} | PBR {:04x?} | DBR {:04x?} | MDR {:04x?}",
        snes.cpu.registers.index_x,
        snes.cpu.registers.index_y,
        snes.cpu.registers.stack_pointer,
        snes.cpu.registers.accumulator,
        snes.cpu.registers.program_counter,
        snes.cpu.registers.direct,
        snes.cpu.registers.program_bank,
        snes.cpu.registers.data_bank,
        snes.cpu.registers.mdr,
    );

    desc += &format!("\nProcessor Status: | N {} | V {} | M {} | X {} | B {} | D {} | I {} | Z {} | C {} | E {} |",
        snes.cpu.registers.processor_status.negative as u8,
        snes.cpu.registers.processor_status.overflow as u8,
        snes.cpu.registers.processor_status.memory_select as u8,
        snes.cpu.registers.processor_status.index_register_select as u8,
        snes.cpu.registers.processor_status.brk as u8,
        snes.cpu.registers.processor_status.decimal_mode as u8,
        snes.cpu.registers.processor_status.irq_disable as u8,
        snes.cpu.registers.processor_status.zero as u8,
        snes.cpu.registers.processor_status.carry as u8,
        snes.cpu.registers.processor_status.emulation as u8,
    );

    desc += &format!(
        "\nAddress Bus Bank: {}, Address: {}, Data Bus: {}",
        if let Some(address_bus) = snes.last_address_bus.into_valid() {
            format!("{:02x?}", address_bus.bank())
        } else {
            "--".to_string()
        },
        if let Some(address_bus) = snes.last_address_bus.into_valid() {
            format!("{:04x?}", address_bus.addr())
        } else {
            "----".to_string()
        },
        if let Ok(data_bus) = snes.last_data_bus.valid() {
            format!("{:02x?}", data_bus)
        } else {
            "--".to_string()
        },
    );

    desc
}

fn clock<'a>(
    mut args: impl Iterator<Item = &'a str>,
    snes: &mut Snes,
    clocks_ran: &mut u64,
) -> Result<String, DebuggerError> {
    let clocks: u64 = if let Some(arg) = args.next() {
        arg.parse().map_err(DebuggerError::InvalidDecimal)?
    } else {
        1
    };

    for _ in 0..clocks {
        snes.clock()?;
        *clocks_ran += 1;
    }

    Ok(String::new())
}

fn step<'a>(
    mut args: impl Iterator<Item = &'a str>,
    snes: &mut Snes,
    clocks_ran: &mut u64,
) -> Result<String, DebuggerError> {
    let steps: u64 = if let Some(arg) = args.next() {
        arg.parse().map_err(DebuggerError::InvalidDecimal)?
    } else {
        1
    };

    for _ in 0..steps {
        *clocks_ran += snes.step()?;
    }

    Ok(String::new())
}

fn breakpoint<'a>(
    mut args: impl Iterator<Item = &'a str>,
    snes: &mut Snes,
    clocks_ran: &mut u64,
) -> Result<String, DebuggerError> {
    let arg = args.next().ok_or(DebuggerError::MissingArgument)?;
    let pc = u16::from_str_radix(arg, 16).map_err(DebuggerError::InvalidHex)?;

    loop {
        *clocks_ran += snes.step()?;
        if snes.cpu.registers.program_counter >= pc {
            break;
        }
    }

    Ok(String::new())
}

fn run<'a>(
    _args: impl Iterator<Item = &'a str>,
    snes: &mut Snes,
    clocks_ran: &mut u64,
) -> Result<String, DebuggerError> {
    loop {
        snes.clock()?;
        *clocks_ran += 1;
    }
}

fn read<'a>(
    mut args: impl Iterator<Item = &'a str>,
    snes: &mut Snes,
    _clocks_ran: &mut u64,
) -> Result<String, DebuggerError> {
    let arg = args.next().ok_or(DebuggerError::MissingArgument)?;
    let address = usize::from_str_radix(arg, 16).map_err(DebuggerError::InvalidHex)?;

    let reads = snes.debug_address(address)?;

    let out = reads
        .into_iter()
        .map(|read| match read {
            rsnes::DebugRead::Ram(b) => format!("RAM: {:02x?}", b),
            rsnes::DebugRead::Rom(b) => format!("ROM: {:02x?}", b),
            rsnes::DebugRead::Ppu(b) => format!("PPU: {:02x?}", b),
            rsnes::DebugRead::Apu(b) => format!("APU: {:02x?}", b),
        })
        .collect::<Vec<_>>()
        .join(", ");

    Ok(out)
}

fn ppu<'a>(
    _args: impl Iterator<Item = &'a str>,
    snes: &mut Snes,
    _clocks_ran: &mut u64,
) -> Result<String, DebuggerError> {
    Ok(format!("{:02x?}", snes.ppu))
}

fn apu<'a>(
    _args: impl Iterator<Item = &'a str>,
    snes: &mut Snes,
    _clocks_ran: &mut u64,
) -> Result<String, DebuggerError> {
    Ok(format!("{:02x?}", snes.apu))
}
