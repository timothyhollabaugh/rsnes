use rsnes::{apu::Apu, cpu::Cpu65816, ppu::Ppu, ram::Ram, Rom, Snes};

fn main() {
    println!("snes: {}", std::mem::size_of::<Snes>());
    println!("rom: {}", std::mem::size_of::<Rom>());
    println!("ram: {}", std::mem::size_of::<Ram>());
    println!("cpu: {}", std::mem::size_of::<Cpu65816>());
    println!("ppu: {}", std::mem::size_of::<Ppu>());
    println!("apu: {}", std::mem::size_of::<Apu>());
}
