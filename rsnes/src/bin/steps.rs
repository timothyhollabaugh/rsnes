use rsnes::Rom;
use rsnes::{Snes, SnesError};

fn main() -> Result<(), SnesError> {
    let mut args = std::env::args().skip(1);

    let rom_dump = std::fs::read(args.next().expect("Argument 1 should be ROM filename"))
        .expect("Could not read ROM");

    let clocks: u64 = args
        .next()
        .expect("Argument 2 should be number of clock cycles")
        .parse()
        .expect("Could not parse clock cycles");

    let dump_header = rom_dump[0x08] == 0xaa && rom_dump[0x09] == 0xbb;

    let rom = if dump_header {
        rom_dump[0x200..].to_owned()
    } else {
        rom_dump
    };

    let rom = Rom::new(rom).unwrap();

    let mut snes = Snes::new(rom);

    for _ in 0..clocks {
        snes.clock()?;
    }

    Ok(())
}
