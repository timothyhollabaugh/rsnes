pub mod alu;
mod control;
pub mod opcodes;

use crate::{
    rom::ExceptionVectors,
    snes::{AddressBus, DataBus, DataBusInvalid, ReadWrite},
};

use self::control::CpuControl;

#[cfg(test)]
mod test;

#[derive(Copy, Clone, Debug)]
pub struct CpuInputs {
    /// The input side of the 8-bit data bus. This is only the data bus and does not include the
    /// high 8 bits of the bank select for the address bus of the first half of the cycle. If this
    /// is None, nothing else is writing to the data bus and it is free to be written to.
    ///
    /// # Datasheet
    ///
    /// The Data/Bank Address Bus pins provide both the Bank Address and Data. The bank address is present
    /// during the first half of a memory cycle, and the data value is read or written during the second half of the
    /// memory cycle. Two memory cycles are required to transfer 16-bit values. These lines may be set to the
    /// high impedance state by the Bus Enable (BE) signal.
    pub data_bus: DataBus,

    /// # Datasheet
    ///
    /// The Abort negative pulse active input is used to abort instructions (usually due to an Address Bus
    /// condition). A negative transition will inhibit modification of any internal register during the current
    /// instruction. Upon completion of this instruction, an interrupt sequence is initiated. The location of the
    /// aborted opcode is stored as the return address in stack memory. The Abort vector address is 00FFF8,9
    /// (Emulation mode) or 00FFE8,9 (Native mode). Note that ABORTB is a pulse sensitive signal; i.e., an
    /// abort will occur whenever there is a negative pulse (or level) on the ABORTB pin during a PHI2 clock.
    pub abortb: bool,

    /// # Datasheet
    ///
    /// The Interrupt Request negative level active input signal is used to request that an interrupt sequence be
    /// initiated. When the IRQB Disable flag is cleared, a low input logic level initiates an interrupt sequence after
    /// the current instruction is completed. The Wait for Interrupt (WAI) instruction may be executed to ensure the
    /// interrupt will be recognized immediately. The Interrupt Request vector address is 00FFFE, F (Emulation
    /// mode) or 00FFEE,F (Native mode). Since IRQB is a level sensitive input, an interrupt will occur if the
    /// interrupt source was not cleared since the last interrupt. Also, no interrupt will occur if the interrupt source is
    /// cleared prior to interrupt recognition. The IRQB signal going low causes 4 bytes of information to be pushed
    /// onto the stack before jumping to the interrupt handler. The first byte is PBR followed by PCH, PCL and P
    /// (Processor Status Register). These register values are used by the RTI instruction to return the processor
    /// to its original state prior to handling the IRQ interrupt (see Table 6-1)
    pub interrupt_request: bool,

    /// # Datasheet
    ///
    /// A negative transition on the non-maskable Interrupt input initiates an interrupt sequence. A high to low
    /// transition initiates an interrupt sequence after the current instruction is completed. The Wait for Interrupt
    /// instruction may be executed to ensure that the interrupt will be recognized immediately. The non-maskable
    /// Interrupt vector address is 00FFFA, B (Emulation mode) or 00FFEA, B (Native mode). Since NMIB is an
    /// edge sensitive input, an interrupt will occur if there is a negative transition while servicing a previous
    /// interrupt. No interrupt will occur if NMIB remains low after the negative transition was processed. The
    /// NMIB signal going low causes 4 bytes of information to be pushed onto the stack before jumping to the
    /// interrupt handler. The first byte on the stack is the PBR followed by the PCH, PCL and P, these register
    /// values are used by the RTI instruction to return the processor to its original state prior to the NMI interrupt.
    pub nonmaskable_interrupt: bool,

    /// # Datasheet
    ///
    /// The Reset active low input is used to initialize the microprocessor and start program execution. The Reset
    /// input buffer has hysteresis such that a simple R-C timing circuit may be used with the internal pull-up
    /// device. The RESB signal must be held low for at least two clock cycles after VDD reaches operating
    /// voltage. Ready (RDY) has no effect while RESB is being held low. The stack pointer must be initialized by
    /// the user's software. During the Reset conditioning period the following processor initialization takes place:
    ///
    /// ## Registers
    ///
    /// D = 0x000
    /// DBR = 0x00
    /// PBR = 0x00
    /// SH = 0x01
    /// SL = Not Initialized
    /// XH = 0x00
    /// XL = Not Initialized
    /// YH = 0x00
    /// YL = Not Initialized
    /// A = Not Initialized
    ///
    /// ## Signals
    ///
    /// E = 1
    /// MX = 1
    /// RWB = 1
    /// VDA = 0
    /// VPB = 1
    /// VPA = 0
    ///
    /// ## P Register
    ///
    /// N = Not Initialized
    /// V = Not Initialized
    /// M = 1
    /// X = 1
    /// D = 0
    /// I = 1
    /// Z = Not Initialized
    /// C = 1
    /// E = Not Initialized
    ///
    /// When Reset is brought high, an interrupt sequence is initiated
    /// - STP and WAI instructions are cleared
    /// - RWB remains in the high state during the stack address cycles.
    /// - The Reset vector address is 00FFFC,D.(see Table 6-1 for Vectors)
    /// - PC is loaded with the contents of 00FFFC,D
    pub reset: bool,

    /// # Datasheet
    ///
    /// The Ready is a bi-directional signal. When it is an output it indicates that a Wait for Interrupt instruction has
    /// been executed halting operation of the microprocessor. A low input logic level will halt the microprocessor
    /// in its current state. Returning RDY to the active high state releases the microprocessor to continue
    /// processing following the next PHI2 negative transition. The RDY signal is internally pulled low following the
    /// execution of a Wait for Interrupt instruction, and then returned to the high state when a RESB, ABORTB,
    /// NMIB, or IRQB external interrupt is active. This feature may be used to reduce interrupt latency by
    /// executing the WAI instruction and waiting for an interrupt to begin processing. If the IRQB Disable flag has
    /// been set, the next instruction will be executed when the IRQB occurs. The processor will not stop after a
    /// WAI instruction if RDY has been forced to a high state. The STP instruction has no effect on RDY. The
    /// RDY pin has an active pull-up and when outputting a low level, the pull-up is turned off to reduce power.
    /// The RDY pin can be wired ORed.
    pub ready: Option<bool>,

    /// # Datasheet
    /// The Bus Enable input signal allows external control of the Address and Data Buffers, as well as the RWB
    /// signal. With Bus Enable high, the RWB and Address Buffers are active. The Data/Address Buffers are
    /// active during the first half of every cycle and the second half of a write cycle. When BE is low, these buffers
    /// are disabled. Bus Enable is an asynchronous signal.
    pub bus_enable: bool,
}

#[derive(Copy, Clone, Debug)]
pub struct CpuOutputs {
    /// The full 24-bit address bus, including the bank that is normally on the databus for the
    /// first half-clock cycle. The address bus is written when it is Some(...). When None, it is
    /// not (left "floating") for something else to write to
    ///
    /// # Datasheet
    /// The sixteen Address Bus output lines along with the bank address (multiplexed on the first half cycle of the
    /// Data Bus (D0-D7) pins) form the 24-bit Address Bus for memory and I/O exchange on the Data Bus. When
    /// using the W65C816S, the address lines may be set to the high impedance state by the Bus Enable (BE)
    /// signal.
    pub address_bus: AddressBus,

    /// The output side of the 8-bit data bus. This is only the data bus and does not include the
    /// high 8 bits of the bank select for the address bus of the first half of the cycle. If this
    /// is None, the bus is not written to and something else can write to it.
    ///
    /// # Datasheet
    ///
    /// The Data/Bank Address Bus pins provide both the Bank Address and Data. The bank address is present
    /// during the first half of a memory cycle, and the data value is read or written during the second half of the
    /// memory cycle. Two memory cycles are required to transfer 16-bit values. These lines may be set to the
    /// high impedance state by the Bus Enable (BE) signal.
    pub data_bus: DataBus,

    /// # Datasheet
    ///
    /// The Ready is a bi-directional signal. When it is an output it indicates that a Wait for Interrupt instruction has
    /// been executed halting operation of the microprocessor. A low input logic level will halt the microprocessor
    /// in its current state. Returning RDY to the active high state releases the microprocessor to continue
    /// processing following the next PHI2 negative transition. The RDY signal is internally pulled low following the
    /// execution of a Wait for Interrupt instruction, and then returned to the high state when a RESB, ABORTB,
    /// NMIB, or IRQB external interrupt is active. This feature may be used to reduce interrupt latency by
    /// executing the WAI instruction and waiting for an interrupt to begin processing. If the IRQB Disable flag has
    /// been set, the next instruction will be executed when the IRQB occurs. The processor will not stop after a
    /// WAI instruction if RDY has been forced to a high state. The STP instruction has no effect on RDY. The
    /// RDY pin has an active pull-up and when outputting a low level, the pull-up is turned off to reduce power.
    /// The RDY pin can be wired ORed.
    pub ready: Option<bool>,

    /// # Datasheet
    ///
    /// The Read/Write output signal is used to control whether the microprocessor is "Reading" or "Writing" to
    /// memory. When the RWB is in the high state, the microprocessor is reading data from memory or I/O.
    /// When RBW is low the Data Bus contains valid data from the microprocessor which is to written to the
    /// addressed memory location. The RWB signal is set to the high impedance state when Bus Enable is low.
    pub read_write: ReadWrite,

    /// # Datasheet
    ///
    /// The Valid Data Address and Valid Program Address output signals indicate valid memory addresses when
    /// high and are used for memory or I/O address qualification.
    ///
    /// VDA=0 VPA=0 -> Internal Operation Address and Data Bus available. The Address Bus may be invalid.
    /// VDA=0 VPA=1 -> Valid program address-may be used for program cache control.
    /// VDA=1 VPA=0 -> Valid data address-may be used for data cache control.
    /// VDA=1 VPA=1 -> Opcode fetch-may be used for program cache control and single step control.
    pub valid_program_address: bool,

    /// # Datasheet
    ///
    /// The Valid Data Address and Valid Program Address output signals indicate valid memory addresses when
    /// high and are used for memory or I/O address qualification.
    ///
    /// VDA=0 VPA=0 -> Internal Operation Address and Data Bus available. The Address Bus may be invalid.
    /// VDA=0 VPA=1 -> Valid program address-may be used for program cache control.
    /// VDA=1 VPA=0 -> Valid data address-may be used for data cache control.
    /// VDA=1 VPA=1 -> Opcode fetch-may be used for program cache control and single step control.
    pub valid_data_address: bool,

    /// # Datasheet
    ///
    /// The Memory Lock active low output may be used to ensure the integrity of Read Modify Write instructions in
    /// a multiprocessor system. Memory Lock indicates the need to defer arbitration of the next bus cycle.
    /// Memory Lock is low during the last three or five cycles of ASL, DEC, INC, LSR, ROL, ROR, TRB, and TSB
    /// memory referencing instructions, depending on the state of the M flag.
    pub memory_lock: bool,

    /// # Datasheet
    ///
    /// The Vector Pull active low output indicates that a vector location is being addressed during an interrupt
    /// sequence. VPB is low during the last two interrupt sequence cycles, during which time the processor loads
    /// the PC with the interrupt handler vector location. The VPB signal may be used to select and prioritize
    /// interrupts from several sources by modifying the vector addresses.
    pub vector_pull: bool,

    /// # Datasheet
    ///
    /// The Emulation Status output reflects the state of the Emulation (E) mode flag in the Processor Status (P)
    /// Register. This signal may be thought of as an opcode extension and used for memory and system
    /// management.
    pub emulation_status: bool,

    /// # Datasheet
    ///
    /// The Memory/Index Select Status multiplexed output reflects the state of the Accumulator (M) and Index (X)
    /// elect flags (bits 5 and 4 of the Processor Status (P) Register. Flag M is valid during PHI2 negative transition
    /// and Flag X is valid during PHI2 positive transition. These bits may be thought of as opcode extensions and
    /// may be used for memory and system management.
    pub memory_select: bool,

    /// # Datasheet
    ///
    /// The Memory/Index Select Status multiplexed output reflects the state of the Accumulator (M) and Index (X)
    /// elect flags (bits 5 and 4 of the Processor Status (P) Register. Flag M is valid during PHI2 negative transition
    /// and Flag X is valid during PHI2 positive transition. These bits may be thought of as opcode extensions and
    /// may be used for memory and system management.
    pub index_select: bool,
}

#[allow(unused)]
impl CpuOutputs {
    fn default_from_registers(r: CpuRegisters) -> CpuOutputs {
        CpuOutputs {
            address_bus: AddressBus::invalid(),
            data_bus: DataBus::invalid(),
            ready: None,
            read_write: ReadWrite::Read,
            valid_program_address: false,
            valid_data_address: false,
            memory_lock: true,
            vector_pull: true,
            emulation_status: r.processor_status.emulation,
            memory_select: r.processor_status.memory_select,
            index_select: r.processor_status.index_register_select,
        }
    }

    fn address_bus(mut self, address_bus: AddressBus) -> CpuOutputs {
        self.address_bus = address_bus;
        self
    }

    fn data_bus(mut self, data_bus: DataBus) -> CpuOutputs {
        self.data_bus = data_bus;
        self
    }

    fn ready(mut self, ready: Option<bool>) -> CpuOutputs {
        self.ready = ready;
        self
    }

    fn read_write(mut self, read_write: ReadWrite) -> CpuOutputs {
        self.read_write = read_write;
        self
    }

    fn valid_program_address(mut self, vpa: bool) -> CpuOutputs {
        self.valid_program_address = vpa;
        self
    }

    fn valid_data_address(mut self, vda: bool) -> CpuOutputs {
        self.valid_data_address = vda;
        self
    }

    fn memory_lock(mut self, memory_lock: bool) -> CpuOutputs {
        self.memory_lock = memory_lock;
        self
    }

    fn vector_pull(mut self, vector_pull: bool) -> CpuOutputs {
        self.vector_pull = vector_pull;
        self
    }
}

#[derive(Copy, Clone, Debug)]
pub struct ProcessorStatus {
    pub negative: bool,
    pub overflow: bool,
    pub memory_select: bool,
    pub index_register_select: bool,
    pub brk: bool,
    pub decimal_mode: bool,
    pub irq_disable: bool,
    pub zero: bool,
    pub carry: bool,
    pub emulation: bool,
}

impl ProcessorStatus {
    pub fn update_from_byte(&mut self, b: u8) {
        self.negative = b & (1 << 7) != 0;
        self.overflow = b & (1 << 6) != 0;
        self.memory_select = b & (1 << 5) != 0;
        self.index_register_select = b & (1 << 4) != 0;
        self.decimal_mode = b & (1 << 3) != 0;
        self.irq_disable = b & (1 << 2) != 0;
        self.zero = b & (1 << 1) != 0;
        self.carry = b & (1 << 0) != 0;
    }
    pub fn to_byte(self) -> u8 {
        (self.negative as u8) << 7
            | (self.overflow as u8) << 6
            | (self.memory_select as u8) << 5
            | (self.index_register_select as u8) << 4
            | (self.decimal_mode as u8) << 3
            | (self.irq_disable as u8) << 2
            | (self.zero as u8) << 1
            | self.carry as u8
    }
}

#[derive(Copy, Clone, Debug)]
pub struct CpuRegisters {
    pub index_x: u16,
    pub index_y: u16,
    pub stack_pointer: u16,
    pub accumulator: u16,
    pub program_counter: u16,
    pub direct: u16,
    pub program_bank: u8,
    pub data_bank: u8,
    pub mdr: u8,
    pub processor_status: ProcessorStatus,
}

#[derive(Copy, Clone, Debug)]
pub enum CpuError {
    UnhandledOpcode,
    InvalidCycle(u8),
    InvalidDataBus,
}

impl From<DataBusInvalid> for CpuError {
    fn from(_: DataBusInvalid) -> Self {
        CpuError::InvalidDataBus
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Cpu65816 {
    pub registers: CpuRegisters,

    pub cycle: u8,
    pub opcode: u8,
    pub operand: [u8; 4],
}

impl Cpu65816 {
    pub fn new(exception_vectors: ExceptionVectors) -> Cpu65816 {
        Cpu65816 {
            registers: CpuRegisters {
                index_x: 0x00,
                index_y: 0x00,
                stack_pointer: 0x00,
                accumulator: 0x00,
                program_counter: exception_vectors.reset_emu,
                direct: 0x00,
                program_bank: 0x00,
                data_bank: 0x00,
                mdr: 0x00,
                processor_status: ProcessorStatus {
                    negative: false,
                    overflow: false,
                    memory_select: true,
                    index_register_select: true,
                    brk: false,
                    decimal_mode: false,
                    irq_disable: true,
                    zero: false,
                    carry: false,
                    emulation: true,
                },
            },
            cycle: 0,
            opcode: 0,
            operand: [0; 4],
        }
    }

    pub fn clock_rising(&mut self) -> Result<CpuOutputs, CpuError> {
        if self.cycle == 0 {
            Ok(CpuOutputs::default_from_registers(self.registers)
                .address_bus(AddressBus::from_bank_addr(
                    self.registers.program_bank,
                    self.registers.program_counter,
                ))
                .valid_program_address(true)
                .valid_data_address(true))
        } else {
            self.control_rising()
        }
    }

    pub fn clock_falling(&mut self, inputs: CpuInputs) -> Result<(), CpuError> {
        if let Ok(data) = inputs.data_bus.valid() {
            self.registers.mdr = data;
        }

        if self.cycle == 0 {
            self.opcode = self.registers.mdr;
            self.cycle = 1;
            Ok(())
        } else {
            self.control_falling()
        }
    }

    fn control_rising(&mut self) -> Result<CpuOutputs, CpuError> {
        match self.opcode {
            opcodes::ADC_IMMEDIATE => control::AdcImmediate::default().clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::BRA => control::Bra::default().clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::BEQ => control::Beq::default().clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::BNE => control::Bne::default().clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::BVS => control::Bvs::default().clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::CLI => {
                control::Cli.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::CMP_ABSOLUTE => control::CmpAbsolute.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::CMP_IMMEDIATE => control::CmpImmediate::default().clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::CPX_IMMEDIATE => control::CpxImmediate::default().clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::CPY_IMMEDIATE => control::CpyImmediate::default().clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::DEC_ACCUMULATOR => control::DecAccumulator::default().clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::DEX => {
                control::Dex.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::DEY => {
                control::Dey.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::INC_ACCUMULATOR => control::IncAccumulator::default().clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::INC_DIRECT_PAGE => control::IncDirect::default().clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::INX => {
                control::Inx.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::INY => {
                control::Iny.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::LDA_ABSOLUTE => control::LdaAbsolute.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::LDA_ABSOLUTE_LONG => control::LdaAbsoluteLong::default().clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::LDA_DIRECT_PAGE => control::LdaDirect.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::LDA_DP_INDIRECT_LONG_INDEXED_Y => control::LdaDirectIndirectLongY
                .clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand),
            opcodes::LDA_IMMEDIATE => control::LdaImmediate.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::LDX_ABSOLUTE => control::LdxAbsolute.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::LDX_IMMEDIATE => control::LdxImmediate.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::LDY_ABSOLUTE => control::LdyAbsolute.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::LDY_IMMEDIATE => control::LdyImmediate.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::CLC => {
                control::Clc.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::CLD => {
                control::Cld.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::JSR_ABSOLUTE => control::JsrAbsolute.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::ORA_ABSOLUTE => control::OraAbsolute.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::PHA => {
                control::Pha.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::PHP => {
                control::Php.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::PLA => {
                control::Pla.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::PLP => {
                control::Plp.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::REP => {
                control::Rep.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::ROL_ACCUMULATOR => control::RolAccumulator::default().clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::RTS => {
                control::Rts.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::SBC_IMMEDIATE => control::SbcImmediate::default().clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::SEC => {
                control::Sec.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::SEP => {
                control::Sep.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::SEI => {
                control::Sei.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::STA_ABSOLUTE => control::StaAbsolute.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::STA_ABSOLUTE_LONG => control::StaAbsoluteLong.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::STA_ABSOLUTE_INDEXED_X => control::StaAbsoluteIndexedX.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::STA_DIRECT_PAGE => control::StaDirect.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::STY_ABSOLUTE => control::StyAbsolute.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::STY_DIRECT_PAGE => control::StyDirect.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::STZ_ABSOLUTE => control::StzAbsolute.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::TAX => {
                control::Tax.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::TCD => {
                control::Tcd.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::TCS => {
                control::Tcs.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::TXA => {
                control::Txa.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::XBA => {
                control::Xba.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::XCE => {
                control::Xce.clock_rising(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            _ => control::Unhandled.clock_rising(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
        }
    }

    fn control_falling(&mut self) -> Result<(), CpuError> {
        match self.opcode {
            opcodes::ADC_IMMEDIATE => control::AdcImmediate::default().clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::BRA => control::Bra::default().clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::BEQ => control::Beq::default().clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::BNE => control::Bne::default().clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::BVS => control::Bvs::default().clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::CLI => {
                control::Cli.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::CMP_ABSOLUTE => control::CmpAbsolute.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::CMP_IMMEDIATE => control::CmpImmediate::default().clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::CPX_IMMEDIATE => control::CpxImmediate::default().clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::CPY_IMMEDIATE => control::CpyImmediate::default().clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::DEC_ACCUMULATOR => control::DecAccumulator::default().clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::DEX => {
                control::Dex.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::DEY => {
                control::Dey.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::INC_ACCUMULATOR => control::IncAccumulator::default().clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::INC_DIRECT_PAGE => control::IncDirect::default().clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::INX => {
                control::Inx.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::INY => {
                control::Iny.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::LDA_ABSOLUTE => control::LdaAbsolute.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::LDA_ABSOLUTE_LONG => control::LdaAbsoluteLong::default().clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::LDA_DIRECT_PAGE => control::LdaDirect.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::LDA_DP_INDIRECT_LONG_INDEXED_Y => control::LdaDirectIndirectLongY
                .clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand),
            opcodes::LDA_IMMEDIATE => control::LdaImmediate.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::LDX_ABSOLUTE => control::LdxAbsolute.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::LDX_IMMEDIATE => control::LdxImmediate.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::LDY_ABSOLUTE => control::LdyAbsolute.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::LDY_IMMEDIATE => control::LdyImmediate.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::CLC => {
                control::Clc.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::CLD => {
                control::Cld.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::JSR_ABSOLUTE => control::JsrAbsolute.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::ORA_ABSOLUTE => control::OraAbsolute.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::PHA => {
                control::Pha.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::PHP => {
                control::Php.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::PLA => {
                control::Pla.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::PLP => {
                control::Plp.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::REP => {
                control::Rep.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::ROL_ACCUMULATOR => control::RolAccumulator::default().clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::RTS => {
                control::Rts.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::SBC_IMMEDIATE => control::SbcImmediate::default().clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::SEC => {
                control::Sec.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::SEP => {
                control::Sep.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::SEI => {
                control::Sei.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::STA_ABSOLUTE => control::StaAbsolute.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::STA_ABSOLUTE_LONG => control::StaAbsoluteLong.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::STA_ABSOLUTE_INDEXED_X => control::StaAbsoluteIndexedX.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::STA_DIRECT_PAGE => control::StaDirect.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::STY_ABSOLUTE => control::StyAbsolute.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::STY_DIRECT_PAGE => control::StyDirect.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::STZ_ABSOLUTE => control::StzAbsolute.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
            opcodes::TAX => {
                control::Tax.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::TCD => {
                control::Tcd.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::TCS => {
                control::Tcs.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::TXA => {
                control::Txa.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::XBA => {
                control::Xba.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            opcodes::XCE => {
                control::Xce.clock_falling(&mut self.registers, &mut self.cycle, &mut self.operand)
            }
            _ => control::Unhandled.clock_falling(
                &mut self.registers,
                &mut self.cycle,
                &mut self.operand,
            ),
        }
    }
}
