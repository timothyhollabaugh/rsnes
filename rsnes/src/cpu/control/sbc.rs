use super::prelude::*;

pub type SbcImmediate = helpers::ArithmaticImmediateImpl<SbcImmediateImpl>;

#[derive(Copy, Clone, Debug, Default)]
pub struct SbcImmediateImpl;

impl helpers::ArithmaticImmediate for SbcImmediateImpl {
    fn emulation(
        &mut self,
        operand: u8,
        accumulator: &mut u8,
        status: &mut crate::cpu::ProcessorStatus,
    ) {
        *accumulator = alu::emulation::sub_carry(*accumulator, operand, status);
    }

    fn native(
        &mut self,
        operand: u16,
        accumulator: &mut u16,
        status: &mut crate::cpu::ProcessorStatus,
    ) {
        *accumulator = alu::native::sub_carry(*accumulator, operand, status);
    }
}

#[cfg(test)]
mod immediate_test {
    use crate::cpu::{control::helpers, test::*};

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;

        helpers::arithmatic_immediate_test::emulation(&mut cpu, opcodes::SBC_IMMEDIATE, 0x15);

        assert_eq!(cpu.registers.accumulator, 0xb5, "Accumulator");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
        assert!(!cpu.registers.processor_status.carry, "Carry flag");
    }

    #[test]
    fn emulation_carry() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;

        helpers::arithmatic_immediate_test::emulation(&mut cpu, opcodes::SBC_IMMEDIATE, 0xd5);

        assert_eq!(cpu.registers.accumulator, 0xf5, "Accumulator");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
        assert!(cpu.registers.processor_status.carry, "Carry flag");
    }

    #[test]
    fn emulation_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;

        helpers::arithmatic_immediate_test::emulation(&mut cpu, opcodes::SBC_IMMEDIATE, 0xca);

        assert_eq!(cpu.registers.accumulator, 0x00, "Accumulator");
        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
        assert!(!cpu.registers.processor_status.carry, "Carry flag");
    }

    #[test]
    fn native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xd546;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        helpers::arithmatic_immediate_test::native(&mut cpu, opcodes::SBC_IMMEDIATE, 0xcab2);

        assert_eq!(cpu.registers.accumulator, 0x0a94, "Accumulator");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
        assert!(!cpu.registers.processor_status.carry, "Carry flag");
    }

    #[test]
    fn native_carry() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        helpers::arithmatic_immediate_test::native(&mut cpu, opcodes::SBC_IMMEDIATE, 0xd546);

        assert_eq!(cpu.registers.accumulator, 0xf56c, "Accumulator");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
        assert!(cpu.registers.processor_status.carry, "Carry flag");
    }

    #[test]
    fn native_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        helpers::arithmatic_immediate_test::native(&mut cpu, opcodes::SBC_IMMEDIATE, 0xcab2);

        assert_eq!(cpu.registers.accumulator, 0x00, "Accumulator");
        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
        assert!(!cpu.registers.processor_status.carry, "Carry flag");
    }
}
