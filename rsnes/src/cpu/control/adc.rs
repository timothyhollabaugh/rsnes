use super::prelude::*;

pub type AdcImmediate = helpers::ArithmaticImmediateImpl<AdcImmediateImpl>;

#[derive(Copy, Clone, Debug, Default)]
pub struct AdcImmediateImpl;

impl helpers::ArithmaticImmediate for AdcImmediateImpl {
    fn emulation(
        &mut self,
        operand: u8,
        accumulator: &mut u8,
        status: &mut crate::cpu::ProcessorStatus,
    ) {
        *accumulator = alu::emulation::add_carry(*accumulator, operand, status);
    }

    fn native(
        &mut self,
        operand: u16,
        accumulator: &mut u16,
        status: &mut crate::cpu::ProcessorStatus,
    ) {
        *accumulator = alu::native::add_carry(*accumulator, operand, status);
    }
}

#[cfg(test)]
mod immediate_test {
    use crate::cpu::{control::helpers, test::*};

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;

        helpers::arithmatic_immediate_test::emulation(&mut cpu, opcodes::ADC_IMMEDIATE, 0x15);

        assert_eq!(cpu.registers.accumulator, 0xdf, "Accumulator");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
        assert!(!cpu.registers.processor_status.carry, "Carry flag");
    }

    #[test]
    fn emulation_carry() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;

        helpers::arithmatic_immediate_test::emulation(&mut cpu, opcodes::ADC_IMMEDIATE, 0xd5);

        assert_eq!(cpu.registers.accumulator, 0x9f, "Accumulator");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
        assert!(cpu.registers.processor_status.carry, "Carry flag");
    }

    #[test]
    fn zero_emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0x36;

        helpers::arithmatic_immediate_test::emulation(&mut cpu, opcodes::ADC_IMMEDIATE, 0xca);

        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
        assert!(cpu.registers.processor_status.carry, "Carry flag");
    }

    #[test]
    fn native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        helpers::arithmatic_immediate_test::native(&mut cpu, opcodes::ADC_IMMEDIATE, 0xd546);

        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
        assert!(cpu.registers.processor_status.carry, "Carry flag");
    }

    #[test]
    fn zero_native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0x354e;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        helpers::arithmatic_immediate_test::native(&mut cpu, opcodes::ADC_IMMEDIATE, 0xcab2);

        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
        assert!(cpu.registers.processor_status.carry, "Carry flag");
    }
}
