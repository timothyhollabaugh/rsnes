use super::prelude::*;

#[derive(Copy, Clone, Debug, Default)]
pub struct CpyImmediate;

impl CpuControl for CpyImmediate {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => helpers::read_operand_rising(0, registers),
            2 => helpers::read_operand_rising(1, registers),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => {
                operand[2] = registers.mdr;

                if registers.processor_status.emulation
                    || registers.processor_status.index_register_select
                {
                    let result = u8::wrapping_sub((registers.index_y & 0x00ff) as u8, operand[2]);
                    registers.processor_status.zero = result == 0;
                    registers.processor_status.negative = result & 0x80 != 0;
                    registers.processor_status.carry =
                        (registers.index_y & 0x00ff) as u8 >= operand[2];

                    registers.program_counter += 2;
                    *cycle = 0;
                } else {
                    *cycle = 2;
                }

                Ok(())
            }
            2 => {
                operand[3] = registers.mdr;

                let o = u16::from_le_bytes(operand[2..4].try_into().unwrap());
                let result = u16::wrapping_sub(registers.index_y, o);
                registers.processor_status.zero = result == 0;
                registers.processor_status.negative = result & 0x8000 != 0;
                registers.processor_status.carry = registers.index_y >= o;

                registers.program_counter += 3;
                *cycle = 0;

                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod cpy_immediate_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.index_y = 0xca;
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcodes::CPY_IMMEDIATE;
        memory[0x8001] = 0xd5;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
        assert!(!cpu.registers.processor_status.carry, "Carry flag");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn zero_emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.index_y = 0xca;
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcodes::CPY_IMMEDIATE;
        memory[0x8001] = 0xca;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
        assert!(cpu.registers.processor_status.carry, "Carry flag");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.index_y = 0xcab2;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.index_register_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::CPY_IMMEDIATE;
        memory[0x8001] = 0x46;
        memory[0x8002] = 0xd5;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
        assert!(!cpu.registers.processor_status.carry, "Carry flag");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
    }

    #[test]
    fn zero_native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.index_y = 0xcab2;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.index_register_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::CPY_IMMEDIATE;
        memory[0x8001] = 0xb2;
        memory[0x8002] = 0xca;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
        assert!(cpu.registers.processor_status.carry, "Carry flag");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
    }
}
