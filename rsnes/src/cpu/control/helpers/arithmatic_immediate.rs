use crate::cpu::ProcessorStatus;

use super::super::prelude::*;

/// Helper trait for implementing CpuControl
///
/// This is for any instructions that use the immidiate addressing mode, and store the result in
/// the accumulator. Either the emulation or native function is called
/// based on whether the memory select or emulation flags are set
pub trait ArithmaticImmediate: std::fmt::Debug + std::default::Default {
    /// Perform this instruction's operation when in emulation mode (memory select or emulation
    /// flags set). Called on the rising edge of the clock.
    fn emulation(&mut self, operand: u8, accumulator: &mut u8, status: &mut ProcessorStatus);

    /// Perform this instruction's operation when in native mode (memory select and emulation
    /// flags clear). Called on the rising edge of the clock.
    fn native(&mut self, operand: u16, accumulator: &mut u16, status: &mut ProcessorStatus);
}

#[derive(Copy, Clone, Debug, Default)]
pub struct ArithmaticImmediateImpl<T>(T);

impl<T: ArithmaticImmediate> CpuControl for ArithmaticImmediateImpl<T> {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => helpers::read_operand_rising(0, registers),
            2 => helpers::read_operand_rising(1, registers),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => {
                operand[2] = registers.mdr;

                if registers.processor_status.emulation || registers.processor_status.memory_select
                {
                    let mut a = registers.accumulator as u8;
                    self.0
                        .emulation(operand[2], &mut a, &mut registers.processor_status);
                    registers.accumulator &= !0x00ff;
                    registers.accumulator |= a as u16;

                    registers.program_counter += 2;
                    *cycle = 0;
                } else {
                    *cycle = 2;
                }

                Ok(())
            }
            2 => {
                operand[3] = registers.mdr;

                let o = u16::from_le_bytes(operand[2..4].try_into().unwrap());
                self.0.native(
                    o,
                    &mut registers.accumulator,
                    &mut registers.processor_status,
                );

                registers.program_counter += 3;
                *cycle = 0;

                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
pub mod arithmatic_immediate_test {
    use crate::cpu::{test::clock_cpu, Cpu65816};

    pub fn emulation(cpu: &mut Cpu65816, opcode: u8, immediate: u8) {
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcode;
        memory[0x8001] = immediate;

        for _ in 0..2 {
            clock_cpu(cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    pub fn native(cpu: &mut Cpu65816, opcode: u8, immediate: u16) {
        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcode;
        memory[0x8001] = immediate as u8;
        memory[0x8002] = (immediate >> 8) as u8;

        for _ in 0..3 {
            clock_cpu(cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
    }
}
