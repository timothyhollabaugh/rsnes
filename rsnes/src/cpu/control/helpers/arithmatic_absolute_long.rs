use crate::cpu::ProcessorStatus;

use super::super::prelude::*;

pub trait ArithmaticAbsoluteLong: std::fmt::Debug + std::default::Default {
    fn emulation(&mut self, operand: u8, accumulator: &mut u8, status: &mut ProcessorStatus);
    fn native(&mut self, operand: u16, accumulator: &mut u16, status: &mut ProcessorStatus);
}

#[derive(Copy, Clone, Debug, Default)]
pub struct ArithmaticAbsoluteLongImpl<T>(T);

impl<T: ArithmaticAbsoluteLong> CpuControl for ArithmaticAbsoluteLongImpl<T> {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => Ok(CpuOutputs::default_from_registers(*registers).address_bus(
                AddressBus::from_bank_addr(registers.program_bank, registers.program_counter + 1),
            )),
            2 => Ok(CpuOutputs::default_from_registers(*registers).address_bus(
                AddressBus::from_bank_addr(registers.program_bank, registers.program_counter + 2),
            )),
            3 => Ok(CpuOutputs::default_from_registers(*registers).address_bus(
                AddressBus::from_bank_addr(registers.program_bank, registers.program_counter + 3),
            )),
            4 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    operand[2],
                    u16::from_le_bytes(operand[0..2].try_into().unwrap()),
                ))
                .read_write(ReadWrite::Read)),
            5 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    operand[2],
                    u16::from_le_bytes(operand[0..2].try_into().unwrap()) + 1,
                ))
                .read_write(ReadWrite::Read)),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match *cycle {
            1 => {
                operand[0] = registers.mdr;
                *cycle = 2;
                Ok(())
            }
            2 => {
                operand[1] = registers.mdr;
                *cycle = 3;
                Ok(())
            }
            3 => {
                operand[2] = registers.mdr;
                *cycle = 4;
                Ok(())
            }
            4 => {
                if registers.processor_status.emulation || registers.processor_status.memory_select
                {
                    let mut a = registers.accumulator as u8;
                    self.0
                        .emulation(registers.mdr, &mut a, &mut registers.processor_status);
                    registers.accumulator &= !0x00ff;
                    registers.accumulator |= a as u16;

                    registers.program_counter += 4;
                    *cycle = 0;
                } else {
                    operand[3] = registers.mdr;
                    *cycle = 5;
                }

                Ok(())
            }
            5 => {
                let o = u16::from_le_bytes([operand[3], registers.mdr]);
                self.0.native(
                    o,
                    &mut registers.accumulator,
                    &mut registers.processor_status,
                );

                registers.program_counter += 4;
                *cycle = 0;

                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
pub mod test {
    use crate::cpu::{test::clock_cpu, Cpu65816};

    pub fn emulation(cpu: &mut Cpu65816, opcode: u8, b: u8) {
        let mut memory = [0x00; 0x02ffff];
        memory[0x8000] = opcode;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x8003] = 0x02;
        memory[0x020201] = b;

        for _ in 0..6 {
            clock_cpu(cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.program_counter, 0x8004, "Program counter");
    }

    pub fn native(cpu: &mut Cpu65816, opcode: u8, b: u16) {
        let mut memory = [0x00; 0x02ffff];
        memory[0x8000] = opcode;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x8003] = 0x02;
        memory[0x020201] = b as u8;
        memory[0x020202] = (b >> 8) as u8;

        for _ in 0..6 {
            clock_cpu(cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.program_counter, 0x8004, "Program counter");
    }
}
