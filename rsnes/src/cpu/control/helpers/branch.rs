use super::super::prelude::*;
use crate::cpu::ProcessorStatus;

pub trait BranchTest: std::fmt::Debug + std::default::Default {
    fn test(&mut self, status: &mut ProcessorStatus) -> bool;
}

#[derive(Copy, Clone, Debug, Default)]
pub struct BranchTestImpl<T>(T);

impl<T: BranchTest> CpuControl for BranchTestImpl<T> {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => helpers::read_operand_rising(0, registers),
            2 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    registers.program_bank,
                    registers.program_counter + 1,
                ))
                .valid_data_address(false)
                .valid_program_address(false)),
            3 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    registers.program_bank,
                    registers.program_counter + 1,
                ))
                .valid_data_address(false)
                .valid_program_address(false)),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => {
                operand[0] = registers.mdr;

                if self.0.test(&mut registers.processor_status) {
                    *cycle += 1;
                } else {
                    registers.program_counter += 2;
                    *cycle = 0;
                }

                Ok(())
            }
            2 => {
                let operand_address = registers.program_counter + 1;
                let new_program_counter =
                    (registers.program_counter as i32 + 2 + (operand[0] as i8) as i32) as u16;
                if operand_address & 0xff00 == new_program_counter & 0xff00 {
                    registers.program_counter = new_program_counter;
                    *cycle = 0;
                } else {
                    *cycle = 3;
                }
                Ok(())
            }
            3 => {
                let new_program_counter =
                    (registers.program_counter as i32 + 2 + (operand[0] as i8) as i32) as u16;
                registers.program_counter = new_program_counter;
                *cycle = 0;
                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
pub mod branch_test {
    use crate::cpu::{test::clock_cpu, Cpu65816};

    pub fn no_page_boundary(cpu: &mut Cpu65816, opcode: u8, should_branch: bool) {
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcode;
        memory[0x8001] = 0x02;

        let cycles = if should_branch { 3 } else { 2 };

        for _ in 0..cycles {
            clock_cpu(cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        if should_branch {
            assert_eq!(cpu.registers.program_counter, 0x8004, "Program counter");
        } else {
            assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
        }
    }

    pub fn page_boundary(cpu: &mut Cpu65816, opcode: u8, should_branch: bool) {
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcode;
        memory[0x8001] = 0xfd;

        let cycles = if should_branch { 4 } else { 2 };

        for _ in 0..cycles {
            clock_cpu(cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);
        if should_branch {
            assert_eq!(cpu.registers.program_counter, 0x7fff, "Program counter");
        } else {
            assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
        }
    }
}
