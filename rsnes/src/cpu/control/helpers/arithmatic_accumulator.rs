use crate::cpu::ProcessorStatus;

use super::super::prelude::*;

pub trait ArithmaticAccumulator: std::fmt::Debug + std::default::Default {
    fn emulation(&mut self, accumulator: &mut u8, status: &mut ProcessorStatus);
    fn native(&mut self, accumulator: &mut u16, status: &mut ProcessorStatus);
}

#[derive(Copy, Clone, Debug, Default)]
pub struct ArithmaticAccumulatorImpl<T>(T);

impl<T: ArithmaticAccumulator> CpuControl for ArithmaticAccumulatorImpl<T> {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        if registers.processor_status.memory_select || registers.processor_status.emulation {
            let mut accumulator_low = registers.accumulator as u8;
            self.0
                .emulation(&mut accumulator_low, &mut registers.processor_status);
            registers.accumulator &= !0x00ff;
            registers.accumulator |= accumulator_low as u16;
        } else {
            self.0
                .native(&mut registers.accumulator, &mut registers.processor_status);
        }
        Ok(CpuOutputs::default_from_registers(*registers))
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        registers.program_counter += 1;
        *cycle = 0;
        Ok(())
    }
}

#[cfg(test)]
pub use test::arithmatic_accumulator as test_arithmatic_accumulator;

#[cfg(test)]
mod test {
    use crate::cpu::{test::clock_cpu, Cpu65816};

    pub fn arithmatic_accumulator(cpu: &mut Cpu65816, opcode: u8) {
        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcode;

        for _ in 0..2 {
            clock_cpu(cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");

        if cpu.registers.processor_status.memory_select || cpu.registers.processor_status.emulation
        {
            assert_eq!(
                cpu.registers.processor_status.zero,
                cpu.registers.accumulator & 0x00ff == 0
            );
            assert_eq!(
                cpu.registers.processor_status.negative,
                cpu.registers.accumulator & 0x0080 != 0
            );
        } else {
            assert_eq!(
                cpu.registers.processor_status.zero,
                cpu.registers.accumulator == 0
            );
            assert_eq!(
                cpu.registers.processor_status.negative,
                cpu.registers.accumulator & 0x8000 != 0
            );
        }
    }
}
