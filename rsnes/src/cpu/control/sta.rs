use super::prelude::*;

#[derive(Clone, Copy, Debug)]
pub struct StaAbsolute;

impl CpuControl for StaAbsolute {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => Ok(CpuOutputs::default_from_registers(*registers).address_bus(
                AddressBus::from_bank_addr(registers.program_bank, registers.program_counter + 1),
            )),
            2 => Ok(CpuOutputs::default_from_registers(*registers).address_bus(
                AddressBus::from_bank_addr(registers.program_bank, registers.program_counter + 2),
            )),
            3 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    registers.data_bank,
                    u16::from_le_bytes(operand[0..2].try_into().unwrap()),
                ))
                .data_bus(DataBus::from_data(registers.accumulator as u8))
                .read_write(ReadWrite::Write)),
            4 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    registers.data_bank,
                    u16::from_le_bytes(operand[0..2].try_into().unwrap()) + 1,
                ))
                .data_bus(DataBus::from_data((registers.accumulator >> 8) as u8))
                .read_write(ReadWrite::Write)),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match *cycle {
            1 => {
                operand[0] = registers.mdr;
                *cycle = 2;
                Ok(())
            }
            2 => {
                operand[1] = registers.mdr;
                *cycle = 3;
                Ok(())
            }
            3 => {
                if registers.processor_status.emulation || registers.processor_status.memory_select
                {
                    *cycle = 0;
                    registers.program_counter += 3;
                } else {
                    *cycle = 4;
                }
                Ok(())
            }
            4 => {
                registers.program_counter += 3;
                *cycle = 0;
                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod absolute_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::STA_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0x35;

        for _ in 0..4 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x0201], 0xca, "Stored Byte");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
    }

    #[test]
    fn zero_emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0x00;
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcodes::STA_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0x35;

        for _ in 0..4 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x0201], 0x00, "Stored Byte 1");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
    }

    #[test]
    fn native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::STA_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0x46;
        memory[0x0202] = 0x35;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x0201], 0xb2, "Stored Byte 1");
        assert_eq!(memory[0x0202], 0xca, "Stored Byte 2");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
    }

    #[test]
    fn zero_native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0x00;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::STA_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0x46;
        memory[0x0202] = 0x35;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x0201], 0x00, "Stored Byte 1");
        assert_eq!(memory[0x0202], 0x00, "Stored Byte 2");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
    }
}

#[derive(Clone, Copy, Debug)]
pub struct StaAbsoluteLong;

impl CpuControl for StaAbsoluteLong {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => Ok(CpuOutputs::default_from_registers(*registers).address_bus(
                AddressBus::from_bank_addr(registers.program_bank, registers.program_counter + 1),
            )),
            2 => Ok(CpuOutputs::default_from_registers(*registers).address_bus(
                AddressBus::from_bank_addr(registers.program_bank, registers.program_counter + 2),
            )),
            3 => Ok(CpuOutputs::default_from_registers(*registers).address_bus(
                AddressBus::from_bank_addr(registers.program_bank, registers.program_counter + 3),
            )),
            4 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    operand[2],
                    u16::from_le_bytes(operand[0..2].try_into().unwrap()),
                ))
                .data_bus(DataBus::from_data(registers.accumulator as u8))
                .read_write(ReadWrite::Write)),
            5 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    operand[2],
                    u16::from_le_bytes(operand[0..2].try_into().unwrap()) + 1,
                ))
                .data_bus(DataBus::from_data((registers.accumulator >> 8) as u8))
                .read_write(ReadWrite::Write)),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match *cycle {
            1 => {
                operand[0] = registers.mdr;
                *cycle = 2;
                Ok(())
            }
            2 => {
                operand[1] = registers.mdr;
                *cycle = 3;
                Ok(())
            }
            3 => {
                operand[2] = registers.mdr;
                *cycle = 4;
                Ok(())
            }
            4 => {
                if registers.processor_status.emulation || registers.processor_status.memory_select
                {
                    *cycle = 0;
                    registers.program_counter += 4;
                } else {
                    *cycle = 5;
                }
                Ok(())
            }
            5 => {
                registers.program_counter += 4;
                *cycle = 0;
                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod absolute_long_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;

        let mut memory = [0x00; 0x02ffff];
        memory[0x8000] = opcodes::STA_ABSOLUTE_LONG;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x8003] = 0x02;
        memory[0x020201] = 0x35;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x020201], 0xca, "Stored Byte");
        assert_eq!(cpu.registers.program_counter, 0x8004, "Program counter");
    }

    #[test]
    fn zero_emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0x00;
        let mut memory = [0x00; 0x02ffff];

        memory[0x8000] = opcodes::STA_ABSOLUTE_LONG;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x8003] = 0x02;
        memory[0x020201] = 0x35;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x020201], 0x00, "Stored Byte 1");
        assert_eq!(cpu.registers.program_counter, 0x8004, "Program counter");
    }

    #[test]
    fn native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0x02ffff];
        memory[0x8000] = opcodes::STA_ABSOLUTE_LONG;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x8003] = 0x02;
        memory[0x020201] = 0x46;
        memory[0x020202] = 0x35;

        for _ in 0..6 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x020201], 0xb2, "Stored Byte 1");
        assert_eq!(memory[0x020202], 0xca, "Stored Byte 2");
        assert_eq!(cpu.registers.program_counter, 0x8004, "Program counter");
    }

    #[test]
    fn zero_native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0x00;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0x02ffff];
        memory[0x8000] = opcodes::STA_ABSOLUTE_LONG;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x8003] = 0x02;
        memory[0x020201] = 0x46;
        memory[0x020202] = 0x35;

        for _ in 0..6 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x020201], 0x00, "Stored Byte 1");
        assert_eq!(memory[0x020202], 0x00, "Stored Byte 2");
        assert_eq!(cpu.registers.program_counter, 0x8004, "Program counter");
    }
}

#[derive(Clone, Copy, Debug)]
pub struct StaAbsoluteIndexedX;

impl CpuControl for StaAbsoluteIndexedX {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    registers.program_bank,
                    registers.program_counter + 1,
                ))
                .valid_program_address(true)
                .valid_data_address(false)),
            2 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    registers.program_bank,
                    registers.program_counter + 2,
                ))
                .valid_program_address(true)
                .valid_data_address(false)),
            3 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    registers.data_bank,
                    u16::from_le_bytes(operand[0..2].try_into().unwrap()) + registers.index_x,
                ))
                .data_bus(DataBus::from_data(registers.accumulator as u8))
                .valid_program_address(false)
                .valid_data_address(false)
                .read_write(ReadWrite::Write)),
            4 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    registers.data_bank,
                    u16::from_le_bytes(operand[0..2].try_into().unwrap()) + registers.index_x,
                ))
                .data_bus(DataBus::from_data(registers.accumulator as u8))
                .valid_program_address(false)
                .valid_data_address(true)
                .read_write(ReadWrite::Write)),
            5 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    registers.data_bank,
                    u16::from_le_bytes(operand[0..2].try_into().unwrap()) + registers.index_x + 1,
                ))
                .data_bus(DataBus::from_data((registers.accumulator >> 8) as u8))
                .valid_program_address(false)
                .valid_data_address(true)
                .read_write(ReadWrite::Write)),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match *cycle {
            1 => {
                operand[0] = registers.mdr;
                *cycle = 2;
                Ok(())
            }
            2 => {
                operand[1] = registers.mdr;
                *cycle = 3;
                Ok(())
            }
            3 => {
                *cycle = 4;
                Ok(())
            }
            4 => {
                if registers.processor_status.emulation || registers.processor_status.memory_select
                {
                    *cycle = 0;
                    registers.program_counter += 3;
                } else {
                    *cycle = 5;
                }
                Ok(())
            }
            5 => {
                registers.program_counter += 3;
                *cycle = 0;
                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod absolute_indexed_x_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        cpu.registers.index_x = 0x04;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::STA_ABSOLUTE_INDEXED_X;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0205] = 0x35;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x0205], 0xca, "Stored Byte");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
    }

    #[test]
    fn zero_emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0x00;
        cpu.registers.index_x = 0x04;
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcodes::STA_ABSOLUTE_INDEXED_X;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0205] = 0x35;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x0205], 0x00, "Stored Byte 1");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
    }

    #[test]
    fn native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.index_x = 0x04;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::STA_ABSOLUTE_INDEXED_X;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0205] = 0x46;
        memory[0x0206] = 0x35;

        for _ in 0..6 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x0205], 0xb2, "Stored Byte 1");
        assert_eq!(memory[0x0206], 0xca, "Stored Byte 2");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
    }

    #[test]
    fn zero_native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0x00;
        cpu.registers.index_x = 0x04;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::STA_ABSOLUTE_INDEXED_X;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0205] = 0x46;
        memory[0x0206] = 0x35;

        for _ in 0..6 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x0205], 0x00, "Stored Byte 1");
        assert_eq!(memory[0x0206], 0x00, "Stored Byte 2");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
    }
}

#[derive(Clone, Copy, Debug)]
pub struct StaDirect;

impl CpuControl for StaDirect {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match *cycle {
            1 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    registers.program_bank,
                    registers.program_counter + 1,
                ))
                .valid_program_address(true)),
            2 => Ok(CpuOutputs::default_from_registers(*registers).address_bus(
                AddressBus::from_bank_addr(registers.program_bank, registers.program_counter + 1),
            )),
            3 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    0,
                    registers.direct + operand[0] as u16,
                ))
                .data_bus(DataBus::from_data(registers.accumulator as u8))
                .valid_data_address(true)
                .read_write(ReadWrite::Write)),
            4 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    0,
                    registers.direct + operand[0] as u16 + 1,
                ))
                .data_bus(DataBus::from_data((registers.accumulator >> 8) as u8))
                .valid_data_address(true)
                .read_write(ReadWrite::Write)),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => {
                operand[0] = registers.mdr;

                if registers.direct & 0x00ff != 0 {
                    *cycle = 2;
                } else {
                    *cycle = 3;
                }

                Ok(())
            }
            2 => {
                *cycle = 3;
                Ok(())
            }
            3 => {
                if registers.processor_status.emulation || registers.processor_status.memory_select
                {
                    *cycle = 0;
                    registers.program_counter += 2;
                } else {
                    *cycle = 4;
                }
                Ok(())
            }
            4 => {
                registers.program_counter += 2;
                *cycle = 0;
                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod direct_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation_direct_low_is_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        cpu.registers.direct = 0x0100;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::STA_DIRECT_PAGE;
        memory[0x8001] = 0xcf;
        memory[0x01cf] = 0x35;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01cf], 0xca, "Stored Byte");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn emulation_direct_low_is_not_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        cpu.registers.direct = 0x0101;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::STA_DIRECT_PAGE;
        memory[0x8001] = 0xcf;
        memory[0x01d0] = 0x35;

        for _ in 0..4 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01d0], 0xca, "Stored Byte");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn native_direct_low_is_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.direct = 0x0100;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::STA_DIRECT_PAGE;
        memory[0x8001] = 0xcf;
        memory[0x01cf] = 0x35;
        memory[0x01d0] = 0x35;

        for _ in 0..4 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01cf], 0xb2, "Stored Byte");
        assert_eq!(memory[0x01d0], 0xca, "Stored Byte");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn native_direct_low_is_not_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.direct = 0x0101;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::STA_DIRECT_PAGE;
        memory[0x8001] = 0xcf;
        memory[0x01d0] = 0x35;
        memory[0x01d1] = 0x35;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01d0], 0xb2, "Stored Byte");
        assert_eq!(memory[0x01d1], 0xca, "Stored Byte");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }
}
