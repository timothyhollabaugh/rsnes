use super::prelude::*;

#[derive(Clone, Copy, Debug)]
pub struct JsrAbsolute;

impl CpuControl for JsrAbsolute {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => helpers::read_operand_rising(0, registers),
            2 => helpers::read_operand_rising(1, registers),
            3 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    registers.program_bank,
                    registers.program_counter + 2,
                ))
                .valid_data_address(false)
                .valid_program_address(false)),
            4 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(0, registers.stack_pointer))
                .data_bus(DataBus::from_data(
                    ((registers.program_counter + 3) >> 8) as u8,
                ))
                .read_write(ReadWrite::Write)),
            5 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(0, registers.stack_pointer - 1))
                .data_bus(DataBus::from_data((registers.program_counter + 3) as u8))
                .read_write(ReadWrite::Write)),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => helpers::read_operand_falling(0, registers, cycle, operand),
            2 => helpers::read_operand_falling(1, registers, cycle, operand),
            3 => {
                *cycle = 4;
                Ok(())
            }
            4 => {
                *cycle = 5;
                Ok(())
            }
            5 => {
                registers.program_counter = u16::from_le_bytes(operand[0..2].try_into().unwrap());
                registers.stack_pointer -= 2;
                *cycle = 0;
                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod absolute_test {
    use crate::cpu::test::*;

    #[test]
    fn native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.stack_pointer = 0x01ff;
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcodes::JSR_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;

        for _ in 0..6 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01ff], 0x80, "Stack top");
        assert_eq!(memory[0x01fe], 0x03, "Stack top");
        assert_eq!(cpu.registers.stack_pointer, 0x01fd, "Stack pointer");
        assert_eq!(cpu.registers.program_counter, 0x0201, "Program counter");
    }
}
