use super::prelude::*;

#[derive(Clone, Copy, Debug)]
pub struct Php;

impl CpuControl for Php {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    registers.program_bank,
                    registers.program_counter + 1,
                ))
                .valid_data_address(false)
                .valid_program_address(false)),
            2 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(0, registers.stack_pointer))
                .data_bus(DataBus::from_data(registers.processor_status.to_byte()))
                .read_write(ReadWrite::Write)
                .valid_data_address(true)),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => {
                *cycle = 2;
                Ok(())
            }
            2 => {
                registers.stack_pointer -= 1;
                registers.program_counter += 1;
                *cycle = 0;
                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod php_test {
    use crate::cpu::test::*;

    #[test]
    fn negative() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.stack_pointer = 0x01ff;
        cpu.registers.processor_status.negative = true;
        cpu.registers.processor_status.overflow = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.processor_status.index_register_select = false;
        cpu.registers.processor_status.brk = false;
        cpu.registers.processor_status.decimal_mode = false;
        cpu.registers.processor_status.irq_disable = false;
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.carry = false;
        cpu.registers.processor_status.emulation = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::PHP;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01ff], 0x80, "Stack top");
        assert_eq!(cpu.registers.stack_pointer, 0x01fe, "Stack pointer");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }

    #[test]
    fn overflow() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.stack_pointer = 0x01ff;
        cpu.registers.processor_status.negative = false;
        cpu.registers.processor_status.overflow = true;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.processor_status.index_register_select = false;
        cpu.registers.processor_status.brk = false;
        cpu.registers.processor_status.decimal_mode = false;
        cpu.registers.processor_status.irq_disable = false;
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.carry = false;
        cpu.registers.processor_status.emulation = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::PHP;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01ff], 0x40, "Stack top");
        assert_eq!(cpu.registers.stack_pointer, 0x01fe, "Stack pointer");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }

    #[test]
    fn memory_select() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.stack_pointer = 0x01ff;
        cpu.registers.processor_status.negative = false;
        cpu.registers.processor_status.overflow = false;
        cpu.registers.processor_status.memory_select = true;
        cpu.registers.processor_status.index_register_select = false;
        cpu.registers.processor_status.brk = false;
        cpu.registers.processor_status.decimal_mode = false;
        cpu.registers.processor_status.irq_disable = false;
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.carry = false;
        cpu.registers.processor_status.emulation = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::PHP;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01ff], 0x20, "Stack top");
        assert_eq!(cpu.registers.stack_pointer, 0x01fe, "Stack pointer");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }

    #[test]
    fn index_register_select() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.stack_pointer = 0x01ff;
        cpu.registers.processor_status.negative = false;
        cpu.registers.processor_status.overflow = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.processor_status.index_register_select = true;
        cpu.registers.processor_status.brk = false;
        cpu.registers.processor_status.decimal_mode = false;
        cpu.registers.processor_status.irq_disable = false;
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.carry = false;
        cpu.registers.processor_status.emulation = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::PHP;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01ff], 0x10, "Stack top");
        assert_eq!(cpu.registers.stack_pointer, 0x01fe, "Stack pointer");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }

    #[test]
    fn decimal_mode() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.stack_pointer = 0x01ff;
        cpu.registers.processor_status.negative = false;
        cpu.registers.processor_status.overflow = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.processor_status.index_register_select = false;
        cpu.registers.processor_status.brk = false;
        cpu.registers.processor_status.decimal_mode = true;
        cpu.registers.processor_status.irq_disable = false;
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.carry = false;
        cpu.registers.processor_status.emulation = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::PHP;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01ff], 0x08, "Stack top");
        assert_eq!(cpu.registers.stack_pointer, 0x01fe, "Stack pointer");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }

    #[test]
    fn irq_disable() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.stack_pointer = 0x01ff;
        cpu.registers.processor_status.negative = false;
        cpu.registers.processor_status.overflow = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.processor_status.index_register_select = false;
        cpu.registers.processor_status.brk = false;
        cpu.registers.processor_status.decimal_mode = false;
        cpu.registers.processor_status.irq_disable = true;
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.carry = false;
        cpu.registers.processor_status.emulation = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::PHP;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01ff], 0x04, "Stack top");
        assert_eq!(cpu.registers.stack_pointer, 0x01fe, "Stack pointer");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }

    #[test]
    fn zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.stack_pointer = 0x01ff;
        cpu.registers.processor_status.negative = false;
        cpu.registers.processor_status.overflow = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.processor_status.index_register_select = false;
        cpu.registers.processor_status.brk = false;
        cpu.registers.processor_status.decimal_mode = false;
        cpu.registers.processor_status.irq_disable = false;
        cpu.registers.processor_status.zero = true;
        cpu.registers.processor_status.carry = false;
        cpu.registers.processor_status.emulation = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::PHP;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01ff], 0x02, "Stack top");
        assert_eq!(cpu.registers.stack_pointer, 0x01fe, "Stack pointer");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }

    #[test]
    fn carry() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.stack_pointer = 0x01ff;
        cpu.registers.processor_status.negative = false;
        cpu.registers.processor_status.overflow = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.processor_status.index_register_select = false;
        cpu.registers.processor_status.brk = false;
        cpu.registers.processor_status.decimal_mode = false;
        cpu.registers.processor_status.irq_disable = false;
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.carry = true;
        cpu.registers.processor_status.emulation = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::PHP;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01ff], 0x01, "Stack top");
        assert_eq!(cpu.registers.stack_pointer, 0x01fe, "Stack pointer");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Pha;

impl CpuControl for Pha {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    registers.program_bank,
                    registers.program_counter + 1,
                ))
                .valid_data_address(false)
                .valid_program_address(false)),
            2 => {
                let a = if registers.processor_status.emulation
                    || registers.processor_status.memory_select
                {
                    registers.accumulator as u8
                } else {
                    (registers.accumulator >> 8) as u8
                };

                Ok(CpuOutputs::default_from_registers(*registers)
                    .address_bus(AddressBus::from_bank_addr(0, registers.stack_pointer))
                    .data_bus(DataBus::from_data(a))
                    .read_write(ReadWrite::Write)
                    .valid_data_address(true))
            }
            3 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(0, registers.stack_pointer - 1))
                .data_bus(DataBus::from_data(registers.accumulator as u8))
                .read_write(ReadWrite::Write)
                .valid_data_address(true)),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => {
                *cycle = 2;
                Ok(())
            }
            2 => {
                if registers.processor_status.emulation || registers.processor_status.memory_select
                {
                    registers.stack_pointer -= 1;
                    registers.program_counter += 1;
                    *cycle = 0;
                } else {
                    *cycle = 3;
                }
                Ok(())
            }
            3 => {
                registers.stack_pointer -= 2;
                registers.program_counter += 1;
                *cycle = 0;
                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod pha_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.stack_pointer = 0x01ff;
        cpu.registers.accumulator = 0xca;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::PHA;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01ff], 0xca, "Stack top");
        assert_eq!(cpu.registers.stack_pointer, 0x01fe, "Stack pointer");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }

    #[test]
    fn native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.stack_pointer = 0x01ff;
        cpu.registers.accumulator = 0xcab2;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::PHA;

        for _ in 0..4 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01ff], 0xca, "Stack top");
        assert_eq!(memory[0x01fe], 0xb2, "Stack top - 1");
        assert_eq!(cpu.registers.stack_pointer, 0x01fd, "Stack pointer");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}

#[derive(Copy, Clone, Debug, Default)]
pub struct Pla;

impl CpuControl for Pla {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match *cycle {
            1 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(0, registers.program_counter + 1))
                .valid_program_address(false)
                .valid_data_address(false)),
            2 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(0, registers.program_counter + 1))
                .valid_program_address(false)
                .valid_data_address(false)),
            3 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(0, registers.stack_pointer + 1))
                .valid_program_address(false)
                .valid_data_address(true)),
            4 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(0, registers.stack_pointer + 2))
                .valid_program_address(false)
                .valid_data_address(true)),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match *cycle {
            1 => {
                *cycle = 2;
                Ok(())
            }
            2 => {
                *cycle = 3;
                Ok(())
            }
            3 => {
                let a = alu::emulation::nop(registers.mdr, &mut registers.processor_status);
                registers.accumulator &= !0x00ff;
                registers.accumulator |= a as u16;

                if registers.processor_status.emulation || registers.processor_status.memory_select
                {
                    *cycle = 0;
                    registers.stack_pointer += 1;
                    registers.program_counter += 1;
                } else {
                    *cycle = 4;
                }
                Ok(())
            }
            4 => {
                let a = alu::emulation::nop(registers.mdr, &mut registers.processor_status);
                registers.accumulator &= !0xff00;
                registers.accumulator |= (a as u16) << 8;

                *cycle = 0;
                registers.stack_pointer += 2;
                registers.program_counter += 1;

                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod pla_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.stack_pointer = 0x01fe;
        cpu.registers.accumulator = 0xca;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::PLA;
        memory[0x01ff] = 0xd5;

        for _ in 0..4 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xd5, "Accumulator");
        assert_eq!(cpu.registers.stack_pointer, 0x01ff, "Stack pointer");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }

    #[test]
    fn native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.stack_pointer = 0x01fd;
        cpu.registers.accumulator = 0xcab2;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::PLA;
        memory[0x01ff] = 0xd5;
        memory[0x01fe] = 0xa6;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xd5a6, "Accumulator");
        assert_eq!(cpu.registers.stack_pointer, 0x01ff, "Stack pointer");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}

#[derive(Copy, Clone, Debug, Default)]
pub struct Plp;

impl CpuControl for Plp {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match *cycle {
            1 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(0, registers.program_counter + 1))
                .valid_program_address(false)
                .valid_data_address(false)),
            2 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(0, registers.program_counter + 1))
                .valid_program_address(false)
                .valid_data_address(false)),
            3 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(0, registers.stack_pointer + 1))
                .valid_program_address(false)
                .valid_data_address(true)),
            4 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(0, registers.stack_pointer + 2))
                .valid_program_address(false)
                .valid_data_address(true)),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match *cycle {
            1 => {
                *cycle = 2;
                Ok(())
            }
            2 => {
                *cycle = 3;
                Ok(())
            }
            3 => {
                registers.processor_status.update_from_byte(registers.mdr);

                *cycle = 0;
                registers.stack_pointer += 1;
                registers.program_counter += 1;
                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod plp_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.stack_pointer = 0x01fe;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::PLP;
        memory[0x01ff] = 0xd5;

        for _ in 0..4 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(cpu.registers.processor_status.negative);
        assert!(cpu.registers.processor_status.overflow);
        assert!(!cpu.registers.processor_status.memory_select);
        assert!(cpu.registers.processor_status.index_register_select);
        assert!(!cpu.registers.processor_status.brk);
        assert!(!cpu.registers.processor_status.decimal_mode);
        assert!(cpu.registers.processor_status.irq_disable);
        assert!(!cpu.registers.processor_status.zero);
        assert!(cpu.registers.processor_status.carry);
        assert!(cpu.registers.processor_status.emulation);
        assert_eq!(cpu.registers.stack_pointer, 0x01ff, "Stack pointer");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}

#[derive(Copy, Clone, Debug, Default)]
pub struct Rts;

impl CpuControl for Rts {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match *cycle {
            1 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(0, registers.program_counter + 1))
                .valid_program_address(false)
                .valid_data_address(false)),
            2 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(0, registers.program_counter + 1))
                .valid_program_address(false)
                .valid_data_address(false)),
            3 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(0, registers.stack_pointer + 1))
                .valid_program_address(false)
                .valid_data_address(true)),
            4 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(0, registers.stack_pointer + 2))
                .valid_program_address(false)
                .valid_data_address(true)),
            5 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(0, registers.stack_pointer + 2))
                .valid_program_address(false)
                .valid_data_address(false)),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match *cycle {
            1 => {
                *cycle = 2;
                Ok(())
            }
            2 => {
                *cycle = 3;
                Ok(())
            }
            3 => {
                registers.program_counter &= !0x00ff;
                registers.program_counter |= registers.mdr as u16;

                *cycle = 4;
                Ok(())
            }
            4 => {
                registers.program_counter &= !0xff00;
                registers.program_counter |= (registers.mdr as u16) << 8;

                *cycle = 5;
                Ok(())
            }
            5 => {
                registers.stack_pointer += 2;

                *cycle = 0;
                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod rts_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.stack_pointer = 0x01fe;
        cpu.registers.accumulator = 0xca;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::RTS;
        memory[0x01ff] = 0x80;
        memory[0x0200] = 0x90;

        for _ in 0..6 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.cycle, 0);
        assert_eq!(cpu.registers.stack_pointer, 0x0200, "Stack pointer");
        assert_eq!(cpu.registers.program_counter, 0x9080, "Program counter");
    }
}
