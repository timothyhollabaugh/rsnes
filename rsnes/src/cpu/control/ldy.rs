use super::prelude::*;

#[derive(Clone, Copy, Debug)]
pub struct LdyAbsolute;

impl CpuControl for LdyAbsolute {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => helpers::read_operand_rising(0, registers),
            2 => helpers::read_operand_rising(1, registers),
            3 => helpers::read_operand_addr_rising(0, registers, operand),
            4 => helpers::read_operand_addr_rising(1, registers, operand),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => helpers::read_operand_falling(0, registers, cycle, operand),
            2 => helpers::read_operand_falling(1, registers, cycle, operand),
            3 => {
                registers.index_y &= !0x00ff;
                registers.index_y |= registers.mdr as u16;

                if registers.processor_status.emulation
                    || registers.processor_status.index_register_select
                {
                    registers.processor_status.zero = registers.index_y == 0;
                    registers.processor_status.negative = registers.index_y & 0x0080 != 0;

                    registers.program_counter += 3;
                    *cycle = 0;
                } else {
                    *cycle = 4;
                }
                Ok(())
            }
            4 => {
                registers.index_y &= !0xff00;
                registers.index_y |= (registers.mdr as u16) << 8;

                registers.processor_status.zero = registers.index_y == 0;
                registers.processor_status.negative = registers.index_y & 0x8000 != 0;

                registers.program_counter += 3;
                *cycle = 0;
                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod absolute_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.index_y = 0xca;
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcodes::LDY_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0xd5;

        for _ in 0..4 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.index_y, 0xd5, "Y Index");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn zero_emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.index_y = 0x00;
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcodes::LDY_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0x00;

        for _ in 0..4 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.index_y, 0x00, "Y Index");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.index_y = 0xcab2;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.index_register_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::LDY_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0x46;
        memory[0x0202] = 0xd5;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.index_y, 0xd546, "Y Index");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn zero_native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.index_y = 0x00;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.index_register_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::LDY_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0x00;
        memory[0x0202] = 0x00;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.index_y, 0x00, "Y Index");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
    }
}

#[derive(Copy, Clone, Debug, Default)]
pub struct LdyImmediate;

impl CpuControl for LdyImmediate {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => helpers::read_operand_rising(0, registers),
            2 => helpers::read_operand_rising(1, registers),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => {
                registers.index_y &= !0x00ff;
                registers.index_y |= registers.mdr as u16;

                if registers.processor_status.emulation
                    || registers.processor_status.index_register_select
                {
                    registers.processor_status.zero = registers.index_y == 0;
                    registers.processor_status.negative = registers.index_y & 0x0080 != 0;

                    registers.program_counter += 2;
                    *cycle = 0;
                } else {
                    *cycle = 2;
                }

                Ok(())
            }
            2 => {
                registers.index_y &= !0xff00;
                registers.index_y |= (registers.mdr as u16) << 8;

                registers.processor_status.zero = registers.index_y == 0;
                registers.processor_status.negative = registers.index_y & 0x8000 != 0;

                registers.program_counter += 3;
                *cycle = 0;

                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod immediate_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.index_y = 0xca;
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcodes::LDY_IMMEDIATE;
        memory[0x8001] = 0xd5;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.index_y, 0xd5, "Y Index");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn zero_emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.index_y = 0x00;
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcodes::LDY_IMMEDIATE;
        memory[0x8001] = 0x00;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.index_y, 0x00, "Y Index");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.index_y = 0xcab2;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.index_register_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::LDY_IMMEDIATE;
        memory[0x8001] = 0x46;
        memory[0x8002] = 0xd5;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.index_y, 0xd546, "Y Index");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn zero_native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.index_y = 0x00;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.index_register_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::LDY_IMMEDIATE;
        memory[0x8001] = 0x00;
        memory[0x8002] = 0x00;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.index_y, 0x00, "Y Index");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
    }
}
