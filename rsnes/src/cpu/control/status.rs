use super::prelude::*;

#[derive(Copy, Clone, Debug)]
pub struct Clc;

impl CpuControl for Clc {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        registers.processor_status.carry = false;
        Ok(CpuOutputs::default_from_registers(*registers))
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        registers.program_counter += 1;
        *cycle = 0;
        Ok(())
    }
}

#[cfg(test)]
mod clc_test {
    use crate::cpu::test::*;

    #[test]
    fn clc() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.carry = true;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::CLC;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(!cpu.registers.processor_status.carry, "Carry");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Cld;

impl CpuControl for Cld {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        registers.processor_status.decimal_mode = false;
        Ok(CpuOutputs::default_from_registers(*registers))
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        registers.program_counter += 1;
        *cycle = 0;
        Ok(())
    }
}

#[cfg(test)]
mod cld_test {
    use crate::cpu::test::*;

    #[test]
    fn cld() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.decimal_mode = true;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::CLD;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(!cpu.registers.processor_status.decimal_mode, "Decimal mode");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Rep;

impl CpuControl for Rep {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => Ok(CpuOutputs::default_from_registers(*registers).address_bus(
                AddressBus::from_bank_addr(registers.program_bank, registers.program_counter + 1),
            )),
            2 => Ok(CpuOutputs::default_from_registers(*registers).address_bus(
                AddressBus::from_bank_addr(registers.program_bank, registers.program_counter + 1),
            )),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => {
                if registers.mdr & 0x80 != 0 {
                    registers.processor_status.negative = false;
                }

                if registers.mdr & 0x40 != 0 {
                    registers.processor_status.overflow = false;
                }

                if registers.mdr & 0x20 != 0 {
                    registers.processor_status.memory_select = false;
                }

                if registers.mdr & 0x10 != 0 {
                    registers.processor_status.index_register_select = false;
                }

                if registers.mdr & 0x08 != 0 {
                    registers.processor_status.decimal_mode = false;
                }

                if registers.mdr & 0x04 != 0 {
                    registers.processor_status.irq_disable = false;
                }

                if registers.mdr & 0x02 != 0 {
                    registers.processor_status.zero = false;
                }

                if registers.mdr & 0x01 != 0 {
                    registers.processor_status.carry = false;
                }

                *cycle = 2;

                Ok(())
            }
            2 => {
                registers.program_counter += 2;
                *cycle = 0;

                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod rep_test {
    use crate::cpu::test::*;

    #[test]
    fn negative() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.negative = true;
        cpu.registers.processor_status.overflow = true;
        cpu.registers.processor_status.memory_select = true;
        cpu.registers.processor_status.index_register_select = true;
        cpu.registers.processor_status.brk = true;
        cpu.registers.processor_status.decimal_mode = true;
        cpu.registers.processor_status.irq_disable = true;
        cpu.registers.processor_status.zero = true;
        cpu.registers.processor_status.carry = true;
        cpu.registers.processor_status.emulation = true;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::REP;
        memory[0x8001] = 0x80;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(!cpu.registers.processor_status.negative, "Negative");
        assert!(cpu.registers.processor_status.overflow, "Overflow");
        assert!(
            cpu.registers.processor_status.memory_select,
            "Memory select"
        );
        assert!(
            cpu.registers.processor_status.index_register_select,
            "Index register select"
        );
        assert!(cpu.registers.processor_status.brk, "Break");
        assert!(cpu.registers.processor_status.decimal_mode, "Decimal Mode");
        assert!(cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert!(cpu.registers.processor_status.zero, "Zero");
        assert!(cpu.registers.processor_status.carry, "Carry");
        assert!(cpu.registers.processor_status.emulation, "Emulation");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn overflow() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.negative = true;
        cpu.registers.processor_status.overflow = true;
        cpu.registers.processor_status.memory_select = true;
        cpu.registers.processor_status.index_register_select = true;
        cpu.registers.processor_status.brk = true;
        cpu.registers.processor_status.decimal_mode = true;
        cpu.registers.processor_status.irq_disable = true;
        cpu.registers.processor_status.zero = true;
        cpu.registers.processor_status.carry = true;
        cpu.registers.processor_status.emulation = true;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::REP;
        memory[0x8001] = 0x40;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(cpu.registers.processor_status.negative, "Negative");
        assert!(!cpu.registers.processor_status.overflow, "Overflow");
        assert!(
            cpu.registers.processor_status.memory_select,
            "Memory select"
        );
        assert!(
            cpu.registers.processor_status.index_register_select,
            "Index register select"
        );
        assert!(cpu.registers.processor_status.brk, "Break");
        assert!(cpu.registers.processor_status.decimal_mode, "Decimal Mode");
        assert!(cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert!(cpu.registers.processor_status.zero, "Zero");
        assert!(cpu.registers.processor_status.carry, "Carry");
        assert!(cpu.registers.processor_status.emulation, "Emulation");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn memory_select() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.negative = true;
        cpu.registers.processor_status.overflow = true;
        cpu.registers.processor_status.memory_select = true;
        cpu.registers.processor_status.index_register_select = true;
        cpu.registers.processor_status.brk = true;
        cpu.registers.processor_status.decimal_mode = true;
        cpu.registers.processor_status.irq_disable = true;
        cpu.registers.processor_status.zero = true;
        cpu.registers.processor_status.carry = true;
        cpu.registers.processor_status.emulation = true;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::REP;
        memory[0x8001] = 0x20;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(cpu.registers.processor_status.negative, "Negative");
        assert!(cpu.registers.processor_status.overflow, "Overflow");
        assert!(
            !cpu.registers.processor_status.memory_select,
            "Memory select"
        );
        assert!(
            cpu.registers.processor_status.index_register_select,
            "Index register select"
        );
        assert!(cpu.registers.processor_status.brk, "Break");
        assert!(cpu.registers.processor_status.decimal_mode, "Decimal Mode");
        assert!(cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert!(cpu.registers.processor_status.zero, "Zero");
        assert!(cpu.registers.processor_status.carry, "Carry");
        assert!(cpu.registers.processor_status.emulation, "Emulation");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn index_register_select() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.negative = true;
        cpu.registers.processor_status.overflow = true;
        cpu.registers.processor_status.memory_select = true;
        cpu.registers.processor_status.index_register_select = true;
        cpu.registers.processor_status.brk = true;
        cpu.registers.processor_status.decimal_mode = true;
        cpu.registers.processor_status.irq_disable = true;
        cpu.registers.processor_status.zero = true;
        cpu.registers.processor_status.carry = true;
        cpu.registers.processor_status.emulation = true;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::REP;
        memory[0x8001] = 0x10;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(cpu.registers.processor_status.negative, "Negative");
        assert!(cpu.registers.processor_status.overflow, "Overflow");
        assert!(
            cpu.registers.processor_status.memory_select,
            "Memory select"
        );
        assert!(
            !cpu.registers.processor_status.index_register_select,
            "Index register select"
        );
        assert!(cpu.registers.processor_status.brk, "Break");
        assert!(cpu.registers.processor_status.decimal_mode, "Decimal Mode");
        assert!(cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert!(cpu.registers.processor_status.zero, "Zero");
        assert!(cpu.registers.processor_status.carry, "Carry");
        assert!(cpu.registers.processor_status.emulation, "Emulation");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn decimal_mode() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.negative = true;
        cpu.registers.processor_status.overflow = true;
        cpu.registers.processor_status.memory_select = true;
        cpu.registers.processor_status.index_register_select = true;
        cpu.registers.processor_status.brk = true;
        cpu.registers.processor_status.decimal_mode = true;
        cpu.registers.processor_status.irq_disable = true;
        cpu.registers.processor_status.zero = true;
        cpu.registers.processor_status.carry = true;
        cpu.registers.processor_status.emulation = true;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::REP;
        memory[0x8001] = 0x08;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(cpu.registers.processor_status.negative, "Negative");
        assert!(cpu.registers.processor_status.overflow, "Overflow");
        assert!(
            cpu.registers.processor_status.memory_select,
            "Memory select"
        );
        assert!(
            cpu.registers.processor_status.index_register_select,
            "Index register select"
        );
        assert!(cpu.registers.processor_status.brk, "Break");
        assert!(!cpu.registers.processor_status.decimal_mode, "Decimal Mode");
        assert!(cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert!(cpu.registers.processor_status.zero, "Zero");
        assert!(cpu.registers.processor_status.carry, "Carry");
        assert!(cpu.registers.processor_status.emulation, "Emulation");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn irq_disable() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.negative = true;
        cpu.registers.processor_status.overflow = true;
        cpu.registers.processor_status.memory_select = true;
        cpu.registers.processor_status.index_register_select = true;
        cpu.registers.processor_status.brk = true;
        cpu.registers.processor_status.decimal_mode = true;
        cpu.registers.processor_status.irq_disable = true;
        cpu.registers.processor_status.zero = true;
        cpu.registers.processor_status.carry = true;
        cpu.registers.processor_status.emulation = true;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::REP;
        memory[0x8001] = 0x04;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(cpu.registers.processor_status.negative, "Negative");
        assert!(cpu.registers.processor_status.overflow, "Overflow");
        assert!(
            cpu.registers.processor_status.memory_select,
            "Memory select"
        );
        assert!(
            cpu.registers.processor_status.index_register_select,
            "Index register select"
        );
        assert!(cpu.registers.processor_status.brk, "Break");
        assert!(cpu.registers.processor_status.decimal_mode, "Decimal Mode");
        assert!(!cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert!(cpu.registers.processor_status.zero, "Zero");
        assert!(cpu.registers.processor_status.carry, "Carry");
        assert!(cpu.registers.processor_status.emulation, "Emulation");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.negative = true;
        cpu.registers.processor_status.overflow = true;
        cpu.registers.processor_status.memory_select = true;
        cpu.registers.processor_status.index_register_select = true;
        cpu.registers.processor_status.brk = true;
        cpu.registers.processor_status.decimal_mode = true;
        cpu.registers.processor_status.irq_disable = true;
        cpu.registers.processor_status.zero = true;
        cpu.registers.processor_status.carry = true;
        cpu.registers.processor_status.emulation = true;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::REP;
        memory[0x8001] = 0x02;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(cpu.registers.processor_status.negative, "Negative");
        assert!(cpu.registers.processor_status.overflow, "Overflow");
        assert!(
            cpu.registers.processor_status.memory_select,
            "Memory select"
        );
        assert!(
            cpu.registers.processor_status.index_register_select,
            "Index register select"
        );
        assert!(cpu.registers.processor_status.brk, "Break");
        assert!(cpu.registers.processor_status.decimal_mode, "Decimal Mode");
        assert!(cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert!(!cpu.registers.processor_status.zero, "Zero");
        assert!(cpu.registers.processor_status.carry, "Carry");
        assert!(cpu.registers.processor_status.emulation, "Emulation");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn carry() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.negative = true;
        cpu.registers.processor_status.overflow = true;
        cpu.registers.processor_status.memory_select = true;
        cpu.registers.processor_status.index_register_select = true;
        cpu.registers.processor_status.brk = true;
        cpu.registers.processor_status.decimal_mode = true;
        cpu.registers.processor_status.irq_disable = true;
        cpu.registers.processor_status.zero = true;
        cpu.registers.processor_status.carry = true;
        cpu.registers.processor_status.emulation = true;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::REP;
        memory[0x8001] = 0x01;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(cpu.registers.processor_status.negative, "Negative");
        assert!(cpu.registers.processor_status.overflow, "Overflow");
        assert!(
            cpu.registers.processor_status.memory_select,
            "Memory select"
        );
        assert!(
            cpu.registers.processor_status.index_register_select,
            "Index register select"
        );
        assert!(cpu.registers.processor_status.brk, "Break");
        assert!(cpu.registers.processor_status.decimal_mode, "Decimal Mode");
        assert!(cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert!(cpu.registers.processor_status.zero, "Zero");
        assert!(!cpu.registers.processor_status.carry, "Carry");
        assert!(cpu.registers.processor_status.emulation, "Emulation");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Sec;

impl CpuControl for Sec {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        registers.processor_status.carry = true;
        Ok(CpuOutputs::default_from_registers(*registers))
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        registers.program_counter += 1;
        *cycle = 0;
        Ok(())
    }
}

#[cfg(test)]
mod sec_test {
    use crate::cpu::test::*;

    #[test]
    fn sec() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.carry = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::SEC;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(cpu.registers.processor_status.carry, "Carry");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Sep;

impl CpuControl for Sep {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => Ok(CpuOutputs::default_from_registers(*registers).address_bus(
                AddressBus::from_bank_addr(registers.program_bank, registers.program_counter + 1),
            )),
            2 => Ok(CpuOutputs::default_from_registers(*registers).address_bus(
                AddressBus::from_bank_addr(registers.program_bank, registers.program_counter + 1),
            )),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => {
                if registers.mdr & 0x80 != 0 {
                    registers.processor_status.negative = true;
                }

                if registers.mdr & 0x40 != 0 {
                    registers.processor_status.overflow = true;
                }

                if registers.mdr & 0x20 != 0 {
                    registers.processor_status.memory_select = true;
                }

                if registers.mdr & 0x10 != 0 {
                    registers.processor_status.index_register_select = true;
                }

                if registers.mdr & 0x08 != 0 {
                    registers.processor_status.decimal_mode = true;
                }

                if registers.mdr & 0x04 != 0 {
                    registers.processor_status.irq_disable = true;
                }

                if registers.mdr & 0x02 != 0 {
                    registers.processor_status.zero = true;
                }

                if registers.mdr & 0x01 != 0 {
                    registers.processor_status.carry = true;
                }

                *cycle = 2;

                Ok(())
            }
            2 => {
                registers.program_counter += 2;
                *cycle = 0;

                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod sep_test {
    use crate::cpu::test::*;

    #[test]
    fn negative() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.negative = false;
        cpu.registers.processor_status.overflow = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.processor_status.index_register_select = false;
        cpu.registers.processor_status.brk = false;
        cpu.registers.processor_status.decimal_mode = false;
        cpu.registers.processor_status.irq_disable = false;
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.carry = false;
        cpu.registers.processor_status.emulation = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::SEP;
        memory[0x8001] = 0x80;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(cpu.registers.processor_status.negative, "Negative");
        assert!(!cpu.registers.processor_status.overflow, "Overflow");
        assert!(
            !cpu.registers.processor_status.memory_select,
            "Memory select"
        );
        assert!(
            !cpu.registers.processor_status.index_register_select,
            "Index register select"
        );
        assert!(!cpu.registers.processor_status.brk, "Break");
        assert!(!cpu.registers.processor_status.decimal_mode, "Decimal Mode");
        assert!(!cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert!(!cpu.registers.processor_status.zero, "Zero");
        assert!(!cpu.registers.processor_status.carry, "Carry");
        assert!(!cpu.registers.processor_status.emulation, "Emulation");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn overflow() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.negative = false;
        cpu.registers.processor_status.overflow = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.processor_status.index_register_select = false;
        cpu.registers.processor_status.brk = false;
        cpu.registers.processor_status.decimal_mode = false;
        cpu.registers.processor_status.irq_disable = false;
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.carry = false;
        cpu.registers.processor_status.emulation = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::SEP;
        memory[0x8001] = 0x40;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(!cpu.registers.processor_status.negative, "Negative");
        assert!(cpu.registers.processor_status.overflow, "Overflow");
        assert!(
            !cpu.registers.processor_status.memory_select,
            "Memory select"
        );
        assert!(
            !cpu.registers.processor_status.index_register_select,
            "Index register select"
        );
        assert!(!cpu.registers.processor_status.brk, "Break");
        assert!(!cpu.registers.processor_status.decimal_mode, "Decimal Mode");
        assert!(!cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert!(!cpu.registers.processor_status.zero, "Zero");
        assert!(!cpu.registers.processor_status.carry, "Carry");
        assert!(!cpu.registers.processor_status.emulation, "Emulation");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn memory_select() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.negative = false;
        cpu.registers.processor_status.overflow = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.processor_status.index_register_select = false;
        cpu.registers.processor_status.brk = false;
        cpu.registers.processor_status.decimal_mode = false;
        cpu.registers.processor_status.irq_disable = false;
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.carry = false;
        cpu.registers.processor_status.emulation = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::SEP;
        memory[0x8001] = 0x20;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(!cpu.registers.processor_status.negative, "Negative");
        assert!(!cpu.registers.processor_status.overflow, "Overflow");
        assert!(
            cpu.registers.processor_status.memory_select,
            "Memory select"
        );
        assert!(
            !cpu.registers.processor_status.index_register_select,
            "Index register select"
        );
        assert!(!cpu.registers.processor_status.brk, "Break");
        assert!(!cpu.registers.processor_status.decimal_mode, "Decimal Mode");
        assert!(!cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert!(!cpu.registers.processor_status.zero, "Zero");
        assert!(!cpu.registers.processor_status.carry, "Carry");
        assert!(!cpu.registers.processor_status.emulation, "Emulation");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn index_register_select() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.negative = false;
        cpu.registers.processor_status.overflow = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.processor_status.index_register_select = false;
        cpu.registers.processor_status.brk = false;
        cpu.registers.processor_status.decimal_mode = false;
        cpu.registers.processor_status.irq_disable = false;
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.carry = false;
        cpu.registers.processor_status.emulation = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::SEP;
        memory[0x8001] = 0x10;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(!cpu.registers.processor_status.negative, "Negative");
        assert!(!cpu.registers.processor_status.overflow, "Overflow");
        assert!(
            !cpu.registers.processor_status.memory_select,
            "Memory select"
        );
        assert!(
            cpu.registers.processor_status.index_register_select,
            "Index register select"
        );
        assert!(!cpu.registers.processor_status.brk, "Break");
        assert!(!cpu.registers.processor_status.decimal_mode, "Decimal Mode");
        assert!(!cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert!(!cpu.registers.processor_status.zero, "Zero");
        assert!(!cpu.registers.processor_status.carry, "Carry");
        assert!(!cpu.registers.processor_status.emulation, "Emulation");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn decimal_mode() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.negative = false;
        cpu.registers.processor_status.overflow = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.processor_status.index_register_select = false;
        cpu.registers.processor_status.brk = false;
        cpu.registers.processor_status.decimal_mode = false;
        cpu.registers.processor_status.irq_disable = false;
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.carry = false;
        cpu.registers.processor_status.emulation = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::SEP;
        memory[0x8001] = 0x08;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(!cpu.registers.processor_status.negative, "Negative");
        assert!(!cpu.registers.processor_status.overflow, "Overflow");
        assert!(
            !cpu.registers.processor_status.memory_select,
            "Memory select"
        );
        assert!(
            !cpu.registers.processor_status.index_register_select,
            "Index register select"
        );
        assert!(!cpu.registers.processor_status.brk, "Break");
        assert!(cpu.registers.processor_status.decimal_mode, "Decimal Mode");
        assert!(!cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert!(!cpu.registers.processor_status.zero, "Zero");
        assert!(!cpu.registers.processor_status.carry, "Carry");
        assert!(!cpu.registers.processor_status.emulation, "Emulation");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn irq_disable() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.negative = false;
        cpu.registers.processor_status.overflow = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.processor_status.index_register_select = false;
        cpu.registers.processor_status.brk = false;
        cpu.registers.processor_status.decimal_mode = false;
        cpu.registers.processor_status.irq_disable = false;
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.carry = false;
        cpu.registers.processor_status.emulation = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::SEP;
        memory[0x8001] = 0x04;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(!cpu.registers.processor_status.negative, "Negative");
        assert!(!cpu.registers.processor_status.overflow, "Overflow");
        assert!(
            !cpu.registers.processor_status.memory_select,
            "Memory select"
        );
        assert!(
            !cpu.registers.processor_status.index_register_select,
            "Index register select"
        );
        assert!(!cpu.registers.processor_status.brk, "Break");
        assert!(!cpu.registers.processor_status.decimal_mode, "Decimal Mode");
        assert!(cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert!(!cpu.registers.processor_status.zero, "Zero");
        assert!(!cpu.registers.processor_status.carry, "Carry");
        assert!(!cpu.registers.processor_status.emulation, "Emulation");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.negative = false;
        cpu.registers.processor_status.overflow = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.processor_status.index_register_select = false;
        cpu.registers.processor_status.brk = false;
        cpu.registers.processor_status.decimal_mode = false;
        cpu.registers.processor_status.irq_disable = false;
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.carry = false;
        cpu.registers.processor_status.emulation = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::SEP;
        memory[0x8001] = 0x02;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(!cpu.registers.processor_status.negative, "Negative");
        assert!(!cpu.registers.processor_status.overflow, "Overflow");
        assert!(
            !cpu.registers.processor_status.memory_select,
            "Memory select"
        );
        assert!(
            !cpu.registers.processor_status.index_register_select,
            "Index register select"
        );
        assert!(!cpu.registers.processor_status.brk, "Break");
        assert!(!cpu.registers.processor_status.decimal_mode, "Decimal Mode");
        assert!(!cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert!(cpu.registers.processor_status.zero, "Zero");
        assert!(!cpu.registers.processor_status.carry, "Carry");
        assert!(!cpu.registers.processor_status.emulation, "Emulation");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn carry() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.negative = false;
        cpu.registers.processor_status.overflow = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.processor_status.index_register_select = false;
        cpu.registers.processor_status.brk = false;
        cpu.registers.processor_status.decimal_mode = false;
        cpu.registers.processor_status.irq_disable = false;
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.carry = false;
        cpu.registers.processor_status.emulation = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::SEP;
        memory[0x8001] = 0x01;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(!cpu.registers.processor_status.negative, "Negative");
        assert!(!cpu.registers.processor_status.overflow, "Overflow");
        assert!(
            !cpu.registers.processor_status.memory_select,
            "Memory select"
        );
        assert!(
            !cpu.registers.processor_status.index_register_select,
            "Index register select"
        );
        assert!(!cpu.registers.processor_status.brk, "Break");
        assert!(!cpu.registers.processor_status.decimal_mode, "Decimal Mode");
        assert!(!cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert!(!cpu.registers.processor_status.zero, "Zero");
        assert!(cpu.registers.processor_status.carry, "Carry");
        assert!(!cpu.registers.processor_status.emulation, "Emulation");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Sei;

impl CpuControl for Sei {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        registers.processor_status.irq_disable = true;
        Ok(CpuOutputs::default_from_registers(*registers))
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        registers.program_counter += 1;
        *cycle = 0;
        Ok(())
    }
}

#[cfg(test)]
mod sei_test {
    use crate::cpu::test::*;

    #[test]
    fn sei() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.irq_disable = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::SEI;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Cli;

impl CpuControl for Cli {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        registers.processor_status.irq_disable = false;
        Ok(CpuOutputs::default_from_registers(*registers))
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        registers.program_counter += 1;
        *cycle = 0;
        Ok(())
    }
}

#[cfg(test)]
mod cli_test {
    use crate::cpu::test::*;

    #[test]
    fn cli() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.irq_disable = true;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::CLI;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(!cpu.registers.processor_status.irq_disable, "IRQ Disable");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Xba;

impl CpuControl for Xba {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        registers.accumulator = registers.accumulator.swap_bytes();
        alu::emulation::nop(registers.accumulator as u8, &mut registers.processor_status);
        Ok(CpuOutputs::default_from_registers(*registers))
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        registers.program_counter += 1;
        *cycle = 0;
        Ok(())
    }
}

#[cfg(test)]
mod xba_test {
    use crate::cpu::test::*;

    #[test]
    fn xba() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::XBA;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xb2ca, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Xce;

impl CpuControl for Xce {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        std::mem::swap(
            &mut registers.processor_status.emulation,
            &mut registers.processor_status.carry,
        );
        Ok(CpuOutputs::default_from_registers(*registers))
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        registers.program_counter += 1;
        *cycle = 0;
        Ok(())
    }
}

#[cfg(test)]
mod xce_test {
    use crate::cpu::test::*;

    #[test]
    fn xce() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.carry = true;
        cpu.registers.processor_status.emulation = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::XCE;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(!cpu.registers.processor_status.carry, "Carry");
        assert!(cpu.registers.processor_status.emulation, "Emulation");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}
