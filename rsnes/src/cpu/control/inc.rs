use super::prelude::*;

pub type IncAccumulator = helpers::ArithmaticAccumulatorImpl<IncAccumulatorImpl>;

#[derive(Clone, Copy, Debug, Default)]
pub struct IncAccumulatorImpl;

impl helpers::ArithmaticAccumulator for IncAccumulatorImpl {
    fn emulation(&mut self, accumulator: &mut u8, status: &mut crate::cpu::ProcessorStatus) {
        *accumulator = alu::emulation::add_carry(*accumulator, 1, status)
    }

    fn native(&mut self, accumulator: &mut u16, status: &mut crate::cpu::ProcessorStatus) {
        *accumulator = alu::native::add_carry(*accumulator, 1, status)
    }
}

#[cfg(test)]
mod inc_accumulator_test {
    use crate::cpu::{control::helpers, test::*};

    #[test]
    fn inc_accumulator() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;

        helpers::test_arithmatic_accumulator(&mut cpu, opcodes::INC_ACCUMULATOR);
        assert_eq!(cpu.registers.accumulator, 0xcb, "Accumulator");
    }
}

#[derive(Clone, Copy, Debug, Default)]
pub struct IncDirect;

impl CpuControl for IncDirect {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match *cycle {
            1 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    registers.program_bank,
                    registers.program_counter + 1,
                ))
                .valid_program_address(true)),
            2 => Ok(CpuOutputs::default_from_registers(*registers).address_bus(
                AddressBus::from_bank_addr(registers.program_bank, registers.program_counter + 1),
            )),
            3 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    0,
                    registers.direct + operand[0] as u16,
                ))
                .valid_data_address(true)
                .read_write(ReadWrite::Read)),
            4 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    0,
                    registers.direct + operand[0] as u16 + 1,
                ))
                .valid_data_address(true)
                .read_write(ReadWrite::Read)),
            5 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    0,
                    registers.direct + operand[0] as u16 + 1,
                ))
                .valid_data_address(false)
                .read_write(ReadWrite::Read)),
            6 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    0,
                    registers.direct + operand[0] as u16 + 1,
                ))
                .data_bus(DataBus::from_data(operand[3]))
                .valid_data_address(true)
                .read_write(ReadWrite::Write)),
            7 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    0,
                    registers.direct + operand[0] as u16,
                ))
                .data_bus(DataBus::from_data(operand[2]))
                .valid_data_address(true)
                .read_write(ReadWrite::Write)),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => {
                operand[0] = registers.mdr;

                if registers.direct & 0x00ff != 0 {
                    *cycle = 2;
                } else {
                    *cycle = 3;
                }

                Ok(())
            }
            2 => {
                *cycle = 3;
                Ok(())
            }
            3 => {
                operand[2] = registers.mdr;
                if registers.processor_status.emulation || registers.processor_status.memory_select
                {
                    *cycle = 5;
                } else {
                    *cycle = 4;
                }
                Ok(())
            }
            4 => {
                operand[3] = registers.mdr;
                *cycle = 5;
                Ok(())
            }
            5 => {
                if registers.processor_status.emulation || registers.processor_status.memory_select
                {
                    operand[2] = operand[2].wrapping_add(1);
                    alu::emulation::nop(operand[2], &mut registers.processor_status);
                    *cycle = 7;
                } else {
                    let o = u16::from_le_bytes(operand[2..4].try_into().unwrap());
                    let o = o.wrapping_add(1);
                    alu::native::nop(o, &mut registers.processor_status);
                    operand[2..4].copy_from_slice(&o.to_le_bytes());
                    *cycle = 6;
                }

                Ok(())
            }
            6 => {
                *cycle = 7;
                Ok(())
            }
            7 => {
                registers.program_counter += 2;
                *cycle = 0;
                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod direct_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation_direct_low_is_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.direct = 0x0100;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::INC_DIRECT_PAGE;
        memory[0x8001] = 0xcf;
        memory[0x01cf] = 0x35;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01cf], 0x36, "Incremented Byte");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn emulation_direct_low_is_not_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.direct = 0x0101;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::INC_DIRECT_PAGE;
        memory[0x8001] = 0xcf;
        memory[0x01d0] = 0x35;

        for _ in 0..6 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01d0], 0x36, "Incremented Byte");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn native_direct_low_is_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.direct = 0x0100;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::INC_DIRECT_PAGE;
        memory[0x8001] = 0xcf;
        memory[0x01cf] = 0x35;
        memory[0x01d0] = 0xca;

        for _ in 0..7 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01cf], 0x36, "Incremented Byte");
        assert_eq!(memory[0x01d0], 0xca, "Incremented Byte");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn native_direct_low_is_not_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.direct = 0x0101;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::INC_DIRECT_PAGE;
        memory[0x8001] = 0xcf;
        memory[0x01d0] = 0x35;
        memory[0x01d1] = 0xca;

        for _ in 0..8 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(memory[0x01d0], 0x36, "Incremented Byte");
        assert_eq!(memory[0x01d1], 0xca, "Incremented Byte");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Inx;

impl CpuControl for Inx {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        registers.index_x += 1;
        registers.processor_status.zero = registers.index_x == 0;
        registers.processor_status.negative = registers.index_x & 0x8000 != 0;
        Ok(CpuOutputs::default_from_registers(*registers))
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        registers.program_counter += 1;
        *cycle = 0;
        Ok(())
    }
}

#[cfg(test)]
mod inx_test {
    use crate::cpu::test::*;

    #[test]
    fn inx() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.index_x = 0xca;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::INX;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.index_x, 0xcb, "Index X");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Iny;

impl CpuControl for Iny {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        registers.index_y += 1;
        registers.processor_status.zero = registers.index_y == 0;
        registers.processor_status.negative = registers.index_y & 0x8000 != 0;
        Ok(CpuOutputs::default_from_registers(*registers))
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        registers.program_counter += 1;
        *cycle = 0;
        Ok(())
    }
}

#[cfg(test)]
mod iny_test {
    use crate::cpu::test::*;

    #[test]
    fn iny() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.index_y = 0xca;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::INY;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.index_y, 0xcb, "Index Y");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}
