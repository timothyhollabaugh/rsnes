use super::prelude::*;

pub type Bra = helpers::branch::BranchTestImpl<BraImpl>;

#[derive(Clone, Copy, Debug, Default)]
pub struct BraImpl;

impl helpers::branch::BranchTest for BraImpl {
    fn test(&mut self, _status: &mut crate::cpu::ProcessorStatus) -> bool {
        true
    }
}

#[cfg(test)]
mod bra_test {
    use crate::cpu::{control::helpers, test::*};

    #[test]
    fn native_branch() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        helpers::branch_test::no_page_boundary(&mut cpu, opcodes::BRA, true);
    }

    #[test]
    fn emulation_no_page_boundary_branch() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.emulation = true;
        helpers::branch_test::no_page_boundary(&mut cpu, opcodes::BRA, true);
    }

    #[test]
    fn emulation_page_boundary_branch() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.emulation = true;
        helpers::branch_test::page_boundary(&mut cpu, opcodes::BRA, true);
    }
}

pub type Beq = helpers::branch::BranchTestImpl<BeqImpl>;

#[derive(Clone, Copy, Debug, Default)]
pub struct BeqImpl;

impl helpers::branch::BranchTest for BeqImpl {
    fn test(&mut self, status: &mut crate::cpu::ProcessorStatus) -> bool {
        status.zero
    }
}

#[cfg(test)]
mod beq_test {
    use crate::cpu::{control::helpers, test::*};

    #[test]
    fn native_no_branch() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.zero = true;
        helpers::branch_test::no_page_boundary(&mut cpu, opcodes::BEQ, true);
    }

    #[test]
    fn native_branch() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.zero = false;
        helpers::branch_test::no_page_boundary(&mut cpu, opcodes::BEQ, false);
    }

    #[test]
    fn emulation_no_page_boundary_branch() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.emulation = true;
        helpers::branch_test::no_page_boundary(&mut cpu, opcodes::BEQ, false);
    }

    #[test]
    fn emulation_page_boundary_branch() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.emulation = true;
        helpers::branch_test::page_boundary(&mut cpu, opcodes::BEQ, false);
    }
}

pub type Bne = helpers::branch::BranchTestImpl<BneImpl>;

#[derive(Clone, Copy, Debug, Default)]
pub struct BneImpl;

impl helpers::branch::BranchTest for BneImpl {
    fn test(&mut self, status: &mut crate::cpu::ProcessorStatus) -> bool {
        !status.zero
    }
}

#[cfg(test)]
mod bne_test {
    use crate::cpu::{control::helpers, test::*};

    #[test]
    fn native_no_branch() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.zero = true;
        helpers::branch_test::no_page_boundary(&mut cpu, opcodes::BNE, false);
    }

    #[test]
    fn native_branch() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.zero = false;
        helpers::branch_test::no_page_boundary(&mut cpu, opcodes::BNE, true);
    }

    #[test]
    fn emulation_no_page_boundary_branch() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.emulation = true;
        helpers::branch_test::no_page_boundary(&mut cpu, opcodes::BNE, true);
    }

    #[test]
    fn emulation_page_boundary_branch() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.zero = false;
        cpu.registers.processor_status.emulation = true;
        helpers::branch_test::page_boundary(&mut cpu, opcodes::BNE, true);
    }
}

pub type Bvs = helpers::branch::BranchTestImpl<BvsImpl>;

#[derive(Clone, Copy, Debug, Default)]
pub struct BvsImpl;

impl helpers::branch::BranchTest for BvsImpl {
    fn test(&mut self, status: &mut crate::cpu::ProcessorStatus) -> bool {
        status.overflow
    }
}

#[cfg(test)]
mod bvs_test {
    use crate::cpu::{control::helpers, test::*};

    #[test]
    fn native_no_branch() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.overflow = false;
        helpers::branch_test::no_page_boundary(&mut cpu, opcodes::BVS, false);
    }

    #[test]
    fn native_branch() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.overflow = true;
        helpers::branch_test::no_page_boundary(&mut cpu, opcodes::BVS, true);
    }

    #[test]
    fn emulation_no_page_boundary_branch() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.overflow = true;
        cpu.registers.processor_status.emulation = true;
        helpers::branch_test::no_page_boundary(&mut cpu, opcodes::BVS, true);
    }

    #[test]
    fn emulation_page_boundary_branch() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.overflow = true;
        cpu.registers.processor_status.emulation = true;
        helpers::branch_test::page_boundary(&mut cpu, opcodes::BVS, true);
    }
}
