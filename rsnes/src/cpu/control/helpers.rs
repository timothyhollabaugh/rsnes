pub mod arithmatic_absolute_long;
pub mod arithmatic_accumulator;
pub mod arithmatic_immediate;
pub mod branch;

pub use super::prelude;
pub use arithmatic_absolute_long::*;
pub use arithmatic_accumulator::*;
pub use arithmatic_immediate::*;
pub use branch::*;
use prelude::*;

pub fn read_operand_rising(n: usize, registers: &CpuRegisters) -> Result<CpuOutputs, CpuError> {
    Ok(
        CpuOutputs::default_from_registers(*registers).address_bus(AddressBus::from_bank_addr(
            registers.program_bank,
            registers.program_counter + 1 + n as u16,
        )),
    )
}

pub fn read_operand_falling(
    n: usize,
    registers: &CpuRegisters,
    cycle: &mut u8,
    operand: &mut [u8; 4],
) -> Result<(), CpuError> {
    operand[n] = registers.mdr;
    *cycle += 1;
    Ok(())
}

pub fn read_operand_addr_rising(
    n: usize,
    registers: &CpuRegisters,
    operand: &mut [u8; 4],
) -> Result<CpuOutputs, CpuError> {
    Ok(
        CpuOutputs::default_from_registers(*registers).address_bus(AddressBus::from_bank_addr(
            registers.data_bank,
            u16::from_le_bytes(operand[0..2].try_into().unwrap()) + n as u16,
        )),
    )
}

pub fn write_operand_addr_rising(
    n: usize,
    value: u8,
    registers: &CpuRegisters,
    operand: &mut [u8; 4],
) -> Result<CpuOutputs, CpuError> {
    Ok(CpuOutputs::default_from_registers(*registers)
        .address_bus(AddressBus::from_bank_addr(
            registers.data_bank,
            u16::from_le_bytes(operand[0..2].try_into().unwrap()) + n as u16,
        ))
        .data_bus(DataBus::from_data(value))
        .read_write(ReadWrite::Write))
}
