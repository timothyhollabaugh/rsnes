use crate::cpu::alu;

use super::helpers::{ArithmaticAccumulator, ArithmaticAccumulatorImpl};

pub type RolAccumulator = ArithmaticAccumulatorImpl<RolAccumulatorImpl>;

#[derive(Clone, Copy, Debug, Default)]
pub struct RolAccumulatorImpl;

impl ArithmaticAccumulator for RolAccumulatorImpl {
    fn emulation(&mut self, accumulator: &mut u8, status: &mut crate::cpu::ProcessorStatus) {
        *accumulator = alu::emulation::rotate_left(*accumulator, status)
    }

    fn native(&mut self, accumulator: &mut u16, status: &mut crate::cpu::ProcessorStatus) {
        *accumulator = alu::native::rotate_left(*accumulator, status)
    }
}

#[cfg(test)]
mod rol_accumulator_test {
    use crate::cpu::{control::helpers, test::*};

    #[test]
    fn rol_accumulator_carry_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        cpu.registers.processor_status.carry = false;

        helpers::test_arithmatic_accumulator(&mut cpu, opcodes::ROL_ACCUMULATOR);

        assert_eq!(cpu.registers.accumulator, 0x94, "Accumulator");
        assert!(cpu.registers.processor_status.carry, "Carry");
    }

    #[test]
    fn rol_accumulator_carry_one() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0x4a;
        cpu.registers.processor_status.carry = true;

        helpers::test_arithmatic_accumulator(&mut cpu, opcodes::ROL_ACCUMULATOR);

        assert_eq!(cpu.registers.accumulator, 0x95, "Accumulator");
        assert!(!cpu.registers.processor_status.carry, "Carry");
    }
}
