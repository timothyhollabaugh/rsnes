use super::prelude::*;

#[derive(Clone, Copy, Debug)]
pub struct Tcd;

impl CpuControl for Tcd {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        registers.direct = registers.accumulator;
        registers.processor_status.zero = registers.direct == 0;
        registers.processor_status.negative = registers.direct & 0x8000 != 0;
        Ok(CpuOutputs::default_from_registers(*registers))
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        registers.program_counter += 1;
        *cycle = 0;
        Ok(())
    }
}

#[cfg(test)]
mod tcd_test {
    use crate::cpu::test::*;

    #[test]
    fn tcd() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        cpu.registers.direct = 0x35;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::TCD;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xca, "Accumulator");
        assert_eq!(cpu.registers.direct, 0xca, "Direct");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Tcs;

impl CpuControl for Tcs {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        registers.stack_pointer = registers.accumulator;
        Ok(CpuOutputs::default_from_registers(*registers))
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        registers.program_counter += 1;
        *cycle = 0;
        Ok(())
    }
}

#[cfg(test)]
mod tcs_test {
    use crate::cpu::test::*;

    #[test]
    fn tcs() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        cpu.registers.stack_pointer = 0x35;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::TCS;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xca, "Accumulator");
        assert_eq!(cpu.registers.stack_pointer, 0xca, "Stack Pointer");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Tax;

impl CpuControl for Tax {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        if registers.processor_status.index_register_select {
            registers.index_x &= !0x00ff;
            registers.index_x |= registers.accumulator & 0x00ff;
            registers.processor_status.zero = registers.index_x & 0x00ff == 0;
            registers.processor_status.negative = registers.index_x & 0x0080 != 0;
        } else {
            registers.index_x = registers.accumulator;
            registers.processor_status.zero = registers.index_x == 0;
            registers.processor_status.negative = registers.index_x & 0x8000 != 0;
        }
        Ok(CpuOutputs::default_from_registers(*registers))
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        registers.program_counter += 1;
        *cycle = 0;
        Ok(())
    }
}

#[cfg(test)]
mod tax_test {
    use crate::cpu::test::*;

    #[test]
    fn tax() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        cpu.registers.index_x = 0x35;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::TAX;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xca, "Accumulator");
        assert_eq!(cpu.registers.index_x, 0xca, "Index X");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Txa;

impl CpuControl for Txa {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        if registers.processor_status.index_register_select {
            registers.accumulator &= !0x00ff;
            registers.accumulator |= registers.index_x & 0x00ff;
            registers.processor_status.zero = registers.index_x & 0x00ff == 0;
            registers.processor_status.negative = registers.index_x & 0x0080 != 0;
        } else {
            registers.accumulator = registers.index_x;
            registers.processor_status.zero = registers.index_x == 0;
            registers.processor_status.negative = registers.index_x & 0x8000 != 0;
        }
        Ok(CpuOutputs::default_from_registers(*registers))
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        registers.program_counter += 1;
        *cycle = 0;
        Ok(())
    }
}

#[cfg(test)]
mod txa_test {
    use crate::cpu::test::*;

    #[test]
    fn tax() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        cpu.registers.index_x = 0x35;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::TXA;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0x35, "Accumulator");
        assert_eq!(cpu.registers.index_x, 0x35, "Index X");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}
