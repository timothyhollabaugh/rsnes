use super::prelude::*;

#[derive(Clone, Copy, Debug)]
pub struct LdaAbsolute;

impl CpuControl for LdaAbsolute {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => helpers::read_operand_rising(0, registers),
            2 => helpers::read_operand_rising(1, registers),
            3 => helpers::read_operand_addr_rising(0, registers, operand),
            4 => helpers::read_operand_addr_rising(1, registers, operand),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => helpers::read_operand_falling(0, registers, cycle, operand),
            2 => helpers::read_operand_falling(1, registers, cycle, operand),
            3 => {
                registers.accumulator &= !0x00ff;
                registers.accumulator |= registers.mdr as u16;

                if registers.processor_status.emulation || registers.processor_status.memory_select
                {
                    alu::emulation::nop(
                        registers.accumulator as u8,
                        &mut registers.processor_status,
                    );

                    registers.program_counter += 3;
                    *cycle = 0;
                } else {
                    *cycle = 4;
                }
                Ok(())
            }
            4 => {
                registers.accumulator &= !0xff00;
                registers.accumulator |= (registers.mdr as u16) << 8;

                alu::native::nop(registers.accumulator, &mut registers.processor_status);

                registers.program_counter += 3;
                *cycle = 0;
                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod absolute_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcodes::LDA_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0xd5;

        for _ in 0..4 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xd5, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn zero_emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0x00;
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcodes::LDA_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0x00;

        for _ in 0..4 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0x00, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::LDA_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0x46;
        memory[0x0202] = 0xd5;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xd546, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn zero_native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0x00;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::LDA_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0x00;
        memory[0x0202] = 0x00;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0x00, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
    }
}

pub type LdaAbsoluteLong = helpers::ArithmaticAbsoluteLongImpl<LdaAbsoluteLongImpl>;

#[derive(Copy, Clone, Debug, Default)]
pub struct LdaAbsoluteLongImpl;

impl helpers::ArithmaticAbsoluteLong for LdaAbsoluteLongImpl {
    fn emulation(
        &mut self,
        operand: u8,
        accumulator: &mut u8,
        status: &mut crate::cpu::ProcessorStatus,
    ) {
        *accumulator = alu::emulation::nop(operand, status);
    }

    fn native(
        &mut self,
        operand: u16,
        accumulator: &mut u16,
        status: &mut crate::cpu::ProcessorStatus,
    ) {
        *accumulator = alu::native::nop(operand, status);
    }
}

#[cfg(test)]
mod absolute_long_test {
    use crate::cpu::{control::helpers, test::*};

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;

        helpers::arithmatic_absolute_long::test::emulation(
            &mut cpu,
            opcodes::LDA_ABSOLUTE_LONG,
            0xa5,
        );

        assert_eq!(cpu.registers.accumulator, 0xa5, "Accumulator");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
    }

    #[test]
    fn native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcafd;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        helpers::arithmatic_absolute_long::test::native(
            &mut cpu,
            opcodes::LDA_ABSOLUTE_LONG,
            0xa5d2,
        );

        assert_eq!(cpu.registers.accumulator, 0xa5d2, "Accumulator");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
    }
}

#[derive(Copy, Clone, Debug, Default)]
pub struct LdaImmediate;

impl CpuControl for LdaImmediate {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => helpers::read_operand_rising(0, registers),
            2 => helpers::read_operand_rising(1, registers),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => {
                registers.accumulator &= !0x00ff;
                registers.accumulator |= registers.mdr as u16;

                if registers.processor_status.emulation || registers.processor_status.memory_select
                {
                    alu::emulation::nop(
                        registers.accumulator as u8,
                        &mut registers.processor_status,
                    );

                    registers.program_counter += 2;
                    *cycle = 0;
                } else {
                    *cycle = 2;
                }

                Ok(())
            }
            2 => {
                registers.accumulator &= !0xff00;
                registers.accumulator |= (registers.mdr as u16) << 8;

                alu::native::nop(registers.accumulator, &mut registers.processor_status);

                registers.program_counter += 3;
                *cycle = 0;

                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod immediate_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcodes::LDA_IMMEDIATE;
        memory[0x8001] = 0xd5;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xd5, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn zero_emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0x00;
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcodes::LDA_IMMEDIATE;
        memory[0x8001] = 0x00;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0x00, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::LDA_IMMEDIATE;
        memory[0x8001] = 0x46;
        memory[0x8002] = 0xd5;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xd546, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn zero_native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0x00;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::LDA_IMMEDIATE;
        memory[0x8001] = 0x00;
        memory[0x8002] = 0x00;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0x00, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
    }
}

#[derive(Clone, Copy, Debug)]
pub struct LdaDirect;

impl CpuControl for LdaDirect {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match *cycle {
            1 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    registers.program_bank,
                    registers.program_counter + 1,
                ))
                .valid_program_address(true)),
            2 => Ok(CpuOutputs::default_from_registers(*registers).address_bus(
                AddressBus::from_bank_addr(registers.program_bank, registers.program_counter + 1),
            )),
            3 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    0,
                    registers.direct + operand[0] as u16,
                ))
                .valid_data_address(true)
                .read_write(ReadWrite::Read)),
            4 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    0,
                    registers.direct + operand[0] as u16 + 1,
                ))
                .valid_data_address(true)
                .read_write(ReadWrite::Read)),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => {
                operand[0] = registers.mdr;

                if registers.direct & 0x00ff != 0 {
                    *cycle = 2;
                } else {
                    *cycle = 3;
                }

                Ok(())
            }
            2 => {
                *cycle = 3;
                Ok(())
            }
            3 => {
                registers.accumulator &= !0x00ff;
                registers.accumulator |= registers.mdr as u16;

                if registers.processor_status.emulation || registers.processor_status.memory_select
                {
                    alu::emulation::nop(
                        registers.accumulator as u8,
                        &mut registers.processor_status,
                    );

                    *cycle = 0;
                    registers.program_counter += 2;
                } else {
                    *cycle = 4;
                }
                Ok(())
            }
            4 => {
                operand[3] = registers.mdr;

                registers.accumulator &= !0xff00;
                registers.accumulator |= (registers.mdr as u16) << 8;

                alu::native::nop(registers.accumulator, &mut registers.processor_status);

                registers.program_counter += 2;
                *cycle = 0;
                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod direct_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation_direct_low_is_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        cpu.registers.direct = 0x0100;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::LDA_DIRECT_PAGE;
        memory[0x8001] = 0xcf;
        memory[0x01cf] = 0x35;

        for _ in 0..3 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0x35, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn emulation_direct_low_is_not_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        cpu.registers.direct = 0x0101;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::LDA_DIRECT_PAGE;
        memory[0x8001] = 0xcf;
        memory[0x01d0] = 0x35;

        for _ in 0..4 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0x35, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn native_direct_low_is_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.direct = 0x0100;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::LDA_DIRECT_PAGE;
        memory[0x8001] = 0xcf;
        memory[0x01cf] = 0x35;
        memory[0x01d0] = 0xda;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xda35, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }

    #[test]
    fn native_direct_low_is_not_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.direct = 0x0101;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::LDA_DIRECT_PAGE;
        memory[0x8001] = 0xcf;
        memory[0x01d0] = 0x35;
        memory[0x01d1] = 0xda;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xda35, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
    }
}

#[derive(Copy, Clone, Debug, Default)]
pub struct LdaDirectIndirectLongY;

impl CpuControl for LdaDirectIndirectLongY {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    registers.program_bank,
                    registers.program_counter + 1,
                ))
                .valid_data_address(false)
                .valid_program_address(true)),
            2 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    registers.program_bank,
                    registers.program_counter + 1,
                ))
                .valid_data_address(false)
                .valid_program_address(false)),

            3 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    0x00,
                    registers.direct + operand[0] as u16,
                ))
                .valid_data_address(true)
                .valid_program_address(false)),
            4 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    0x00,
                    registers.direct + operand[0] as u16 + 1,
                ))
                .valid_data_address(true)
                .valid_program_address(false)),
            5 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    0x00,
                    registers.direct + operand[0] as u16 + 2,
                ))
                .valid_data_address(true)
                .valid_program_address(false)),

            6 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    operand[3],
                    u16::from_le_bytes(operand[1..3].try_into().unwrap()) + registers.index_y,
                ))
                .valid_data_address(true)
                .valid_program_address(false)),
            7 => Ok(CpuOutputs::default_from_registers(*registers)
                .address_bus(AddressBus::from_bank_addr(
                    operand[3],
                    u16::from_le_bytes(operand[1..3].try_into().unwrap()) + registers.index_y + 1,
                ))
                .valid_data_address(true)
                .valid_program_address(false)),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => {
                operand[0] = registers.mdr;
                if registers.direct & 0x00ff == 0x00 {
                    *cycle = 3;
                } else {
                    *cycle = 2;
                }
                Ok(())
            }
            2 => {
                *cycle = 3;
                Ok(())
            }
            3 => {
                operand[1] = registers.mdr;
                *cycle = 4;
                Ok(())
            }
            4 => {
                operand[2] = registers.mdr;
                *cycle = 5;
                Ok(())
            }
            5 => {
                operand[3] = registers.mdr;
                *cycle = 6;
                Ok(())
            }
            6 => {
                registers.accumulator &= !0x00ff;
                registers.accumulator |= registers.mdr as u16;

                if registers.processor_status.emulation || registers.processor_status.memory_select
                {
                    alu::emulation::nop(
                        registers.accumulator as u8,
                        &mut registers.processor_status,
                    );

                    registers.program_counter += 2;
                    *cycle = 0;
                } else {
                    *cycle = 7;
                }
                Ok(())
            }
            7 => {
                registers.accumulator &= !0xff00;
                registers.accumulator |= (registers.mdr as u16) << 8;

                alu::native::nop(registers.accumulator, &mut registers.processor_status);

                registers.program_counter += 2;
                *cycle = 0;
                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod direct_indirect_long_y_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation_direct_low_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        cpu.registers.direct = 0x0100;
        cpu.registers.index_y = 0x0023;
        let mut memory = [0x00; 0x04ffff];

        memory[0x8000] = opcodes::LDA_DP_INDIRECT_LONG_INDEXED_Y;
        memory[0x8001] = 0x23;
        memory[0x0123] = 0x01;
        memory[0x0124] = 0x02;
        memory[0x0125] = 0x03;
        memory[0x030224] = 0xd5;

        for _ in 0..6 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xd5, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn emulation_direct_low_nonzero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        cpu.registers.direct = 0x0103;
        cpu.registers.index_y = 0x0023;
        let mut memory = [0x00; 0x04ffff];

        memory[0x8000] = opcodes::LDA_DP_INDIRECT_LONG_INDEXED_Y;
        memory[0x8001] = 0x20;
        memory[0x0123] = 0x01;
        memory[0x0124] = 0x02;
        memory[0x0125] = 0x03;
        memory[0x030224] = 0xd5;

        for _ in 0..7 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xd5, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn emulation_zero_direct_low_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        cpu.registers.direct = 0x0100;
        cpu.registers.index_y = 0x0023;
        let mut memory = [0x00; 0x04ffff];

        memory[0x8000] = opcodes::LDA_DP_INDIRECT_LONG_INDEXED_Y;
        memory[0x8001] = 0x23;
        memory[0x0123] = 0x01;
        memory[0x0124] = 0x02;
        memory[0x0125] = 0x03;
        memory[0x030224] = 0x00;

        for _ in 0..6 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0x00, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn emulation_zero_direct_low_nonzero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        cpu.registers.direct = 0x0103;
        cpu.registers.index_y = 0x0023;
        let mut memory = [0x00; 0x04ffff];

        memory[0x8000] = opcodes::LDA_DP_INDIRECT_LONG_INDEXED_Y;
        memory[0x8001] = 0x20;
        memory[0x0123] = 0x01;
        memory[0x0124] = 0x02;
        memory[0x0125] = 0x03;
        memory[0x030224] = 0x00;

        for _ in 0..7 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0x00, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn native_direct_low_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.direct = 0x0100;
        cpu.registers.index_y = 0x0023;
        let mut memory = [0x00; 0x04ffff];

        memory[0x8000] = opcodes::LDA_DP_INDIRECT_LONG_INDEXED_Y;
        memory[0x8001] = 0x23;
        memory[0x0123] = 0x01;
        memory[0x0124] = 0x02;
        memory[0x0125] = 0x03;
        memory[0x030224] = 0x46;
        memory[0x030225] = 0xd5;

        for _ in 0..7 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xd546, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn native_direct_low_nonzero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.direct = 0x0103;
        cpu.registers.index_y = 0x0023;
        let mut memory = [0x00; 0x04ffff];

        memory[0x8000] = opcodes::LDA_DP_INDIRECT_LONG_INDEXED_Y;
        memory[0x8001] = 0x20;
        memory[0x0123] = 0x01;
        memory[0x0124] = 0x02;
        memory[0x0125] = 0x03;
        memory[0x030224] = 0x46;
        memory[0x030225] = 0xd5;

        for _ in 0..8 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xd546, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn native_zero_direct_low_zero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.accumulator = 0xca;
        cpu.registers.direct = 0x0100;
        cpu.registers.index_y = 0x0023;
        let mut memory = [0x00; 0x04ffff];

        memory[0x8000] = opcodes::LDA_DP_INDIRECT_LONG_INDEXED_Y;
        memory[0x8001] = 0x23;
        memory[0x0123] = 0x01;
        memory[0x0124] = 0x02;
        memory[0x0125] = 0x03;
        memory[0x030224] = 0x00;
        memory[0x030224] = 0x00;

        for _ in 0..7 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0x00, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn native_zero_direct_low_nonzero() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;
        cpu.registers.accumulator = 0xca;
        cpu.registers.direct = 0x0103;
        cpu.registers.index_y = 0x0023;
        let mut memory = [0x00; 0x04ffff];

        memory[0x8000] = opcodes::LDA_DP_INDIRECT_LONG_INDEXED_Y;
        memory[0x8001] = 0x20;
        memory[0x0123] = 0x01;
        memory[0x0124] = 0x02;
        memory[0x0125] = 0x03;
        memory[0x030224] = 0x00;
        memory[0x030224] = 0x00;

        for _ in 0..8 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0x00, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8002, "Program counter");
        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
    }
}
