use super::prelude::*;

#[derive(Clone, Copy, Debug)]
pub struct OraAbsolute;

impl CpuControl for OraAbsolute {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => helpers::read_operand_rising(0, registers),
            2 => helpers::read_operand_rising(1, registers),
            3 => helpers::read_operand_addr_rising(0, registers, operand),
            4 => helpers::read_operand_addr_rising(1, registers, operand),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => helpers::read_operand_falling(0, registers, cycle, operand),
            2 => helpers::read_operand_falling(1, registers, cycle, operand),
            3 => {
                registers.accumulator |= registers.mdr as u16;

                if registers.processor_status.emulation || registers.processor_status.memory_select
                {
                    registers.processor_status.zero = registers.accumulator == 0;
                    registers.processor_status.negative = registers.accumulator & 0x0080 != 0;

                    registers.program_counter += 3;
                    *cycle = 0;
                } else {
                    *cycle = 4;
                }
                Ok(())
            }
            4 => {
                registers.accumulator |= (registers.mdr as u16) << 8;

                registers.processor_status.zero = registers.accumulator == 0;
                registers.processor_status.negative = registers.accumulator & 0x8000 != 0;

                registers.program_counter += 3;
                *cycle = 0;
                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod absolute_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcodes::ORA_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0x35;

        for _ in 0..4 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xca | 0x35, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn zero_emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0x00;
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcodes::ORA_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0x00;

        for _ in 0..4 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0x00, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::ORA_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0x46;
        memory[0x0202] = 0x35;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0xcab2 | 0x3546, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
    }

    #[test]
    fn zero_native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0x00;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::ORA_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0x00;
        memory[0x0202] = 0x00;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.accumulator, 0x00, "Accumulator");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
    }
}
