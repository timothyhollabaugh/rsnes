use super::prelude::*;

pub type DecAccumulator = helpers::ArithmaticAccumulatorImpl<DecAccumulatorImpl>;

#[derive(Clone, Copy, Debug, Default)]
pub struct DecAccumulatorImpl;

impl helpers::ArithmaticAccumulator for DecAccumulatorImpl {
    fn emulation(&mut self, accumulator: &mut u8, status: &mut crate::cpu::ProcessorStatus) {
        *accumulator = alu::emulation::sub(*accumulator, 1, status)
    }

    fn native(&mut self, accumulator: &mut u16, status: &mut crate::cpu::ProcessorStatus) {
        *accumulator = alu::native::sub(*accumulator, 1, status)
    }
}

#[cfg(test)]
mod dec_accumulator_test {
    use crate::cpu::{control::helpers, test::*};

    #[test]
    fn dec_accumulator() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;

        helpers::test_arithmatic_accumulator(&mut cpu, opcodes::DEC_ACCUMULATOR);
        assert_eq!(cpu.registers.accumulator, 0xc9, "Accumulator");
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Dex;

impl CpuControl for Dex {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        registers.index_x -= 1;
        registers.processor_status.zero = registers.index_x == 0;
        registers.processor_status.negative = registers.index_x & 0x8000 != 0;
        Ok(CpuOutputs::default_from_registers(*registers))
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        registers.program_counter += 1;
        *cycle = 0;
        Ok(())
    }
}

#[cfg(test)]
mod dex_test {
    use crate::cpu::test::*;

    #[test]
    fn dex() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.index_x = 0xca;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::DEX;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.index_x, 0xc9, "Index X");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Dey;

impl CpuControl for Dey {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        registers.index_y -= 1;
        registers.processor_status.zero = registers.index_y == 0;
        registers.processor_status.negative = registers.index_y & 0x8000 != 0;
        Ok(CpuOutputs::default_from_registers(*registers))
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        registers.program_counter += 1;
        *cycle = 0;
        Ok(())
    }
}

#[cfg(test)]
mod dey_test {
    use crate::cpu::test::*;

    #[test]
    fn dey() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.index_y = 0xca;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::DEY;

        for _ in 0..2 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert_eq!(cpu.registers.index_y, 0xc9, "Index Y");
        assert_eq!(cpu.registers.program_counter, 0x8001, "Program counter");
    }
}
