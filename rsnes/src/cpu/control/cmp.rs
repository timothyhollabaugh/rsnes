use super::prelude::*;

#[derive(Clone, Copy, Debug)]
pub struct CmpAbsolute;

impl CpuControl for CmpAbsolute {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        match cycle {
            1 => helpers::read_operand_rising(0, registers),
            2 => helpers::read_operand_rising(1, registers),
            3 => helpers::read_operand_addr_rising(0, registers, operand),
            4 => helpers::read_operand_addr_rising(1, registers, operand),
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }

    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        match cycle {
            1 => helpers::read_operand_falling(0, registers, cycle, operand),
            2 => helpers::read_operand_falling(1, registers, cycle, operand),
            3 => {
                operand[2] = registers.mdr;

                if registers.processor_status.emulation || registers.processor_status.memory_select
                {
                    let result =
                        u8::wrapping_sub((registers.accumulator & 0x00ff) as u8, operand[2]);
                    registers.processor_status.zero = result == 0;
                    registers.processor_status.negative = result & 0x80 != 0;
                    //registers.processor_status.carry =
                    //(registers.accumulator & 0x00ff) as u8 >= operand[2];

                    registers.program_counter += 3;
                    *cycle = 0;
                } else {
                    *cycle = 4;
                }
                Ok(())
            }
            4 => {
                operand[3] = registers.mdr;

                let o = u16::from_le_bytes(operand[2..4].try_into().unwrap());
                let result = u16::wrapping_sub(registers.accumulator, o);
                registers.processor_status.zero = result == 0;
                registers.processor_status.negative = result & 0x8000 != 0;
                //registers.processor_status.carry = registers.accumulator >= o;

                registers.program_counter += 3;
                *cycle = 0;
                Ok(())
            }
            _ => Err(CpuError::InvalidCycle(*cycle)),
        }
    }
}

#[cfg(test)]
mod absolute_test {
    use crate::cpu::test::*;

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcodes::CMP_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0xd5;

        for _ in 0..4 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
        assert!(!cpu.registers.processor_status.carry, "Carry flag");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
    }

    #[test]
    fn zero_emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;
        let mut memory = [0x00; 0xffff];

        memory[0x8000] = opcodes::CMP_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0xca;

        for _ in 0..4 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
        assert!(!cpu.registers.processor_status.carry, "Carry flag");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
    }

    #[test]
    fn native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::CMP_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0x46;
        memory[0x0202] = 0xd5;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
        assert!(!cpu.registers.processor_status.carry, "Carry flag");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
    }

    #[test]
    fn zero_native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        let mut memory = [0x00; 0xffff];
        memory[0x8000] = opcodes::CMP_ABSOLUTE;
        memory[0x8001] = 0x01;
        memory[0x8002] = 0x02;
        memory[0x0201] = 0xb2;
        memory[0x0202] = 0xca;

        for _ in 0..5 {
            clock_cpu(&mut cpu, &mut memory).unwrap();
        }

        println!("{:#04x?}", &cpu);

        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
        assert!(!cpu.registers.processor_status.carry, "Carry flag");
        assert_eq!(cpu.registers.program_counter, 0x8003, "Program counter");
    }
}

pub type CmpImmediate = helpers::ArithmaticImmediateImpl<CmpImmediateImpl>;

#[derive(Copy, Clone, Debug, Default)]
pub struct CmpImmediateImpl;

impl helpers::ArithmaticImmediate for CmpImmediateImpl {
    fn emulation(
        &mut self,
        operand: u8,
        accumulator: &mut u8,
        status: &mut crate::cpu::ProcessorStatus,
    ) {
        alu::emulation::sub(*accumulator, operand, status);
    }

    fn native(
        &mut self,
        operand: u16,
        accumulator: &mut u16,
        status: &mut crate::cpu::ProcessorStatus,
    ) {
        alu::native::sub(*accumulator, operand, status);
    }
}

#[cfg(test)]
mod immediate_test {
    use crate::cpu::{control::helpers, test::*};

    #[test]
    fn emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;

        helpers::arithmatic_immediate_test::emulation(&mut cpu, opcodes::CMP_IMMEDIATE, 0xa5);

        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
        assert!(!cpu.registers.processor_status.carry, "Carry flag");
    }

    #[test]
    fn emulation_carry() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;

        helpers::arithmatic_immediate_test::emulation(&mut cpu, opcodes::CMP_IMMEDIATE, 0xd5);

        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
        assert!(cpu.registers.processor_status.carry, "Carry flag");
    }

    #[test]
    fn zero_emulation() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xca;

        helpers::arithmatic_immediate_test::emulation(&mut cpu, opcodes::CMP_IMMEDIATE, 0xca);

        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
        assert!(!cpu.registers.processor_status.carry, "Carry flag");
    }

    #[test]
    fn native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        helpers::arithmatic_immediate_test::native(&mut cpu, opcodes::CMP_IMMEDIATE, 0xa546);

        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
        assert!(!cpu.registers.processor_status.carry, "Carry flag");
    }

    #[test]
    fn native_carry() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        helpers::arithmatic_immediate_test::native(&mut cpu, opcodes::CMP_IMMEDIATE, 0xd546);

        assert!(!cpu.registers.processor_status.zero, "Zero flag");
        assert!(cpu.registers.processor_status.negative, "Negative flag");
        assert!(cpu.registers.processor_status.carry, "Carry flag");
    }

    #[test]
    fn zero_native() {
        let mut cpu = Cpu65816::new(test_exception_vectors());
        cpu.registers.accumulator = 0xcab2;
        cpu.registers.processor_status.emulation = false;
        cpu.registers.processor_status.memory_select = false;

        helpers::arithmatic_immediate_test::native(&mut cpu, opcodes::CMP_IMMEDIATE, 0xcab2);

        assert!(cpu.registers.processor_status.zero, "Zero flag");
        assert!(!cpu.registers.processor_status.negative, "Negative flag");
        assert!(!cpu.registers.processor_status.carry, "Carry flag");
    }
}
