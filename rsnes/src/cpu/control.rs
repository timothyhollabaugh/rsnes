mod adc;
mod branch;
mod cmp;
mod cpx;
mod cpy;
mod dec;
pub mod helpers;
mod inc;
mod jsr;
mod lda;
mod ldx;
mod ldy;
mod ora;
mod rol;
mod sbc;
mod sta;
mod stack;
mod status;
mod sty;
mod stz;
mod transfer;

pub use adc::*;
pub use branch::*;
pub use cmp::*;
pub use cpx::*;
pub use cpy::*;
pub use dec::*;
pub use inc::*;
pub use jsr::*;
pub use lda::*;
pub use ldx::*;
pub use ldy::*;
pub use ora::*;
pub use rol::*;
pub use sbc::*;
pub use sta::*;
pub use stack::*;
pub use status::*;
pub use sty::*;
pub use stz::*;
pub use transfer::*;

use super::{CpuError, CpuOutputs, CpuRegisters};
use std::fmt::Debug;

pub mod prelude {
    pub use super::helpers;
    pub use super::CpuControl;
    pub use crate::{
        cpu::{alu, opcodes, CpuError, CpuOutputs, CpuRegisters},
        snes::{AddressBus, DataBus, ReadWrite},
    };
}

pub trait CpuControl: Debug {
    fn clock_rising(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError>;
    fn clock_falling(
        &mut self,
        registers: &mut CpuRegisters,
        cycle: &mut u8,
        operand: &mut [u8; 4],
    ) -> Result<(), CpuError>;
}

#[derive(Copy, Clone, Debug, Default)]
pub struct Unhandled;

impl CpuControl for Unhandled {
    fn clock_rising(
        &mut self,
        _registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<CpuOutputs, CpuError> {
        Err(CpuError::UnhandledOpcode)
    }

    fn clock_falling(
        &mut self,
        _registers: &mut CpuRegisters,
        _cycle: &mut u8,
        _operand: &mut [u8; 4],
    ) -> Result<(), CpuError> {
        Err(CpuError::UnhandledOpcode)
    }
}
