pub use crate::{
    rom::ExceptionVectors,
    snes::{DataBus, ReadWrite},
};

pub use super::{
    opcodes::{self, LDA_ABSOLUTE},
    Cpu65816, CpuError, CpuInputs,
};

pub fn clock_cpu(cpu: &mut Cpu65816, memory: &mut [u8]) -> Result<(), CpuError> {
    let cpu_outputs = cpu.clock_rising()?;

    let data_bus = if let Some(address_bus) = cpu_outputs.address_bus.into_valid() {
        match cpu_outputs.read_write {
            ReadWrite::Read => DataBus::maybe_data(memory.get(address_bus.index()).cloned()),
            ReadWrite::Write => {
                if let (Ok(data_bus), Some(memory_data)) = (
                    cpu_outputs.data_bus.valid(),
                    memory.get_mut(address_bus.index()),
                ) {
                    *memory_data = data_bus;
                }
                DataBus::invalid()
            }
        }
    } else {
        DataBus::invalid()
    };

    println!(
        "Bank {} Addr {} Data {}",
        if let Some(address_bus) = cpu_outputs.address_bus.into_valid() {
            format!("{:02x?}", address_bus.bank())
        } else {
            "--".to_string()
        },
        if let Some(address_bus) = cpu_outputs.address_bus.into_valid() {
            format!("{:04x?}", address_bus.addr())
        } else {
            "----".to_string()
        },
        if let Ok(data_bus) = data_bus.valid() {
            format!("{:02x?}", data_bus)
        } else {
            "--".to_string()
        },
    );

    let cpu_inputs = CpuInputs {
        data_bus,
        abortb: true,
        interrupt_request: true,
        nonmaskable_interrupt: true,
        reset: true,
        ready: None,
        bus_enable: true,
    };

    cpu.clock_falling(cpu_inputs)?;

    Ok(())
}

pub fn test_exception_vectors() -> ExceptionVectors {
    ExceptionVectors {
        cop_native: 0x00,
        brk_native: 0x00,
        abort_native: 0x00,
        nmi_native: 0x00,
        irq_native: 0x00,
        cop_emu: 0x00,
        abort_emu: 0x00,
        nmi_emu: 0x00,
        reset_emu: 0x8000,
        irq_break_emu: 0x00,
    }
}
