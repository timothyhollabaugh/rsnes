pub mod emulation {
    #![allow(unused)]
    use crate::cpu::ProcessorStatus;

    /// op1 + op2 + carry, updating the carry flag
    pub fn add_carry(op1: u8, op2: u8, status: &mut ProcessorStatus) -> u8 {
        let negative_before = op1 & 0x80 != 0;
        let (r, carry) = u8::carrying_add(op1, op2, status.carry);
        status.carry = carry;
        status.negative = r & 0x80 != 0;
        status.zero = r == 0;
        status.overflow = negative_before != status.negative;
        r
    }

    /// op1 + op2, but doesn't update the overflow flag
    pub fn add(op1: u8, op2: u8, status: &mut ProcessorStatus) -> u8 {
        let (r, carry) = u8::carrying_add(op1, op2, false);
        status.carry = carry;
        status.negative = r & 0x80 != 0;
        status.zero = r == 0;
        r
    }

    /// op1 - op2 - carry, updating the carry flag
    pub fn sub_carry(op1: u8, op2: u8, status: &mut ProcessorStatus) -> u8 {
        let negative_before = op1 & 0x80 != 0;
        let (r, carry) = u8::borrowing_sub(op1, op2, status.carry);
        status.carry = carry;
        status.negative = r & 0x80 != 0;
        status.zero = r == 0;
        status.overflow = negative_before != status.negative;
        r
    }

    /// op1 - op2, but doesn't update the overflow flag
    pub fn sub(op1: u8, op2: u8, status: &mut ProcessorStatus) -> u8 {
        let (r, carry) = u8::borrowing_sub(op1, op2, false);
        status.carry = carry;
        status.negative = r & 0x80 != 0;
        status.zero = r == 0;
        r
    }

    /// op1 & op2
    pub fn and(op1: u8, op2: u8, status: &mut ProcessorStatus) -> u8 {
        let r = op1 & op2;
        status.negative = r & 0x80 != 0;
        status.zero = r == 0;
        r
    }

    /// op1 | op2
    pub fn or(op1: u8, op2: u8, status: &mut ProcessorStatus) -> u8 {
        let r = op1 | op2;
        status.negative = r & 0x80 != 0;
        status.zero = r == 0;
        r
    }

    /// op1 ^ op2
    pub fn xor(op1: u8, op2: u8, status: &mut ProcessorStatus) -> u8 {
        let r = op1 ^ op2;
        status.negative = r & 0x80 != 0;
        status.zero = r == 0;
        r
    }

    /// op1 << 1, updating the carry flag with leftmost bit of op1
    pub fn shift_left(op1: u8, status: &mut ProcessorStatus) -> u8 {
        let r = op1 << 1;
        status.carry = op1 & 0x80 != 0;
        status.negative = r & 0x80 != 0;
        status.zero = r == 0;
        r
    }

    /// (op1 << 1) | carry, updating the carry flag with leftmost bit of op1
    pub fn rotate_left(op1: u8, status: &mut ProcessorStatus) -> u8 {
        let mut r = op1 << 1;
        r |= status.carry as u8;
        status.carry = op1 & 0x80 != 0;
        status.negative = r & 0x80 != 0;
        status.zero = r == 0;
        r
    }

    /// Do nothing to the operand, but set status flags negative and zero
    /// appropriately
    pub fn nop(op1: u8, status: &mut ProcessorStatus) -> u8 {
        let r = op1;
        status.negative = r & 0x80 != 0;
        status.zero = r == 0;
        r
    }
}

pub mod native {
    #![allow(unused)]
    use crate::cpu::ProcessorStatus;

    /// op1 + op2 + carry, updating the carry and overflow flag
    pub fn add_carry(op1: u16, op2: u16, status: &mut ProcessorStatus) -> u16 {
        let negative_before = op1 & 0x8000 != 0;
        let (r, carry) = u16::carrying_add(op1, op2, status.carry);
        status.carry = carry;
        status.negative = r & 0x8000 != 0;
        status.zero = r == 0;
        status.overflow = negative_before != status.negative;
        r
    }

    /// op1 + op2, but doesn't update the overflow flag
    pub fn add(op1: u16, op2: u16, status: &mut ProcessorStatus) -> u16 {
        let (r, carry) = u16::carrying_add(op1, op2, false);
        status.carry = carry;
        status.negative = r & 0x8000 != 0;
        status.zero = r == 0;
        r
    }

    /// op1 - op2 - carry, updating the carry and overflow flag
    pub fn sub_carry(op1: u16, op2: u16, status: &mut ProcessorStatus) -> u16 {
        let negative_before = op1 & 0x8000 != 0;
        let (r, carry) = u16::borrowing_sub(op1, op2, status.carry);
        status.carry = carry;
        status.negative = r & 0x8000 != 0;
        status.zero = r == 0;
        status.overflow = negative_before != status.negative;
        r
    }

    /// op1 - op2, but doesn't update the overflow flag
    pub fn sub(op1: u16, op2: u16, status: &mut ProcessorStatus) -> u16 {
        let (r, carry) = u16::borrowing_sub(op1, op2, false);
        status.carry = carry;
        status.negative = r & 0x8000 != 0;
        status.zero = r == 0;
        r
    }

    /// op1 & op2
    pub fn and(op1: u16, op2: u16, status: &mut ProcessorStatus) -> u16 {
        let r = op1 & op2;
        status.negative = r & 0x8000 != 0;
        status.zero = r == 0;
        r
    }

    /// op1 | op2
    pub fn or(op1: u16, op2: u16, status: &mut ProcessorStatus) -> u16 {
        let r = op1 | op2;
        status.negative = r & 0x8000 != 0;
        status.zero = r == 0;
        r
    }

    /// op1 ^ op2
    pub fn xor(op1: u16, op2: u16, status: &mut ProcessorStatus) -> u16 {
        let r = op1 ^ op2;
        status.negative = r & 0x8000 != 0;
        status.zero = r == 0;
        r
    }

    /// op1 << 1, updating the carry flag with leftmost bit of op1
    pub fn shift_left(op1: u16, status: &mut ProcessorStatus) -> u16 {
        let r = op1 << 1;
        status.carry = op1 & 0x8000 != 0;
        status.negative = r & 0x8000 != 0;
        status.zero = r == 0;
        r
    }

    /// (op1 << 1) | carry, updating the carry flag with leftmost bit of op1
    pub fn rotate_left(op1: u16, status: &mut ProcessorStatus) -> u16 {
        let mut r = op1 << 1;
        r |= op1 & 0x01;
        status.carry = op1 & 0x8000 != 0;
        status.negative = r & 0x8000 != 0;
        status.zero = r == 0;
        r
    }

    /// Do nothing to the operand, but set negative and zero
    /// status flags appropriately
    pub fn nop(op1: u16, status: &mut ProcessorStatus) -> u16 {
        let r = op1;
        status.negative = r & 0x8000 != 0;
        status.zero = r == 0;
        r
    }
}
