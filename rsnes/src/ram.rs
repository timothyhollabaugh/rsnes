use crate::snes::{AddressBus, DataBus, DataBusInvalid, ReadWrite};

#[derive(Copy, Clone, Debug)]
pub enum RamError {
    UnvalidDataBus,
}

impl From<DataBusInvalid> for RamError {
    fn from(_: DataBusInvalid) -> Self {
        RamError::UnvalidDataBus
    }
}

#[derive(Clone, Debug)]
pub struct Ram([u8; 128 * 1024]);

impl Ram {
    pub fn new() -> Ram {
        Ram([0; 128 * 1024])
    }

    pub fn clock(
        &mut self,
        address: AddressBus,
        data_bus: DataBus,
        read_write: ReadWrite,
    ) -> Result<DataBus, RamError> {
        if let Some(bus) = address.into_valid() {
            if ((bus.region() == 0 || bus.region() == 2) && bus.addr() < 0x2000)
                || bus.bank() == 0x7e
            {
                match read_write {
                    ReadWrite::Read => Ok(DataBus::from_data(self.0[bus.addr() as usize])),
                    ReadWrite::Write => {
                        self.0[bus.addr() as usize] = data_bus.valid()?;
                        Ok(DataBus::invalid())
                    }
                }
            } else if bus.bank() == 0x7f {
                match read_write {
                    ReadWrite::Read => {
                        Ok(DataBus::from_data(self.0[bus.addr() as usize + 0x10000]))
                    }
                    ReadWrite::Write => {
                        self.0[bus.addr() as usize + 0x10000] = data_bus.valid()?;
                        Ok(DataBus::invalid())
                    }
                }
            } else {
                Ok(DataBus::invalid())
            }
        } else {
            Ok(DataBus::invalid())
        }
    }
}
