#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

use std::sync::Arc;

use parking_lot::Mutex;

use eframe::egui;
use rsnes::{cpu::Cpu65816, opcodes, rom::Coprocessor, Rom, Snes, ppu::Ppu};
use slotmap::{DenseSlotMap, SecondaryMap};

fn main() {
    let options = eframe::NativeOptions {
        vsync: true,
        ..Default::default()
    };
    eframe::run_native(
        "rsnes",
        options,
        Box::new(|_cc| {
            Box::new(RsnesGui {
                rom_file_path: None,
                debugger_handle: None,
                timing_clock: None,
                f: 0.0,
                watchpoints: Watchpoints { points: DenseSlotMap::with_key() },
            })
        }),
    );
}

#[derive(Clone)]
struct Watchpoints {
    points: DenseSlotMap<WatchpointKey, Watchpoint>,
}

slotmap::new_key_type! { struct WatchpointKey; }

#[derive(Clone)]
struct Watchpoint {
    name: String,
    expression: fn(&Snes) -> u64,
    format: fn(u64) -> String,
    conditions: Vec<Breakpoint>,
}

impl Watchpoint {
    fn new(
        name: String,
        expression: fn(&Snes) -> u64,
        format: fn(u64) -> String,
    ) -> Watchpoint {
        Watchpoint {
            name,
            expression,
            format,
            conditions: Vec::new(),
        }
    }

    fn formatted(&self, snes: &Snes) -> String {
        let d = (self.expression)(snes);
        (self.format)(d)
    }
}

#[derive(Clone)]
struct Breakpoint {
    enabled: bool,
    condition: BreakpointCondition,
    condition_value: u64,
    only_change: bool,
}

#[derive(Clone, PartialEq)]
enum BreakpointCondition {
    Always,
    Equal,
    NotEqual,
    LessThan,
    GreaterThan,
}

impl std::fmt::Display for BreakpointCondition {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            BreakpointCondition::Always => write!(f, "Always"),
            BreakpointCondition::Equal => write!(f, "Equal"),
            BreakpointCondition::NotEqual => write!(f, "Not Equal"),
            BreakpointCondition::LessThan => write!(f, "Less"),
            BreakpointCondition::GreaterThan => write!(f, "Greater"),
        }
    }
}

#[derive(Clone)]
enum ToDebuggerMsg {
    Clock(u64),
    Run(Watchpoints),
    Stop,
}

#[derive(Clone)]
enum DebuggerState {
    Idle,
    Clock(u64),
    Run,
}

struct DebuggerHandle {
    //pub to_debugger: crossbeam::channel::Sender<ToDebuggerMsg>,
    pub shared_command: Arc<Mutex<Option<ToDebuggerMsg>>>,
    pub shared_snes: Arc<Mutex<Option<Snes>>>,
}

struct Debugger<F: FnMut()> {
    rom_file_path: std::path::PathBuf,
    shared_command: Arc<Mutex<Option<ToDebuggerMsg>>>,
    shared_snes: Arc<Mutex<Option<Snes>>>,
    //from_handle: crossbeam::channel::Receiver<ToDebuggerMsg>,
    on_send: F,
}

impl<F: FnMut()> Debugger<F> {
    fn new(rom_file_path: std::path::PathBuf, on_send: F) -> (Debugger<F>, DebuggerHandle) {
        //let (to_debugger, from_handle) = crossbeam::channel::unbounded();

        let shared_command = Arc::new(Mutex::new(None));
        let shared_snes = Arc::new(Mutex::new(None));

        let debugger = Debugger {
            rom_file_path,
            shared_command: shared_command.clone(),
            shared_snes: shared_snes.clone(),
            //from_handle,
            on_send,
        };

        let handle = DebuggerHandle {
            //to_debugger,
            shared_command,
            shared_snes,
        };

        (debugger, handle)
    }

    fn run(mut self) {
        let rom_dump = std::fs::read(self.rom_file_path).expect("Could not read ROM");

        let dump_header = rom_dump[0x08] == 0xaa && rom_dump[0x09] == 0xbb;

        let rom = if dump_header {
            rom_dump[0x200..].to_owned()
        } else {
            rom_dump
        };

        let rom = Rom::new(rom).unwrap();

        println!("Loaded ROM");
        println!("Header: {:#02x?}", rom.header());
        println!("Exception Vectors: {:#02x?}", rom.exception_vectors());

        let mut snes = Snes::new(rom);
        //let mut last_snes = snes.clone();

        let mut watchpoints = Watchpoints {
            points: DenseSlotMap::with_key(),
        };

        let mut last_watchpoints: SecondaryMap<WatchpointKey, u64> = SecondaryMap::new();

        //let mut master_clock = 21.4772700f64;
        let mut state = DebuggerState::Idle;

        let mut shared_snes = self.shared_snes.lock();
        *shared_snes = Some(snes.clone());
        (self.on_send)();
        drop(shared_snes);

        let mut last_send = 0;

        loop {
            // This drops the f from 91MHz to 40MHz
            if let Some(mut cmd) = self.shared_command.try_lock() {
                if let Some(cmd) = cmd.take() {
                    match cmd {
                        ToDebuggerMsg::Clock(clocks) => state = DebuggerState::Clock(clocks),
                        ToDebuggerMsg::Run(new_watchpoints) => {
                            watchpoints = new_watchpoints;
                            state = DebuggerState::Run;
                        }
                        ToDebuggerMsg::Stop => {
                            let mut shared_snes = self.shared_snes.lock();
                            *shared_snes = Some(snes.clone());
                            (self.on_send)();
                            last_send = snes.clock;
                            state = DebuggerState::Idle;
                        }
                    }
                }
            }

            match state {
                DebuggerState::Idle => {}
                DebuggerState::Clock(clocks) => {
                    println!("clock {}", clocks);
                    snes.clock();

                    //if let Ok(mut shared_snes) = self.shared_snes.try_lock() {
                    //*shared_snes = Some(snes.clone());
                    //(self.on_send)();
                    //}

                    if (clocks) > 1 {
                        state = DebuggerState::Clock(clocks - 1);
                    } else {
                        let mut shared_snes = self.shared_snes.lock();
                        *shared_snes = Some(snes.clone());
                        (self.on_send)();
                        last_send = snes.clock;
                        state = DebuggerState::Idle;
                    }
                }
                DebuggerState::Run => {
                    snes.clock();

                    for (key, watchpoint) in watchpoints.points.iter() {
                        let value = (watchpoint.expression)(&snes);
                        let last_value = last_watchpoints.get(key).cloned();

                        let matching_conditions = watchpoint.conditions.iter().filter(|breakpoint| {
                            breakpoint.enabled && 
                                (!breakpoint.only_change || last_value.map(|last_value| value != last_value).unwrap_or(true)) &&
                                (
                                    (breakpoint.condition == BreakpointCondition::Always) ||
                                    (breakpoint.condition == BreakpointCondition::Equal && value == breakpoint.condition_value) ||
                                    (breakpoint.condition == BreakpointCondition::NotEqual && value != breakpoint.condition_value) ||
                                    (breakpoint.condition == BreakpointCondition::LessThan && value < breakpoint.condition_value) ||
                                    (breakpoint.condition == BreakpointCondition::GreaterThan && value > breakpoint.condition_value)
                                )
                        });

                        for _ in matching_conditions {
                            let mut shared_snes = self.shared_snes.lock();
                            *shared_snes = Some(snes.clone());
                            (self.on_send)();
                            last_send = snes.clock;
                            state = DebuggerState::Idle;
                        }

                        last_watchpoints.insert(key, value);
                    }
                }
            }

            if snes.clock - last_send >= 100_000 {
                if let Some(mut shared_snes) = self.shared_snes.try_lock() {
                    *shared_snes = Some(snes.clone());
                    (self.on_send)();
                }
                last_send = snes.clock;
            }

            //last_snes = snes.clone();

            //let now = std::time::Instant::now();

            //if now.duration_since(last_send) > std::time::Duration::from_millis(100) {
            //}

            //let delay = 1000.0 / master_clock;
            //std::thread::sleep(std::time::Duration::from_nanos(delay as u64));
        }
    }
}

struct RsnesGui {
    rom_file_path: Option<std::path::PathBuf>,
    debugger_handle: Option<DebuggerHandle>,
    timing_clock: Option<(u64, std::time::Instant)>,
    f: f64,
    watchpoints: Watchpoints,
}

impl eframe::App for RsnesGui {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            if self.debugger_handle.is_none() && ui.button("Open ROM").clicked() {
                if let Some(path) = rfd::FileDialog::new().pick_file() {
                    self.rom_file_path = Some(path);
                }
            }

            if let Some(rom_file_path) = &self.rom_file_path {
                ui.horizontal(|ui| {
                    ui.label("ROM:");
                    ui.monospace(rom_file_path.display().to_string());

                    if self.debugger_handle.is_none() {
                        if ui.button("Load").clicked() {
                            let ctx2 = ctx.clone();
                            let (debugger, handle) =
                                Debugger::new(rom_file_path.clone(), move || {
                                    ctx2.request_repaint()
                                });
                            self.debugger_handle = Some(handle);
                            std::thread::spawn(move || debugger.run());
                        }
                    } else {
                        ui.label("Loaded");
                    }
                });
            }

            if let Some(debugger_handle) = &self.debugger_handle {
                if let Some(snes) = debugger_handle.shared_snes.try_lock() {
                    if let Some(snes) = snes.clone() {
                        let now = std::time::Instant::now();

                        if let Some((t_clock, t)) = self.timing_clock.as_mut() {
                            let time_elapsed = now.duration_since(*t);
                            if time_elapsed > std::time::Duration::from_secs(1) {
                                let clocks_elapsed = snes.clock - *t_clock;
                                let seconds_elapsed = time_elapsed.as_secs_f64();
                                self.f = clocks_elapsed as f64 / seconds_elapsed;
                                *t_clock = snes.clock;
                                *t = now;
                            }
                        } else {
                            self.timing_clock = Some((snes.clock, now));
                        }

                        ui.horizontal(|ui| {
                            ui.label("Clock:");
                            ui.monospace(snes.clock.to_string());
                        });

                        ui.horizontal(|ui| {
                            ui.label("f:");
                            ui.monospace(format!("{:.3} MHz ({:.3} ns)", self.f / 1_000_000.0, 1_000_000_000.0 / self.f));
                        });

                        ui.horizontal(|ui| {
                            if ui.button("Clock").clicked() {
                                let mut cmd = debugger_handle.shared_command.lock();
                                *cmd = Some(ToDebuggerMsg::Clock(1));
                                    //.to_debugger
                                    //.send(ToDebuggerMsg::Clock(1))
                                    //.ok();
                            }
                            if ui.button("Run").clicked() {
                                //debugger_handle.to_debugger.send(ToDebuggerMsg::Run(self.watchpoints.clone())).ok();
                                let mut cmd = debugger_handle.shared_command.lock();
                                *cmd = Some(ToDebuggerMsg::Run(self.watchpoints.clone()));
                            }
                            if ui.button("Stop").clicked() {
                                //debugger_handle.to_debugger.send(ToDebuggerMsg::Stop).ok();
                                let mut cmd = debugger_handle.shared_command.lock();
                                *cmd = Some(ToDebuggerMsg::Stop);
                            }
                        });

                        ui.horizontal(|ui| {
                            ui.vertical(|ui| {
                                ui.set_width(250.0);
                                ui.collapsing("ROM", |ui| rom_ui(ui, &snes.rom));
                                ui.collapsing("CPU", |ui| cpu_ui(ui, &snes.cpu));
                                ui.collapsing("PPU", |ui| ppu_ui(ui, &snes.ppu));
                            });

                            ui.group(|ui| watchpoints_ui(ui, &mut self.watchpoints, &snes));
                        });
                    }
                }
            }
        });
    }
}

fn ppu_ui(ui: &mut egui::Ui, ppu: &Ppu) {
    ui.vertical(|ui| {
        ui.horizontal(|ui| {
            ui.horizontal(|ui| {
                ui.label("H Pixel");
            });
            ui.with_layout(egui::Layout::right_to_left(), |ui| {
                ui.monospace(format!("{}", ppu.h_pixel));
            });
        });

        ui.horizontal(|ui| {
            ui.horizontal(|ui| {
                ui.label("V Pixel");
            });
            ui.with_layout(egui::Layout::right_to_left(), |ui| {
                ui.monospace(format!("{}", ppu.v_pixel));
            });
        });

        ui.horizontal(|ui| {
            ui.horizontal(|ui| {
                ui.label("Field");
            });
            ui.with_layout(egui::Layout::right_to_left(), |ui| {
                ui.monospace(format!("{}", ppu.field));
            });
        });
    });
}

fn watchpoints_ui(ui: &mut egui::Ui, watchpoints: &mut Watchpoints, snes: &Snes) {
    ui.vertical(|ui| {
        ui.set_width(250.0);
        let mut i = 0;

        let mut keys_to_remove = Vec::new();
        for (key, watchpoint) in watchpoints.points.iter_mut() {
            ui.vertical(|ui| {
                ui.horizontal(|ui| {
                    ui.horizontal(|ui| {
                        ui.label(&watchpoint.name);
                    });

                    ui.with_layout(egui::Layout::right_to_left(), |ui| {
                        if ui.button("x").clicked() {
                            keys_to_remove.push(key);
                        }

                        if ui.button("+brk").clicked() {
                            watchpoint.conditions.push(Breakpoint {
                                enabled: false,
                                condition_value: 0,
                                condition: BreakpointCondition::Always,
                                only_change: true,
                            });
                        }

                        ui.monospace(watchpoint.formatted(snes));
                    });
                });

                watchpoint.conditions.retain_mut(|breakpoint| {
                    let mut remove = false;

                    ui.horizontal(|ui| {
                        ui.horizontal(|ui| {
                            ui.checkbox(&mut breakpoint.enabled, "");
                            egui::ComboBox::from_id_source(i)
                                .width(75.0)
                                .selected_text(breakpoint.condition.to_string())
                                .show_ui(ui, |ui| {
                                    ui.selectable_value(
                                        &mut breakpoint.condition,
                                        BreakpointCondition::Always,
                                        BreakpointCondition::Always.to_string(),
                                    );
                                    ui.selectable_value(
                                        &mut breakpoint.condition,
                                        BreakpointCondition::Equal,
                                        BreakpointCondition::Equal.to_string(),
                                    );
                                    ui.selectable_value(
                                        &mut breakpoint.condition,
                                        BreakpointCondition::NotEqual,
                                        BreakpointCondition::NotEqual.to_string(),
                                    );
                                    ui.selectable_value(
                                        &mut breakpoint.condition,
                                        BreakpointCondition::LessThan,
                                        BreakpointCondition::LessThan.to_string(),
                                    );
                                    ui.selectable_value(
                                        &mut breakpoint.condition,
                                        BreakpointCondition::GreaterThan,
                                        BreakpointCondition::GreaterThan.to_string(),
                                    );
                                });


                            let mut value = breakpoint.condition_value.to_string();
                            let value_edit =
                                egui::TextEdit::singleline(&mut value).desired_width(25.0);
                            ui.add_enabled(breakpoint.condition != BreakpointCondition::Always, value_edit);

                            if let Ok(v) = value.parse() {
                                breakpoint.condition_value = v;
                            }

                            ui.checkbox(&mut breakpoint.only_change, "Change");
                        });

                        ui.with_layout(egui::Layout::right_to_left(), |ui| {
                            remove = !ui.button("x").clicked();
                        });
                    });

                    i += 1;

                    remove
                });

                if !watchpoint.conditions.is_empty() {
                    ui.separator();
                }
            });
        }

        for key in keys_to_remove {
            watchpoints.points.remove(key);
        }

        ui.menu_button("Add", |ui| {
            ui.menu_button("SNES", |ui| {
                if ui.button("NMI").clicked() {
                    watchpoints.points.insert(Watchpoint::new(
                        "SNES NMI".to_string(),
                        |snes| snes.nmi as u64,
                        decimal,
                    ));
                }
            });

            ui.menu_button("PPU", |ui| {
                if ui.button("H Pixel").clicked() {
                    watchpoints.points.insert(Watchpoint::new(
                        "PPU H Pixel".to_string(),
                        |snes| snes.ppu.h_pixel as u64,
                        decimal,
                    ));
                }
                if ui.button("V Pixel").clicked() {
                    watchpoints.points.insert(Watchpoint::new(
                        "PPU V Pixel".to_string(),
                        |snes| snes.ppu.v_pixel as u64,
                        decimal,
                    ));
                }
                if ui.button("Field").clicked() {
                    watchpoints.points.insert(Watchpoint::new(
                        "PPU Field".to_string(),
                        |snes| snes.ppu.field as u64,
                        decimal,
                    ));
                }
            });

            ui.menu_button("CPU", |ui| {
                if ui.button("Cycle").clicked() {
                    watchpoints.points.insert(Watchpoint::new(
                        "CPU Cycle".to_string(),
                        |snes| snes.cpu.cycle as u64,
                        decimal,
                    ));
                }

                if ui.button("Opcode").clicked() {
                    watchpoints.points.insert(Watchpoint::new(
                        "CPU Opcode".to_string(),
                        |snes| snes.cpu.opcode as u64,
                        |opcode| opcodes::opcode_name(opcode as u8).to_string(),
                    ));
                }

                if ui.button("X").clicked() {
                    watchpoints.points.insert(Watchpoint::new(
                        "CPU X".to_string(),
                        |snes| snes.cpu.registers.index_x as u64,
                        hex_two_byte,
                    ));
                }

                if ui.button("Y").clicked() {
                    watchpoints.points.insert(Watchpoint::new(
                        "CPU Y".to_string(),
                        |snes| snes.cpu.registers.index_y as u64,
                        hex_two_byte,
                    ));
                }

                if ui.button("A").clicked() {
                    watchpoints.points.insert(Watchpoint::new(
                        "CPU A".to_string(),
                        |snes| snes.cpu.registers.accumulator as u64,
                        hex_two_byte,
                    ));
                }

                if ui.button("Stack Pointer").clicked() {
                    watchpoints.points.insert(Watchpoint::new(
                        "CPU SP".to_string(),
                        |snes| snes.cpu.registers.stack_pointer as u64,
                        hex_two_byte,
                    ));
                }

                if ui.button("Program Counter").clicked() {
                    watchpoints.points.insert(Watchpoint::new(
                        "CPU PC".to_string(),
                        |snes| snes.cpu.registers.program_counter as u64,
                        hex_two_byte,
                    ));
                }

                if ui.button("Direct").clicked() {
                    watchpoints.points.insert(Watchpoint::new(
                        "CPU D".to_string(),
                        |snes| snes.cpu.registers.direct as u64,
                        hex_two_byte,
                    ));
                }

                if ui.button("PBR").clicked() {
                    watchpoints.points.insert(Watchpoint::new(
                        "CPU PBR".to_string(),
                        |snes| snes.cpu.registers.program_bank as u64,
                        hex_one_byte,
                    ));
                }

                if ui.button("DBR").clicked() {
                    watchpoints.points.insert(Watchpoint::new(
                        "CPU DBR".to_string(),
                        |snes| snes.cpu.registers.data_bank as u64,
                        hex_one_byte,
                    ));
                }

                if ui.button("MDR").clicked() {
                    watchpoints.points.insert(Watchpoint::new(
                        "CPU MDR".to_string(),
                        |snes| snes.cpu.registers.mdr as u64,
                        hex_one_byte,
                    ));
                }

                ui.menu_button("Status", |ui| {
                    if ui.button("N").clicked() {
                        watchpoints.points.insert(Watchpoint::new(
                            "Status N".to_string(),
                            |snes| snes.cpu.registers.processor_status.negative as u64,
                            decimal,
                        ));
                    }

                    if ui.button("V").clicked() {
                        watchpoints.points.insert(Watchpoint::new(
                            "Status V".to_string(),
                            |snes| snes.cpu.registers.processor_status.overflow as u64,
                            decimal,
                        ));
                    }

                    if ui.button("M").clicked() {
                        watchpoints.points.insert(Watchpoint::new(
                            "Status M".to_string(),
                            |snes| snes.cpu.registers.processor_status.memory_select as u64,
                            decimal,
                        ));
                    }

                    if ui.button("X").clicked() {
                        watchpoints.points.insert(Watchpoint::new(
                            "Status X".to_string(),
                            |snes| snes.cpu.registers.processor_status.index_register_select as u64,
                            decimal,
                        ));
                    }

                    if ui.button("B").clicked() {
                        watchpoints.points.insert(Watchpoint::new(
                            "Status B".to_string(),
                            |snes| snes.cpu.registers.processor_status.brk as u64,
                            decimal,
                        ));
                    }

                    if ui.button("D").clicked() {
                        watchpoints.points.insert(Watchpoint::new(
                            "Status D".to_string(),
                            |snes| snes.cpu.registers.processor_status.decimal_mode as u64,
                            decimal,
                        ));
                    }

                    if ui.button("I").clicked() {
                        watchpoints.points.insert(Watchpoint::new(
                            "Status I".to_string(),
                            |snes| snes.cpu.registers.processor_status.irq_disable as u64,
                            decimal,
                        ));
                    }

                    if ui.button("Z").clicked() {
                        watchpoints.points.insert(Watchpoint::new(
                            "Status Z".to_string(),
                            |snes| snes.cpu.registers.processor_status.zero as u64,
                            decimal,
                        ));
                    }

                    if ui.button("C").clicked() {
                        watchpoints.points.insert(Watchpoint::new(
                            "Status C".to_string(),
                            |snes| snes.cpu.registers.processor_status.carry as u64,
                            decimal,
                        ));
                    }

                    if ui.button("E").clicked() {
                        watchpoints.points.insert(Watchpoint::new(
                            "Status E".to_string(),
                            |snes| snes.cpu.registers.processor_status.emulation as u64,
                            decimal,
                        ));
                    }
                });
            });
        });
    });
}

fn cpu_ui(ui: &mut egui::Ui, cpu: &Cpu65816) {
    ui.with_layout(
        egui::Layout::top_down_justified(egui::Align::Center),
        |ui| {
            ui.horizontal(|ui| {
                ui.horizontal(|ui| {
                    ui.label("Cycle:");
                    ui.monospace(cpu.cycle.to_string());
                });

            });

            ui.horizontal(|ui| {
                ui.label("Opcode:");
                ui.monospace(format!(
                        "{} ({:02x})",
                        opcodes::opcode_name(cpu.opcode),
                        cpu.opcode
                ));
            });

            ui.horizontal(|ui| {
                ui.label("Operand:");
                ui.monospace(format!("{:02x?}", cpu.operand));
            });

            ui.horizontal(|ui| {
                ui.horizontal(|ui| {
                    ui.label("X:");
                    ui.monospace(format!("{:04x}", cpu.registers.index_x));
                });

                ui.horizontal(|ui| {
                    ui.label("Y:");
                    ui.monospace(format!("{:04x}", cpu.registers.index_y));
                });

                ui.horizontal(|ui| {
                    ui.label("A:");
                    ui.monospace(format!("{:04x}", cpu.registers.accumulator));
                });
            });

            ui.horizontal(|ui| {
                ui.horizontal(|ui| {
                    ui.label("SP:");
                    ui.monospace(format!("{:04x}", cpu.registers.stack_pointer));
                });

                ui.horizontal(|ui| {
                    ui.label("PC:");
                    ui.monospace(format!("{:04x}", cpu.registers.program_counter));
                });

                ui.horizontal(|ui| {
                    ui.label("D:");
                    ui.monospace(format!("{:04x}", cpu.registers.direct));
                });
            });

            ui.horizontal(|ui| {
                ui.horizontal(|ui| {
                    ui.label("PBR:");
                    ui.monospace(format!("{:02x}", cpu.registers.program_bank));
                });

                ui.horizontal(|ui| {
                    ui.label("DBR:");
                    ui.monospace(format!("{:02x}", cpu.registers.data_bank));
                });

                ui.horizontal(|ui| {
                    ui.label("MDR:");
                    ui.monospace(format!("{:02x}", cpu.registers.mdr));
                });
            });

            ui.horizontal(|ui| {
                ui.horizontal(|ui| {
                    ui.label("N:");
                    ui.monospace(one_zero(cpu.registers.processor_status.negative));
                });

                ui.horizontal(|ui| {
                    ui.label("V:");
                    ui.monospace(one_zero(cpu.registers.processor_status.overflow));
                });

                ui.horizontal(|ui| {
                    ui.label("M:");
                    ui.monospace(one_zero(cpu.registers.processor_status.memory_select));
                });

                ui.horizontal(|ui| {
                    ui.label("X:");
                    ui.monospace(one_zero(
                        cpu.registers.processor_status.index_register_select,
                    ));
                });

                ui.horizontal(|ui| {
                    ui.label("B:");
                    ui.monospace(one_zero(cpu.registers.processor_status.brk));
                });
            });

            ui.horizontal(|ui| {
                ui.horizontal(|ui| {
                    ui.label("D:");
                    ui.monospace(one_zero(cpu.registers.processor_status.decimal_mode));
                });

                ui.horizontal(|ui| {
                    ui.label("I:");
                    ui.monospace(one_zero(cpu.registers.processor_status.irq_disable));
                });

                ui.horizontal(|ui| {
                    ui.label("Z:");
                    ui.monospace(one_zero(cpu.registers.processor_status.zero));
                });

                ui.horizontal(|ui| {
                    ui.label("C:");
                    ui.monospace(one_zero(cpu.registers.processor_status.carry));
                });

                ui.horizontal(|ui| {
                    ui.label("E:");
                    ui.monospace(one_zero(cpu.registers.processor_status.emulation));
                });
            });
        },
    );
}

fn rom_ui(ui: &mut egui::Ui, rom: &Rom) {
    let header = rom.header();
    ui.vertical(|ui| {
        ui.horizontal(|ui| {
            ui.label("Title:");
            ui.monospace(String::from_utf8_lossy(&header.title).as_ref());
        });

        ui.horizontal(|ui| {
            ui.label("Speed:");
            ui.monospace(match header.speed {
                rsnes::rom::RomSpeed::Fast => "Fast",
                rsnes::rom::RomSpeed::Slow => "Slow",
            });
        });

        ui.horizontal(|ui| {
            ui.label("Map Mode:");
            ui.monospace(match header.map_mode {
                rsnes::rom::MapMode::LoRom => "LoRom",
                rsnes::rom::MapMode::HiRom => "HiRom",
                rsnes::rom::MapMode::LoRomSdd1 => "LowRom SDD1",
                rsnes::rom::MapMode::LoRomSa1 => "LowRom SA1",
                rsnes::rom::MapMode::ExHiRom => "ExHiRom",
                rsnes::rom::MapMode::ExHiRomSpc7110 => "ExHiRom SPC7110",
            });
        });

        ui.horizontal(|ui| {
            ui.label("RAM:");
            ui.monospace(yes_no(header.chipset.ram));
        });

        ui.horizontal(|ui| {
            ui.label("Coprocessor:");
            ui.monospace(match header.chipset.coprocessor {
                Some(Coprocessor::Dsp) => "DSP",
                Some(Coprocessor::Gsu) => "GSU",
                Some(Coprocessor::Obc1) => "OBC1",
                Some(Coprocessor::Sa1) => "SA1",
                Some(Coprocessor::Sdd1) => "SDD1",
                Some(Coprocessor::Srtc) => "SRTC",
                Some(Coprocessor::Other) => "Other",
                Some(Coprocessor::Custom) => "Custom",
                None => "None",
            });
        });

        ui.horizontal(|ui| {
            ui.label("Battery:");
            ui.monospace(yes_no(header.chipset.battery));
        });

        ui.horizontal(|ui| {
            ui.label("RTC 4513:");
            ui.monospace(yes_no(header.chipset.rtc_4513));
        });

        ui.horizontal(|ui| {
            ui.label("GSU1 Overclocked:");
            ui.monospace(yes_no(header.chipset.overclocked_gsu1));
        });

        ui.horizontal(|ui| {
            ui.label("ROM Size:");
            ui.monospace(format!("{}k", header.rom_size_k));
        });

        ui.horizontal(|ui| {
            ui.label("RAM Size:");
            ui.monospace(format!("{}k", header.ram_size_k));
        });

        ui.horizontal(|ui| {
            ui.label("Country:");
            ui.monospace(format!("{}", header.country));
        });

        ui.horizontal(|ui| {
            ui.label("Developer:");
            ui.monospace(format!("{}", header.developer_id));
        });

        ui.horizontal(|ui| {
            ui.label("ROM Version:");
            ui.monospace(format!("{}", header.rom_version));
        });

        ui.horizontal(|ui| {
            ui.label("Checksum:");
            ui.monospace(format!("{}", header.checksum));
        });
    });
}

fn yes_no(b: bool) -> &'static str {
    if b {
        "Yes"
    } else {
        "No"
    }
}

fn one_zero(b: bool) -> &'static str {
    if b {
        "1"
    } else {
        "0"
    }
}

fn decimal(n: u64) -> String {
    format!("{}", n)
}

fn hex_one_byte(n: u64) -> String {
    format!("{:02x}", n)
}

fn hex_two_byte(n: u64) -> String {
    format!("{:04x}", n)
}
